/* 
 * File:   keyboard_mouse_manager.h
 * Author: firmware
 *
 * Created on December 18, 2019, 6:02 PM
 */

#ifndef KEYBOARD_MOUSE_MANAGER_H
#define	KEYBOARD_MOUSE_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "globals.h"

/***** PROTOTYPES ***/    
    
void MouseAnalogCoordinateRead(void);    
uint8_t  ReadMouseButton(uint8_t nbtn);
void Handle_MouseUSB(void);
void SetRowsInput(uint8_t nrow);
void SetColsInput(uint8_t ncol);
uint8_t  ReadColStatus(uint8_t ncol);
void SetLeds(void);
void KeyboardScanRead(void);


void APP_KeyboardInit(void);
void APP_KeyboardTasks(void);
/*********************************************************************
* Function: void APP_DeviceMouseStart(void);
*
* Overview: Starts running the demo.
*
* PreCondition: The device should be configured into the configuration
*   that contains the custome HID interface.  The APP_DeviceMouseInitialize()
*   function should also have been called before calling this function.
*
* Input: None
*
* Output: None
*
********************************************************************/
//void APP_DeviceMouseStart();

/*********************************************************************
* Function: void APP_DeviceMouseTasks(void);
*
* Overview: Keeps the demo running.
*
* PreCondition: The demo should have been initialized and started via
*   the APP_DeviceMouseInitialize() and APP_DeviceMouseStart() demos
*   respectively.
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceMouseTasks();

bool APP_DeviceMouseEventHandler(unsigned int event, void *pdata, size_t size);
void APP_DeviceMouseSOFHandler(void);

#ifdef	__cplusplus
}
#endif

#endif	/* KEYBOARD_MOUSE_MANAGER_H */

