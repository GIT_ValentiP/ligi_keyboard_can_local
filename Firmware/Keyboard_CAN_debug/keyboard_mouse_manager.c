
/**
  Generated Main Source File

  Company:
 K-Tronic

  File Name:
    keyboard_mouse_manager.c

  Summary:
  

  Description:
    This header file provides implementations for driver 
*/

#include <stdint.h>
#include <string.h>
#include "globals.h"
#include "keyboard_mouse_manager.h"
#include "usb_hid_keys.h"
#include "mcc_generated_files/usb/usb.h"
#include "mcc_generated_files/usb/usb_device_hid.h"
#include "mcc_generated_files/usb/usb_device_config.h"
#include "mcc_generated_files/mcc.h"
#include "mcc_generated_files/adc.h"



//***********************************************                 COL_0        COL_1        COL_2        COL_3       COL_4         COL_5        COL_6        COL_7       COL_8      COL_9
const uint8_t KEYBOARD1_SCANCODE[NUM_ROW_MAX*NUM_COL_MAX]={     KEY_ESC,      KEY_F11,       KEY_6,         KEY_T,     KEY_ENTER,       KEY_A,   KEY_SLASH, KEY_LEFTSHIFT, KEY_LEFTMETA,KEY_NUMLOCK, //ROW_0
                                                                 KEY_F1,      KEY_F12,       KEY_5,         KEY_Y,KEY_APOSTROPHE,    KEY_NONE,      KEY_UP,KEY_RIGHTSHIFT,  KEY_COMPOSE,   KEY_NONE, //ROW_1
                                                                 KEY_F2,     KEY_NONE,       KEY_4,         KEY_U, KEY_SEMICOLON,       KEY_Z,KEY_PAGEDOWN,  KEY_LEFTCTRL,     KEY_NONE,   KEY_NONE, //ROW_2 
                                                                 KEY_F3,KEY_BACKSPACE,       KEY_3,         KEY_I,         KEY_L,       KEY_X,   KEY_RIGHT,   KEY_LEFTALT,     KEY_NONE,   KEY_NONE, //ROW_3
                                                                 KEY_F4,     KEY_HOME,       KEY_2,         KEY_O,         KEY_K,       KEY_C,    KEY_DOWN,  KEY_RIGHTALT,     KEY_NONE,   KEY_NONE, //ROW_4
                                                                 KEY_F5,    KEY_EQUAL,       KEY_1,         KEY_P,         KEY_J,       KEY_V,    KEY_LEFT,  KEY_CAPSLOCK,     KEY_NONE,   KEY_NONE, //ROW_5
                                                                 KEY_F6,    KEY_MINUS,     KEY_TAB, KEY_LEFTBRACE,         KEY_H,       KEY_B,  KEY_DELETE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_6
                                                                 KEY_F7,        KEY_0,       KEY_Q,KEY_RIGHTBRACE,         KEY_G,       KEY_N,  KEY_INSERT,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_7
                                                                 KEY_F8,        KEY_9,       KEY_W, KEY_BACKSLASH,         KEY_F,       KEY_M,   KEY_SPACE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_8
                                                                 KEY_F9,        KEY_8,       KEY_E,       KEY_END,         KEY_D,   KEY_COMMA,    KEY_NONE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_9
                                                                 KEY_F10,       KEY_7,       KEY_R,  KEY_PAGEDOWN,         KEY_S,     KEY_DOT,    KEY_NONE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_10
                                                 };

const uint8_t KEYBOARD1_SCANCODE_1[NUM_ROW_MAX*NUM_COL_MAX]={   KEY_ESC,      KEY_F11,       KEY_6,         KEY_T,     KEY_ENTER,       KEY_A,  KEY_KPPLUS, KEY_LEFTSHIFT, KEY_LEFTMETA,KEY_NUMLOCK, //ROW_0
                                                                 KEY_F1,      KEY_F12,       KEY_5,         KEY_Y,KEY_APOSTROPHE,    KEY_NONE,      KEY_UP,KEY_RIGHTSHIFT,  KEY_COMPOSE,   KEY_NONE, //ROW_1
                                                                 KEY_F2,     KEY_NONE,       KEY_4,       KEY_KP4,   KEY_KPMINUS,       KEY_Z,KEY_PAGEDOWN,  KEY_LEFTCTRL,     KEY_NONE,   KEY_NONE, //ROW_2 
                                                                 KEY_F3,KEY_BACKSPACE,       KEY_3,       KEY_KP5,       KEY_KP3,       KEY_X,   KEY_RIGHT,   KEY_LEFTALT,     KEY_NONE,   KEY_NONE, //ROW_3
                                                                 KEY_F4,     KEY_HOME,       KEY_2,       KEY_KP6,       KEY_KP2,       KEY_C,    KEY_DOWN,  KEY_RIGHTALT,     KEY_NONE,   KEY_NONE, //ROW_4
                                                                 KEY_F5,    KEY_MAJOR,       KEY_1,KEY_KPASTERISK,       KEY_KP1,       KEY_V,    KEY_LEFT,  KEY_CAPSLOCK,     KEY_NONE,   KEY_NONE, //ROW_5
                                                                 KEY_F6,KEY_102_MINOR,     KEY_TAB,KEY_GRAVE_BKSH,         KEY_H,       KEY_B,  KEY_DELETE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_6
                                                                 KEY_F7,  KEY_KPSLASH,       KEY_Q,KEY_RIGHTBRACE,         KEY_G,       KEY_N,  KEY_INSERT,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_7
                                                                 KEY_F8,      KEY_KP9,       KEY_W, KEY_BACKSLASH,         KEY_F,     KEY_KP0,   KEY_SPACE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_8
                                                                 KEY_F9,      KEY_KP8,       KEY_E,       KEY_END,         KEY_D,   KEY_KPDOT,    KEY_NONE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_9
                                                                 KEY_F10,     KEY_KP7,       KEY_R,  KEY_PAGEDOWN,         KEY_S, KEY_KPEQUAL,    KEY_NONE,      KEY_NONE,     KEY_NONE,   KEY_NONE, //ROW_10
                                                 };




// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Constants
// *****************************************************************************
// *****************************************************************************


// *****************************************************************************
// *****************************************************************************
// Section: File Scope Data Types
// *****************************************************************************
// *****************************************************************************

/* This typedef defines the only INPUT report found in the HID report
 * descriptor and gives an easy way to create the OUTPUT report. */
typedef struct __attribute__((packed))
{
    /* The union below represents the first byte of the INPUT report.  It is
     * formed by the following HID report items:
     *
     *  0x19, 0xe0, //   USAGE_MINIMUM (Keyboard LeftControl)
     *  0x29, 0xe7, //   USAGE_MAXIMUM (Keyboard Right GUI)
     *  0x15, 0x00, //   LOGICAL_MINIMUM (0)
     *  0x25, 0x01, //   LOGICAL_MAXIMUM (1)
     *  0x75, 0x01, //   REPORT_SIZE (1)
     *  0x95, 0x08, //   REPORT_COUNT (8)
     *  0x81, 0x02, //   INPUT (Data,Var,Abs)
     *
     * The report size is 1 specifying 1 bit per entry.
     * The report count is 8 specifying there are 8 entries.
     * These entries represent the Usage items between Left Control (the usage
     * minimum) and Right GUI (the usage maximum).
     */
    union __attribute__((packed))
    {
        uint8_t value;
        struct __attribute__((packed))
        {
            unsigned leftControl    :1;
            unsigned leftShift      :1;
            unsigned leftAlt        :1;
            unsigned leftGUI        :1;
            unsigned rightControl   :1;
            unsigned rightShift     :1;
            unsigned rightAlt       :1;
            unsigned rightGUI       :1;
        } bits;
    } modifiers;

    /* There is one byte of constant data/padding that is specified in the
     * input report:
     *
     *  0x95, 0x01,                    //   REPORT_COUNT (1)
     *  0x75, 0x08,                    //   REPORT_SIZE (8)
     *  0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
     */
    unsigned :8;

    /* The last INPUT item in the INPUT report is an array type.  This array
     * contains an entry for each of the keys that are currently pressed until
     * the array limit, in this case 6 concurent key presses.
     *
     *  0x95, 0x06,                    //   REPORT_COUNT (6)
     *  0x75, 0x08,                    //   REPORT_SIZE (8)
     *  0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
     *  0x25, 0x65,                    //   LOGICAL_MAXIMUM (101)
     *  0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
     *  0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
     *  0x29, 0x65,                    //   USAGE_MAXIMUM (Keyboard Application)
     *
     * Report count is 6 indicating that the array has 6 total entries.
     * Report size is 8 indicating each entry in the array is one byte.
     * The usage minimum indicates the lowest key value (Reserved/no event)
     * The usage maximum indicates the highest key value (Application button)
     * The logical minimum indicates the remapped value for the usage minimum:
     *   No Event has a logical value of 0.
     * The logical maximum indicates the remapped value for the usage maximum:
     *   Application button has a logical value of 101.
     *
     * In this case the logical min/max match the usage min/max so the logical
     * remapping doesn't actually change the values.
     *
     * To send a report with the 'a' key pressed (usage value of 0x04, logical
     * value in this example of 0x04 as well), then the array input would be the
     * following:
     *
     * LSB [0x04][0x00][0x00][0x00][0x00][0x00] MSB
     *
     * If the 'b' button was then pressed with the 'a' button still held down,
     * the report would then look like this:
     *
     * LSB [0x04][0x05][0x00][0x00][0x00][0x00] MSB
     *
     * If the 'a' button was then released with the 'b' button still held down,
     * the resulting array would be the following:
     *
     * LSB [0x05][0x00][0x00][0x00][0x00][0x00] MSB
     *
     * The 'a' key was removed from the array and all other items in the array
     * were shifted down. */
    uint8_t keys[6];
} KEYBOARD_INPUT_REPORT;


/* This typedef defines the only OUTPUT report found in the HID report
 * descriptor and gives an easy way to parse the OUTPUT report. */
typedef union __attribute__((packed))
{
    /* The OUTPUT report is comprised of only one byte of data. */
    uint8_t value;
    struct
    {
        /* There are two report items that form the one byte of OUTPUT report
         * data.  The first report item defines 5 LED indicators:
         *
         *  0x95, 0x05,                    //   REPORT_COUNT (5)
         *  0x75, 0x01,                    //   REPORT_SIZE (1)
         *  0x05, 0x08,                    //   USAGE_PAGE (LEDs)
         *  0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
         *  0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
         *  0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
         *
         * The report count indicates there are 5 entries.
         * The report size is 1 indicating each entry is just one bit.
         * These items are located on the LED usage page
         * These items are all of the usages between Num Lock (the usage
         * minimum) and Kana (the usage maximum).
         */
        unsigned numLock        :1;
        unsigned capsLock       :1;
        unsigned scrollLock     :1;
        unsigned compose        :1;
        unsigned kana           :1;

        /* The second OUTPUT report item defines 3 bits of constant data
         * (padding) used to make a complete byte:
         *
         *  0x95, 0x01,                    //   REPORT_COUNT (1)
         *  0x75, 0x03,                    //   REPORT_SIZE (3)
         *  0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
         *
         * Report count of 1 indicates that there is one entry
         * Report size of 3 indicates the entry is 3 bits long. */
        unsigned                :3;
    } leds;
} KEYBOARD_OUTPUT_REPORT;


/* INPUT report - this structure will represent the only INPUT report in the HID
 * descriptor.
 */
typedef struct __attribute__((packed))
{
    /* The first INPUT item is the following:
     *   0x05, 0x09,    //Usage Page (Buttons)
     *   0x19, 0x01,    //Usage Minimum (01)
     *   0x29, 0x03,    //Usage Maximum (03)
     *   0x15, 0x00,    //Logical Minimum (0)
     *   0x25, 0x01,    //Logical Maximum (1)
     *   0x95, 0x03,    //Report Count (3)
     *   0x75, 0x01,    //Report Size (1)
     *   0x81, 0x02,    //Input (Data, Variable, Absolute)
     *
     * The usage page is buttons
     * The report size is 1 (1-bit)
     * The report count is 3, thus 3 1-bit items
     * The Usage Min is 1 and the Usage maximum is 3, thus buttons 1-3, also
     *   call the primary, secondary, and tertiary buttons.
     *
     * The second INPUT item comes from the fact that the report must be byte
     * aligned, so we need to pad the previous 3-bit report with 5-bits of
     * constant(filler) data.
     *   0x95, 0x01,    //Report Count (1)
     *   0x75, 0x05,    //Report Size (5)
     *   0x81, 0x01,    //Input (Constant)
     */
    union __attribute__((packed))
    {
        struct __attribute__((packed))
        {
            unsigned button1   :1; //button LEFT
            unsigned button2   :1; //Button CENTRAL
            unsigned button3   :1;//butto RIGHT
            unsigned :5;
        };
        struct __attribute__((packed))
        {
            unsigned primary   :1;
            unsigned secondary :1;
            unsigned tertiary  :1;
            unsigned :5;
        };
        uint8_t value;
    } buttons;

    /* The final INPUT item is the following:
     *   0x05, 0x01,    //Usage Page (Generic Desktop)
     *   0x09, 0x30,    //Usage (X)
     *   0x09, 0x31,    //Usage (Y)
     *   0x15, 0x81,    //Logical Minimum (-127)
     *   0x25, 0x7F,    //Logical Maximum (127)
     *   0x75, 0x08,    //Report Size (8)
     *   0x95, 0x02,    //Report Count (2)
     *   0x81, 0x06,    //Input (Data, Variable, Relative)
     *
     * The report size is 8 (8-bit)
     * The report count is 2, thus 2 bytes of data.
     * The first usage is (X) and the second is (Y) so the first byte will
     *   represent the X mouse value, and the second the Y value.
     * The logical min/max determines the bounds for X and Y, -127 to 127.
     * The INPUT type is relative so each report item is relative to the last
     *   report item.  So reporting "-1" for X means that since the last report
     *   was sent, the mouse has move left
     */
    uint8_t x;
    uint8_t y;
} MOUSE_REPORT;


/* This creates a storage type for all of the information required to track the
 * current state of the keyboard. */
typedef struct
{
    USB_HANDLE lastINTransmission;
    USB_HANDLE lastOUTTransmission;
    unsigned char key;
    bool waitingForRelease;
    bool waitingAltRelease;
} KEYBOARD;

typedef struct
{
    bool sentStop;
    bool lastButtonState;
    uint8_t vectorPosition;
    uint16_t movementCount;
    bool movementMode;
    
    struct
    {
        USB_HANDLE handle;
        uint8_t idleRate;
        uint8_t idleRateSofCount;
    } inputReport[1];

} MOUSE;


/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */


#define MOUSE_MOVEMENT_DISTANCE 400

static const uint8_t xVector[]={ -1,  0, 1, 0};
static const uint8_t yVector[]={  0, -1, 0, 1};

static MOUSE mouse;

static KEYBOARD keyboard;


// *****************************************************************************
// *****************************************************************************
// Section: File Scope or Global Variables
// *****************************************************************************
// *****************************************************************************


#if !defined(KEYBOARD_INPUT_REPORT_DATA_BUFFER_ADDRESS_TAG)
    #define KEYBOARD_INPUT_REPORT_DATA_BUFFER_ADDRESS_TAG
#endif
static KEYBOARD_INPUT_REPORT inputReport KEYBOARD_INPUT_REPORT_DATA_BUFFER_ADDRESS_TAG;

#if !defined(KEYBOARD_OUTPUT_REPORT_DATA_BUFFER_ADDRESS_TAG)
    #define KEYBOARD_OUTPUT_REPORT_DATA_BUFFER_ADDRESS_TAG
#endif
static volatile KEYBOARD_OUTPUT_REPORT outputReport KEYBOARD_OUTPUT_REPORT_DATA_BUFFER_ADDRESS_TAG;

#if defined(FIXED_ADDRESS_MEMORY)
    #if defined(COMPILER_MPLAB_C18)
        #pragma udata MOUSE_REPORT_DATA_BUFFER=MOUSE_REPORT_DATA_BUFFER_ADDRESS
            static MOUSE_REPORT mouseReport;
        #pragma udata
    #elif defined(__XC8)
        static MOUSE_REPORT mouseReport  MOUSE_REPORT_DATA_BUFFER_ADDRESS;
    #endif
#else
    static MOUSE_REPORT mouseReport;
#endif



// *****************************************************************************
// *****************************************************************************
// Section: Private Prototypes
// *****************************************************************************
// *****************************************************************************
static void APP_KeyboardProcessOutputReport(void);


//Exteranl variables declared in other .c files
//extern volatile signed int SOFCounter;


//Application variables that need wide scope
KEYBOARD_INPUT_REPORT oldInputReport;
signed int keyboardIdleRate;
//signed int LocalSOFCount;
//static signed int OldSOFCount;







void SetMouseButtonUSB(uint8_t nbtn,uint8_t val);
/******************************************************************************
 * Function:        void MouseAnalogCoordinateRead(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            
 *                  
 *                  
 *****************************************************************************/


//**********************************************************************
void MouseAnalogCoordinateRead(void) {
   adc_result_t ReadADC;

	enum {
		ASSEX1=EAST_MOUSE,			// testato 18/3/09
		ASSEX2=WEST_MOUSE,
		ASSEY1=SOUTH_MOUSE,
		ASSEY2=NORTH_MOUSE,
		};

	ReadADC=ADC_GetConversion(ASSEY1);
// si pu� usare ADC_12_TAD, per auto-ritardare il campionamento dal SetChan...
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
	YVel=(ReadADC >> 6) & 0xf;				// 10 bit, prendo i 4 + significativi

	ReadADC=ADC_GetConversion(ASSEY2);
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
	YVel2=(ReadADC >> 6) & 0xf;				// 10 bit, prendo i 4 + significativi

	ReadADC=ADC_GetConversion(ASSEX1);
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
	XVel=(ReadADC >> 6) & 0xf;				// 10 bit, prendo i 4 + significativi

	ReadADC=ADC_GetConversion(ASSEX2);
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
//	Delay_uS(5);
//	ConvertADC();						// spezzare, spostare e migliorare...
//	while(BusyADC());
	XVel2=(ReadADC >> 6) & 0xf;				// 10 bit, prendo i 4 + significativi
// LAVORO CON VALORE RELATIVO, OSSIA DIFFERENZA TRA LETTURA N e S, e DIFFERENZA TRA E e W (migliore in caso di non perfetta pressione sul pad)
	XCOUNT=XVel ^ 0xf;		// nel mouse PS/2, i valori sono a 9 bit!! quindi i valori vanno da +1 a +15 e da -1 a -15!! (cfr. seriale!)
	XCOUNT -= (XVel2 ^ 0xf);
	if(XCOUNT) {
		XCOUNT *=2;		//2007
		// PER ORA NON USO SCALING (scalatura fissa) ne' FLAGS<4> (scalatura 1:1 opp. 2:1)
		READ[1]=XCOUNT;			// sarebbe pi� giusto ADD, in caso il valore letto non potesse essere inviato subito, ma per semplicita' (gestione pos/neg.) faccio cosi'...
		if(XCOUNT < 0)		// faccio sign-extension: da 8 bit a 9 bit...
			READ[0] |= (1 << 4);
	}
	YCOUNT=YVel ^ 0xf;
	YCOUNT -= (YVel2 ^ 0xf);
	if(YCOUNT) {
		YCOUNT *=2;		//2007
		// PER ORA NON USO SCALING (scalatura fissa) ne' FLAGS<4> (scalatura 1:1 opp. 2:1)
		READ[2]=YCOUNT;		// sarebbe pi� giusto ADD, in caso il valore letto non potesse essere inviato subito, ma per semplicita' (gestione pos/neg.) faccio cosi'...
		if(YCOUNT < 0)		// faccio sign-extension: da 8 bit a 9 bit...
			READ[0] |= (1 << 5);
	}
    mouse_status.XCOUNT=XCOUNT;
    mouse_status.YCOUNT=YCOUNT;
	if(XCOUNT || YCOUNT) 
		FLAGA |= (1 << TRIGM);
}


/******************************************************************************
 * Function:       ReadMouseButton
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
uint8_t  ReadMouseButton(uint8_t nbtn)
{
   uint8_t ret_val;
   ret_val=1;
   switch(nbtn){
       case(0):
               ret_val= SW1_LEFT_GetValue();
               break;
       case(1):
               ret_val=SW2_RIGHT_GetValue();
               break;
       
       default:
           
               break;
   }
   return(ret_val);
}
/******************************************************************************
 * Function:       ReadMouseButton
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetMouseButtonUSB(uint8_t nbtn,uint8_t val)
{
   
   switch(nbtn){
       case(LEFT_BUTTON_BIT):
                      if (val==0) 
                            mouseBuffer[0] |= (1 << LEFT_BUTTON_BIT);
                      else
                          mouseBuffer[0] &= ~(1 << LEFT_BUTTON_BIT);

                      break;
       case(RIGHT_BUTTON_BIT):
                     if(val==0)
                        mouseBuffer[0] |= (1 << RIGHT_BUTTON_BIT);
                      else
                        mouseBuffer[0] &= ~(1 << RIGHT_BUTTON_BIT);
                     break;
        case(MIDDLE_BUTTON_BIT):
                     if(val==0)
                        mouseBuffer[0] |= (1 << MIDDLE_BUTTON_BIT);
                      else
                        mouseBuffer[0] &= ~(1 << MIDDLE_BUTTON_BIT);
                     break;
       default:
           
               break;
   }
   
}
/******************************************************************************
 * Function:        void ProcessIO(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is a place holder for other user routines.
 *                  It is a mixture of both USB and non-USB tasks.
 *
 * Note:            None
 *****************************************************************************/
void Handle_MouseUSB(void) {
	uint8_t i ;
	//ClrWdt();
	
		mouseBuffer[0]=0;
		mouseBuffer[1]=0;
		mouseBuffer[2]=0;
		mouseBuffer[3]=0;
//		i=~PORTA;             //BUTTON STATUS CHANGE ?
//		i &= (Bu1Val | Bu2Val);
//		if(i != BSTAT) {		      //IF CHANGE THEN TRIGGER
//			FLAGA |= (1 << TRIGM);         //(CHANGE) SET TRIGGER FLAG
//		}
//        if(!m_Bu1Bit)
//            mouseBuffer[0] |= (1 << LEFT_BUTTON_BIT);
//        else
//            mouseBuffer[0] &= ~(1 << LEFT_BUTTON_BIT);
//        if(!m_Bu2Bit)
//            mouseBuffer[0] |= (1 << RIGHT_BUTTON_BIT);
//        else
//            mouseBuffer[0] &= ~(1 << RIGHT_BUTTON_BIT);
		for(i=0;i<NUM_BUTTON_MOUSE;i++){
           mouse_status.btn_old[i]=mouse_status.btn_val[i];        
           mouse_status.btn_val[i]=ReadMouseButton(i);    
           if (mouse_status.btn_old[i]==mouse_status.btn_val[i]){
               if (mouse_status.btn_deb_ms[i] > KEY_PRESSED_DEBOUNCE_MS){
                  mouse_status.btn_deb_ms[i] = KEY_PRESSED_DEBOUNCE_MS; 
               }
               mouse_status.btn_deb_ms[i]++;
           }else{
               FLAGA |= (1 << TRIGM);         //(CHANGE) SET TRIGGER FLAG
               switch (i){
                    case(0):                   
                           
                                break;
                    case(1):
                                //NUMLOCK_LED2_Toggle(); 
                                break;   
                    default:
                           break;  
               } 
               mouse_status.btn_deb_ms[i]=0;
           }
           SetMouseButtonUSB(i,mouse_status.btn_val[i]);
        }																
		//BSTAT=i;
		MouseAnalogCoordinateRead();
		if(FLAGA & (1 << TRIGM)) {
            
			mouseBuffer[1]=XCOUNT;		// sarebbe pi� giusto ADD, in caso il valore letto non potesse essere inviato subito, ma per semplicita' (gestione pos/neg.) faccio cosi'...
			i=-YCOUNT; //??
			mouseBuffer[2]=i;		// sarebbe pi� giusto ADD, in caso il valore letto non potesse essere inviato subito, ma per semplicita' (gestione pos/neg.) faccio cosi'...
//            if (YCOUNT >128){
//                if(duty_pwm_backlight>0){
//                   duty_pwm_backlight--;
//                }     
//            }else if (YCOUNT > 0){             
//                if(duty_pwm_backlight<100){
//                   duty_pwm_backlight++;
//                }               
//            }
//            EPWM1_LoadDutyValue(duty_pwm_backlight);
            /*****  USB Transmission  ***/
//			while(mHIDTxIsBusy())	{
//				USBDriverService(); 
//				}
//				HIDTxReport(mouseBuffer,3);		// � giusto 3... in ASM ne avevamo messi 4...
			FLAGA &= ~(1 << TRIGM);
	    }

}

/******************************************************************************
 * Function:        SetRowsInput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetRowsInput(uint8_t nrow)
{
//    uint8_t dir_pin[NUM_ROW_MAX];
//    uint8_t id;
//    for(id=0;id<NUM_ROW_MAX;id++){
//        if(id==nrow){
//           dir_pin[id]= OUTPUT_PIN_PORT;
//        }else{
//           dir_pin[id]= INPUT_PIN_PORT;  
//        }
//    }
//    id=0;
//    ROW0_RB0_TRIS=dir_pin[id++];
//    ROW1_RB1_TRIS=dir_pin[id++];
//    ROW2_RB2_TRIS=dir_pin[id++];
//    ROW3_RB3_TRIS=dir_pin[id++];
//    ROW4_RB4_TRIS=dir_pin[id++];
//    ROW5_RB5_TRIS=dir_pin[id++];
//    ROW6_RB6_TRIS=dir_pin[id++];
//    ROW7_RB7_TRIS=dir_pin[id++];
//    ROW8_RE0_TRIS=dir_pin[id++];
//    ROW9_RE1_TRIS=dir_pin[id++];
//    ROW10_RE2_TRIS=dir_pin[id++];
    LATB=0xFF;
    LATE=0x07;
    TRISE = 0x00;
    TRISB = 0x00;
    switch(nrow){
        case(0):              
                ROW0_RB0_SetLow(); 
                break;
        case(1):
                ROW1_RB1_SetLow(); 
                break;
        case(2):
                ROW2_RB2_SetLow(); 
                break;        
        case(3):
                ROW3_RB3_SetLow(); 
                break;
        case(4):
                ROW4_RB4_SetLow(); 
                break;
        case(5):              
                ROW5_RB5_SetLow(); 
                break;
        case(6):
                ROW6_RB6_SetLow(); 
                break;
        case(7):        
                ROW7_RB7_SetLow(); 
                break;        
        case(8):
                ROW8_RE0_SetLow(); 
                break;
        case(9): 
                ROW9_RE1_SetLow(); 
                break;        
        case(10): 
                ROW10_RE2_SetLow(); 
                break;            
                
        default:
                break;
    }
    
   
}




/******************************************************************************
 * Function:        SetColsInput(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetColsInput(uint8_t ncol)
{
    
}



/******************************************************************************
 * Function:       ReadColStatus(uint8_t ncol)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
uint8_t  ReadColStatus(uint8_t ncol)
{
   uint8_t ret_val;
   ret_val=1;
   switch(ncol){
       case(0):
               ret_val=COL0_RD0_GetValue();
               break;
       case(1):
               ret_val=COL1_RD1_GetValue();
               break;
       case(2):
               ret_val=COL2_RD2_GetValue();
               break;
       case(3):
               ret_val=COL3_RD3_GetValue();
               break;
       case(4):
               ret_val=COL4_RD4_GetValue();
               break;
       case(5):
               ret_val=COL5_RD5_GetValue();
               break;
       case(6):
               ret_val=COL6_RD6_GetValue();
               break;
       case(7):
               ret_val=COL7_RD7_GetValue();
               break;
       case(8):
               ret_val=COL8_RC6_GetValue();
               break;
       case(9):
               ret_val=COL9_RC7_GetValue();
               break;        
       default:
           
               break;
   }
   return(ret_val);
}


/******************************************************************************
 * Function:       SetLeds()
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void SetLeds(void)
{//uint8_t ncol,nrow;
    uint8_t key_code,key_modifier;
    if(num_keys_pressed>1){//probably MODIFIERS pressed
       //if(keys_special.numLock==1){}
       key_code= keys_multi[1];
       key_modifier=keys_multi[0];
//       if(key_code==KEY_RIGHTSHIFT){
//        keys_multi[1]=key_modifier;
//        //NUMLOCK_LED2_SetHigh(); 
//        keys_multi[0]=key_code;
//       }else if(key_modifier==KEY_RIGHTSHIFT){
//           keys_multi[0]=key_modifier;
//           //CAPSLOCK_LED1_SetHigh(); 
//           keys_multi[1]=key_code;
//       }
       
       switch(key_code){
                           case (KEY_LEFTSHIFT):
                                                   keys_multi[1]=key_modifier;
                                                  
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_RIGHTSHIFT):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_LEFTCTRL):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_RIGHTCTRL):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;  
                           case (KEY_LEFTALT):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_RIGHTALT)://ALT+CTRL =AltGr
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_LEFTMETA):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;
                           case (KEY_RIGHTMETA):
                                                   keys_multi[1]=keys_multi[0];
                                                   keys_multi[0]=key_code;
                                                   break;     
                                        default:
                                                   //NUMLOCK_LED2_SetLow(); 
                                                  
                                                   break;
        }

       
   }else if(num_keys_pressed==1){
              switch (keys_multi[0]){
           case(KEY_CAPSLOCK):
                       //CAPSLOCK_LED1_SetHigh(); 
                       // CAPSLOCK_LED1_Toggle();
                       break;
           case(KEY_NUMLOCK):
                       //NUMLOCK_LED2_SetHigh(); 
                       //NUMLOCK_LED2_Toggle(); 
                       break;   
           case(KEY_F8):
                    if(duty_pwm_backlight <100){
                        duty_pwm_backlight++;
                    }
                    
                    break;     
           case(KEY_F11):
                    if(duty_pwm_backlight>0){
                        duty_pwm_backlight--;
                    }
                    
                    break;            
           default:
                
                  break;  
       }
   }
}

/******************************************************************************
 * Function:       KeyboardScanRead(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void KeyboardScanRead(void)
{
  uint8_t nrow,ncol;
  uint8_t pos_key=0;
  //uint8_t ret_val;
  //uint8_t key_val;
 
  num_keys_pressed=0;  
  //CAPSLOCK_LED1_Toggle(); 
  LATB=0xFF;
  LATE=0x07;
  TRISC = 0xC0;
  TRISD = 0xFF;
  for(nrow=0;nrow< NUM_ROW_MAX;nrow++){
      SetRowsInput(nrow);
      for(ncol=0;ncol < NUM_COL_MAX;ncol++){
           //key_val
           pos_key =(nrow*NUM_COL_MAX)+ncol; 
           kb_status[nrow][ncol].old=kb_status[nrow][ncol].val;        
           kb_status[nrow][ncol].val=ReadColStatus(ncol);    
           if (kb_status[nrow][ncol].old==kb_status[nrow][ncol].val){
               if (kb_status[nrow][ncol].deb_ms > KEY_PRESSED_DEBOUNCE_MS){
                  kb_status[nrow][ncol].deb_ms = KEY_PRESSED_DEBOUNCE_MS; 
               }
               kb_status[nrow][ncol].deb_ms++;
           }else{
               kb_status[nrow][ncol].deb_ms=0;
           }
           //ret_val=1;
           //ret_val=ReadColStatus(ncol); 
           if (kb_status[nrow][ncol].val==KEY_PRESSED_VALUE){
               //NUMLOCK_LED2_SetHigh(); 
               //if (kb_status[nrow][ncol].deb_ms > KEY_PRESSED_DEBOUNCE_MS){
               if(keys_special.numLock==1){
                  keys_multi[num_keys_pressed]=KEYBOARD1_SCANCODE_1[pos_key];  
               }else{
                  keys_multi[num_keys_pressed]=KEYBOARD1_SCANCODE[pos_key];
               }
               num_keys_pressed++;
               //}
               
           }
           //else if (num_keys_pressed==0){
           //    NUMLOCK_LED2_SetLow(); 
           // }
      }
  }  
   
    
}




// *****************************************************************************
// *****************************************************************************
// Section: Macros or Functions
// *****************************************************************************
// *****************************************************************************
void APP_KeyboardInit(void)
{
    //initialize the variable holding the handle for the last
    // transmission
    keyboard.lastINTransmission = 0;
    
    keyboard.key = 4;
    keyboard.waitingForRelease = false;

    //Set the default idle rate to 500ms (until the host sends a SET_IDLE request to change it to a new value)
    keyboardIdleRate = 500;

    mouse.movementMode = true;
    
    /* initialize the handles to invalid so we know they aren't being used. */
    mouse.inputReport[0].handle = NULL;

    mouse.sentStop = false;

    mouse.movementCount = 0;

#ifdef    USB_USE_HID_MOUSE
    //enable the HID endpoint
    USBEnableEndpoint(HID_EP2,USB_IN_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
#endif
 
#ifdef    USB_USE_HID_KEYBOARD
    //enable the HID endpoint
    USBEnableEndpoint(HID_EP1, USB_IN_ENABLED|USB_OUT_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);
    
    //Arm OUT endpoint so we can receive caps lock, num lock, etc. info from host
    keyboard.lastOUTTransmission = HIDRxPacket(HID_EP1,(uint8_t*)&outputReport, sizeof(outputReport) );

#endif
   
}

void APP_KeyboardTasks(void)
{
  
    unsigned char i,pos_no_mod;
    bool needToSendNewReportPacket;

    
    
    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if( USBGetDeviceState() < CONFIGURED_STATE )
    {
        return;
    }

    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if( USBIsDeviceSuspended()== true )
    {
        //Check if we should assert a remote wakeup request to the USB host,
        //when the user presses the pushbutton.
//        if(BUTTON_IsPressed(BUTTON_USB_DEVICE_REMOTE_WAKEUP) == 0)
//        {
//            //Add code here to issue a resume signal.
//        }

        return;
    }

    needToSendNewReportPacket = false;
    /* Check if the IN endpoint is busy, and if it isn't check if we want to send
     * keystroke data to the host. */
    if(HIDTxHandleBusy(keyboard.lastINTransmission) == false)
    {
        /* Clear the INPUT report buffer.  Set to all zeros. */
        memset(&inputReport, 0, sizeof(inputReport));

        if(num_keys_pressed>0)//(BUTTON_IsPressed(BUTTON_USB_DEVICE_HID_KEYBOARD_KEY) == true)
        {
            //if((keyboard.waitingForRelease == false)||(oldInputReport.keys[0]!=keys_multi[0]))
            //{
                needToSendNewReportPacket  = true;
                keyboard.waitingForRelease = true;
 
                if(num_keys_pressed>1){//&&(keys_special.numLock==0)){
                    i=(num_keys_pressed);//-1);
                    pos_no_mod=i-1;
                    while(i>0){ 
                       switch(keys_multi[i-1]){
                           case (KEY_LEFTSHIFT):
                                                   //duty_pwm_backlight=50;   
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.leftShift=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTSHIFT):     
                                                   //duty_pwm_backlight=100;        
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.rightShift=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_LEFTCTRL):
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.leftControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTCTRL):
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.rightControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;  
                           case (KEY_LEFTALT):
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   //inputReport.modifiers.bits.leftAlt=1; 
                                                   if(keys_special.numLock==0){
                                                      inputReport.modifiers.bits.leftAlt=1;
                                                   }else{
                                                      inputReport.modifiers.bits.leftAlt=1;  
//                                                      if(num_keys_pressed==2){
//                                                         if((keys_multi[1]>KEY_KPENTER)&&(keys_multi[1]<KEY_KPDOT)){ 
//                                                            if(keyboard.waitingAltRelease == false){
//                                                                inputReport.modifiers.bits.leftAlt=1; 
//                                                                keyboard.waitingAltRelease = true;                                                           
//                                                            } 
//                                                            //inputReport.keys[0] = 0;
//                                                            keypad_multi[id_kp++]= keys_multi[1];
//                                                         }  
//                                                       }  
                                                   } 
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTALT)://ALT+CTRL =AltGr
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.rightAlt=1;
                                                   inputReport.modifiers.bits.rightControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_LEFTMETA):
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.leftGUI=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTMETA):
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   inputReport.modifiers.bits.rightGUI=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;     
                                        default:
                                            
                                                   inputReport.keys[0] = keys_multi[pos_no_mod];
                                                   pos_no_mod=i-1;
                                                   //duty_pwm_backlight=0;
                                                   break;
                       } 
                       i--;
                    } 
                  }else{
                        /* Set the only important data, the key press data. */
                       inputReport.keys[0] = keys_multi[0];
                       
                       switch(keys_multi[0]){
                         
                           case (KEY_KPEQUAL):
                                                   if(keys_special.numLock==1){
                                                     inputReport.keys[0] = KEY_0;  
                                                     inputReport.modifiers.bits.leftShift=1;
                                                   }
                                                   break;
                           case (KEY_MAJOR):
                                                  
                                                   if(keys_special.numLock==1){
                                                     inputReport.keys[0] = KEY_102_MINOR;  
                                                     inputReport.modifiers.bits.leftShift=1;
                                                   }
                                                   break;
                                                   
                               
                           case (KEY_LEFTMETA):
                                                  
                                                   inputReport.modifiers.bits.leftGUI=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTMETA):
                                                   
                                                   inputReport.modifiers.bits.rightGUI=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;     
                           case (KEY_LEFTSHIFT):
                                                   //duty_pwm_backlight=50;   
                                                  
                                                   inputReport.modifiers.bits.leftShift=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTSHIFT):     
                                                   //duty_pwm_backlight=100;        
                                                 
                                                   inputReport.modifiers.bits.rightShift=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_LEFTCTRL):
                                                   
                                                   inputReport.modifiers.bits.leftControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTCTRL):
                                                  
                                                   inputReport.modifiers.bits.rightControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;  
                           case (KEY_LEFTALT):
                                                   if(keys_special.numLock==0){
                                                      inputReport.modifiers.bits.leftAlt=1;
                                                   }else{
                                                      
                                                         if((keyboard.waitingAltRelease == false)||(id_kp==0)){
                                                             inputReport.modifiers.bits.leftAlt=1; 
                                                             keyboard.waitingAltRelease = true; 
                                                         }
                                                   }
                                                   
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                           case (KEY_RIGHTALT)://ALT+CTRL =AltGr
                                                  
                                                   inputReport.modifiers.bits.rightAlt=1;
                                                   inputReport.modifiers.bits.rightControl=1;
                                                   //keyboard.waitingForRelease = false;
                                                   break;
                            
                                        default:
                                            
                                                  
                                                   break;
                       } 
                      
                       
                       //duty_pwm_backlight=0;
                  }
                
                /* Set the only important data, the key press data. */
                //inputReport.keys[0] = keys_multi[0];//keyboard.key++;//
               
                //In this simulated keyboard, if the last key pressed exceeds the a-z + 0-9,
                //then wrap back around so we send 'a' again.
               // if(keyboard.key == 40)
               // {
               //     keyboard.key = 4;
               // }
            //}
            oldInputReport.keys[0]=keys_multi[0];//
           
          
        }
        else 
        { 
            if(keyboard.waitingForRelease == true)
            {
              needToSendNewReportPacket  = true;  
            } 
            if(keyboard.waitingAltRelease == true){
//                 inputReport.modifiers.bits.leftAlt=1; 
//                 inputReport.keys[0] =  keypad_multi[2];
//                 inputReport.keys[1] =  keypad_multi[1];
//                 inputReport.keys[2] =  keypad_multi[0]; 
            }
            id_kp=0;
            keyboard.waitingForRelease = false;
            keyboard.waitingAltRelease = false;
        }

        //Check to see if the new packet contents are somehow different from the most
        //recently sent packet contents.
        //needToSendNewReportPacket = false;
//        for(i = 0; i < sizeof(inputReport); i++)
//        {
//            if(*((uint8_t*)&oldInputReport + i) != *((uint8_t*)&inputReport + i))
//            { 
//                needToSendNewReportPacket = true;
//                break;
//            }
//        }
       
        //Check if the host has set the idle rate to something other than 0 (which is effectively "infinite").
        //If the idle rate is non-infinite, check to see if enough time has elapsed since
        //the last packet was sent, and it is time to send a new repeated packet or not.
        if (keyboardIdleRateMillisec==0) //if(keyboardIdleRate != 0)
        {
           //Check if the idle rate time limit is met.  If so, need to send another HID input report packet to the host
           // if(TimeDeltaMilliseconds >= keyboardIdleRate)
           // {
                needToSendNewReportPacket = true;
           // }
             // NUMLOCK_LED2_Toggle();  
             //keyboardIdleRateMillisec=KEYBOARD_TIMEOUT_IDLE;   
        }

        //Now send the new input report packet, if it is appropriate to do so (ex: new data is
        //present or the idle rate limit was met).
        if(needToSendNewReportPacket == true)
        {
            //Save the old input report packet contents.  We do this so we can detect changes in report packet content
            //useful for determining when something has changed and needs to get re-sent to the host when using
            //infinite idle rate setting.
            //oldInputReport = inputReport;
            //NUMLOCK_LED2_Toggle(); 
            /* Send the 8 byte packet over USB to the host. */
            keyboard.lastINTransmission = HIDTxPacket(HID_EP2, (uint8_t*)&inputReport, sizeof(inputReport));
            keyboardIdleRateMillisec=KEYBOARD_TIMEOUT_IDLE;  
            //OldSOFCount = LocalSOFCount;    //Save the current time, so we know when to send the next packet (which depends in part on the idle rate setting)
        }

    }//if(HIDTxHandleBusy(keyboard.lastINTransmission) == false)


    /* Check if any data was sent from the PC to the keyboard device.  Report
     * descriptor allows host to send 1 byte of data.  Bits 0-4 are LED states,
     * bits 5-7 are unused pad bits.  The host can potentially send this OUT
     * report data through the HID OUT endpoint (EP1 OUT), or, alternatively,
     * the host may try to send LED state information by sending a SET_REPORT
     * control transfer on EP0.  See the USBHIDCBSetReportHandler() function. */
    if (HIDRxHandleBusy(keyboard.lastOUTTransmission) == false)
    {
        APP_KeyboardProcessOutputReport();

        keyboard.lastOUTTransmission = HIDRxPacket(HID_EP2,(uint8_t*)&outputReport,sizeof(outputReport));
    }
    
    return;		
}

static void APP_KeyboardProcessOutputReport(void)
{
    if(outputReport.leds.capsLock)
    {
        //LED_On(LED_USB_DEVICE_HID_KEYBOARD_CAPS_LOCK);
        // NUMLOCK_LED2_Toggle(); 
         CAPSLOCK_LED1_SetHigh();
         keys_special.capsLock=1;
    }
    else
    {
         CAPSLOCK_LED1_SetLow(); 
         keys_special.capsLock=0;
        //LED_Off(LED_USB_DEVICE_HID_KEYBOARD_CAPS_LOCK);
    }
    if(outputReport.leds.numLock)
    {
        //LED_On(LED_USB_DEVICE_HID_KEYBOARD_CAPS_LOCK);
        // NUMLOCK_LED2_Toggle(); 
         NUMLOCK_LED2_SetHigh();
         keys_special.numLock=1;
    }
    else
    {
         NUMLOCK_LED2_SetLow();
         keys_special.numLock=0;
        //LED_Off(LED_USB_DEVICE_HID_KEYBOARD_CAPS_LOCK);
    }
}

static void USBHIDCBSetReportComplete(void)
{
    /* 1 byte of LED state data should now be in the CtrlTrfData buffer.  Copy
     * it to the OUTPUT report buffer for processing */
    outputReport.value = CtrlTrfData[0];

    /* Process the OUTPUT report. */
    APP_KeyboardProcessOutputReport();
}

void USBHIDCBSetReportHandler(void)
{
    /* Prepare to receive the keyboard LED state data through a SET_REPORT
     * control transfer on endpoint 0.  The host should only send 1 byte,
     * since this is all that the report descriptor allows it to send. */
    USBEP0Receive((uint8_t*)&CtrlTrfData, USB_EP0_BUFF_SIZE, USBHIDCBSetReportComplete);
}


//Callback function called by the USB stack, whenever the host sends a new SET_IDLE
//command.
void USBHIDCBSetIdleRateHandler(uint8_t reportID, uint8_t newIdleRate)
{
    //Make sure the report ID matches the keyboard input report id number.
    //If however the firmware doesn't implement/use report ID numbers,
    //then it should be == 0.
    if(reportID == 0)
    {
        //NUMLOCK_LED2_Toggle();
        keyboardIdleRate = newIdleRate;
    }
}


/*********************************************************************
* Function: void APP_DeviceMouseTasks(void);
*
* Overview: Keeps the demo running.
*
* PreCondition: The demo should have been initialized and started via
*   the APP_DeviceMouseInitialize() and APP_DeviceMouseStart() demos
*   respectively.
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceMouseTasks(void)
{
    bool currentButtonState;

    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if( USBGetDeviceState() < CONFIGURED_STATE )
    {
        return;
    }

    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if( USBIsDeviceSuspended() == true )
    {
        return;
    }
    
    /* Get the current button state */
    currentButtonState = 0;//BUTTON_IsPressed(BUTTON_USB_DEVICE_HID_MOUSE);

    /* If the button state has changed since the last time we checked it, then
     * we enable/disable movement mode.
     */
    if(mouse.lastButtonState != currentButtonState)
    {
        mouse.lastButtonState = currentButtonState;

        if(currentButtonState == false)
        {
            mouse.movementMode = !mouse.movementMode;

            mouse.inputReport[0].idleRateSofCount = 0;
            mouse.sentStop = false;
        }
    }
}//end ProcessIO

void APP_DeviceMouseIdleRateCallback(uint8_t reportId, uint8_t idleRate)
{
    //Make sure the host is requesting to set the idleRate on a legal/implemented
    //report ID.  In applications that don't implement report IDs (such as this
    //firmware) the value should be == 0.
    if(reportId == 0)
    {
        mouse.inputReport[reportId].idleRate = idleRate;
    }
}

/*******************************************************************************
 * Function: void APP_DeviceMouseSOFHandler(void)
 *
 * Overview: Handles SOF events.  This is used to calculate the mouse movement
 *           based on the SOF counter instead of a device timer or CPU clocks.
 *           It can also be used to handle idle rate issues, if applicable for
 *           the demo.
 *
 * Input: none
 * Output: none
 *
 ******************************************************************************/
void APP_DeviceMouseSOFHandler(void)
{
    /* We will be getting SOF packets before we get the SET_CONFIGURATION
     * packet that will configure this device, thus, we need to make sure that
     * we are actually initialized and open before we do anything else,
     * otherwise we should exit the function without doing anything.
     */
    if(USBGetDeviceState() != CONFIGURED_STATE)
    {
        return;
    }

    /* On each SOF, which should happen nearly every millisecond, check to see
     * if our idle duration has expired.  If it has, then we need to send a
     * repeat report.
     *
     * Note here that the idle rate duration specified by the host is as
     * follows:
     *
     * if the duration == 0, then it is indefinate duration
     * if the duration != 0, then it specifies the number of 4ms blocks between
     *   duplicate reports.  Thus an idle rate duration of 1 repeats reports
     *   every 4ms even if none of the data has changed.
     *
     * Here are the cases where we are suppose to transmit data:
     * 1) The report data has changed since the last time we sent a report (any
     *    movement or button press/release in the case of a mouse).
     * 2) The idle rate duration counter has expired and there hasn't been any
     *    change in data, then we are are going resend the same data.  Note here
     *    that if the idle rate duration = 0, then it is infinite so you would
     *    thus never send a duplicate report.
     */

    /* We can only send a report if the last report has been sent.
     */
    if(HIDTxHandleBusy(mouse.inputReport[0].handle) == false)
    {
        /* We are going to move a certain number of pixels in each direction.
         * The last transmission was successful so let's count it now.
         */
 //       mouse.movementCount++;

        /* Once we've reached the specified number of pixels moved, we'll move
         * to the next direction in the position array.
         */
//        if(mouse.movementCount == MOUSE_MOVEMENT_DISTANCE)
//        {
//            mouse.movementCount = 0;
//
//            mouse.vectorPosition++;
//            if(mouse.vectorPosition >= sizeof(xVector))
//            {
//                mouse.vectorPosition = 0;
//            }
//        }

        /* Here we will provide a way for the application to disable the mouse
         * mouse momement by pressing a button.  If emulateMode == true, then we
         * are emulating the mouse, if == false, then make it so there is no
         * movement.
         */
//        if(mouse.movementMode == true)
//        {
//            mouseReport.buttons.button1 = 0;
//            mouseReport.buttons.button2 = 0;
//            mouseReport.buttons.button3 = 0;
//
//            mouseReport.x = xVector[mouse.vectorPosition];
//            mouseReport.y = yVector[mouse.vectorPosition];
//        }
//        else
//        {
            mouseReport.buttons.value =  mouseBuffer[0];
            mouseReport.x = mouseBuffer[1];//0;
            mouseReport.y = mouseBuffer[2];//0;//

            if(mouse.inputReport[0].idleRate != 0)
            {
                mouse.inputReport[0].idleRateSofCount++;
            }
            
            /* if we haven't sent this "stop" packet yet, then we need to send
             * it regardless of the current idle count because it represents a
             * change in value.  If we have already sent the "stop" then we
             * need to check the idle rate counter before we send it again.
             */
            if(mouse.sentStop == true)
            {
                /* If we have haven't exceeded the idle rate yet, then we don't
                 * need to send a repeat report.  This needs to be "<=" because
                 * both the count and the rate can be set to 0 if the host
                 * selects an indefiniate duration.
                 */
                if(mouse.inputReport[0].idleRateSofCount <= mouse.inputReport[0].idleRate)
                {
                    return;
                }
            }
  //      }
        
        mouse.inputReport[0].handle = HIDTxPacket(HID_EP1,(uint8_t*)&mouseReport, sizeof(mouseReport));
    }
}






/*******************************************************************************
 End of File
*/