

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CAN_PROTO_H
#define	CAN_PROTO_H

#include <xc.h> // include processor files - each processor file is guarded. 

#endif	/* XC_HEADER_TEMPLATE_H */

