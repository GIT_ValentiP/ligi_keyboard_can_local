/**
  SW01_IN01 Generated Driver API Header File

  @Company
    Microchip Technology Inc.

  @File Name
    sw01_in01.h

  @Summary
    This is the generated header file for the SW01_IN01 driver using Board Support Library

  @Description
    This header file provides APIs for driver for SW01_IN01.
    Generation Information :
        Product Revision  :  Board Support Library - 0.95.1
        Device            :  dsPIC33EP256GM710
        Driver Version    :  0.95
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef SW01_IN01_H
#define SW01_IN01_H

#include <stdbool.h>
#include <xc.h>

static inline bool SW01_IN01_IsPressed() {return (_RC2 == false);}
static inline bool SW02_IN02_IsPressed() {return (_RC11 == false);}
static inline bool SW03_IN03_IsPressed() {return (_RG11 == false);}
static inline bool SW04_IN04_IsPressed() {return (_RF13 == false);}
static inline bool SW05_IN05_IsPressed() {return (_RF12 == false);}
static inline bool SW06_IN06_IsPressed() {return (_RE12 == false);}
static inline bool SW07_IN07_IsPressed() {return (_RE13 == false);}
static inline bool SW08_IN08_IsPressed() {return (_RE14 == false);}
static inline bool SW09_IN09_IsPressed() {return (_RE15 == false);}
static inline bool SW10_IN10_IsPressed() {return (_RD14 == false);}
static inline bool SW11_IN11_IsPressed() {return (_RD15 == false);}
static inline bool SW12_IN12_IsPressed() {return (_RA4 == false);}
static inline bool SW13_IN13_IsPressed() {return (_RE1 == false);}
static inline bool SW14_IN14_IsPressed() {return (_RA9 == false);}

static inline bool SW15_INB01_IsPressed() {return (_RC3 == false);}
static inline bool SW16_INB02_IsPressed() {return (_RG2 == false);}
static inline bool SW17_INB03_IsPressed() {return (_RG3 == false);}
static inline bool SW18_INB04_IsPressed() {return (_RF4 == false);}
static inline bool SW19_INB05_IsPressed() {return (_RF5 == false);}
static inline bool SW20_INB06_IsPressed() {return (_RA14 == false);}
static inline bool SW21_INB07_IsPressed() {return (_RA15 == false);}
static inline bool SW22_INB08_IsPressed() {return (_RD8 == false);}
static inline bool SW23_INB09_IsPressed() {return (_RB5 == false);}
static inline bool SW24_INB10_IsPressed() {return (_RB6 == false);}
static inline bool SW25_INB11_IsPressed() {return (_RC10 == false);}
static inline bool SW26_INB12_IsPressed() {return (_RB7 == false);}
static inline bool SW27_INB13_IsPressed() {return (_RC13 == false);}
static inline bool SW28_INB14_IsPressed() {return (_RB8 == false);}
#endif	/* SW01_IN01_H */