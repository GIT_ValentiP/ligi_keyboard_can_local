
/**
 * \file EEPROM_driver.c
 * \brief Source file containing implementations for EEPROM API Function Library with MCC foundation services
 * \brief v3.0
 * \author m16133
 * \date Jan 2019
 *
 * @see www.microchip.com
 */
 /**
  Generated Source File

  Company:
    Microchip Technology Inc.

    (c) 2018 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

 
#include <xc.h>
#include <stdbool.h>

#include "EEPROM_driver.h"
#include "drivers/i2c_types.h"
#include "drivers/i2c_master.h"

uint8_t    EEPROM_DEVICE_ADDRESS=0x50;

typedef struct
{
    uint8_t *data;
    uint16_t *dataSize;
} buffer_t;

uint8_t EEPROM_CurrentAddressRead(void){
    return(EEPROM_DEVICE_ADDRESS);
}
void EEPROM_SetDeviceAddress(uint8_t MSBAddress)
{
    switch(MSBAddress)
    {
        case 0x0:
            EEPROM_DEVICE_ADDRESS = (0xA8 >> 1);       
            break;
        case 0x1:
            EEPROM_DEVICE_ADDRESS = (0xAA >> 1);
            break;
        case 0x2:
            EEPROM_DEVICE_ADDRESS = (0xAC >> 1);
            break;
        case 0x3:
            EEPROM_DEVICE_ADDRESS = (0xAE >> 1);
            break;
        default:
            break;        
    }
}

void EEPROM_ResetDeviceAddress()
{
    switch(EEPROM_DEVICE_ADDRESS)
    {
        case 0x54: 
            EEPROM_DEVICE_ADDRESS = (0xAA >> 1);  
            break;
        case 0x55:
            EEPROM_DEVICE_ADDRESS = (0xAC >> 1);
            break;
        case 0x56:
            EEPROM_DEVICE_ADDRESS = (0xAE >> 1);
            break;
        case 0x57:
            EEPROM_DEVICE_ADDRESS = (0xA8 >> 1);
            break;
        default:
            break;        
    }
}


static i2c_operations_t readOneByteHandler(void *p)
{
    buffer_t *buffer = p;
    i2c_setBuffer(buffer->data,1);
    i2c_setDataCompleteCallback(NULL,NULL);
    return i2c_restart_read;
}

uint8_t EEPROM_ReadOneByte(uint32_t address)
{
    uint8_t data;
    while(!i2c_open(EEPROM_DEVICE_ADDRESS)); // sit here until we get the bus..
    i2c_setDataCompleteCallback(readOneByteHandler,&data);
    address = (address&0xFF00)>>8|(address&0x00FF)<<8;
    i2c_setBuffer(&address,2);
    i2c_setAddressNACKCallback((i2c_callback)i2c_restart_write,NULL); //NACK polling?
    i2c_masterWrite();
    while(I2C_BUSY == i2c_close()); // sit here until finished
    return data;
}

static i2c_operations_t sequentialWriteHandler(void *p)
{
    buffer_t *buffer = p;
    i2c_setBuffer(buffer->data,(size_t)buffer->dataSize);
    i2c_setDataCompleteCallback(NULL,NULL);
    return i2c_continue;
}

void EEPROM_SequentialWrite (uint32_t writeAddress, uint8_t * data, uint32_t count)
{
    sequentialBuffer_t buffer = {0};
    buffer.data = data;
    *buffer.dataCount =(uint8_t)count;

    while(!i2c_open(EEPROM_DEVICE_ADDRESS)); // sit here until we get the bus..
    i2c_setDataCompleteCallback(sequentialWriteHandler,&buffer);
    writeAddress = (writeAddress&0xFF00)>>8|(writeAddress&0x00FF)<<8;
    i2c_setBuffer(&writeAddress,2);
    i2c_setAddressNACKCallback((i2c_callback)i2c_restart_write,NULL); //NACK polling?
    i2c_masterWrite();
    while(I2C_BUSY == i2c_close()); // sit here until finished.
}

void EEPROM_PageWrite	   (uint32_t writeAddress, uint8_t * data)
{   
    return EEPROM_SequentialWrite ( writeAddress, data, PAGESIZE);
}
static i2c_operations_t sequentialReadHandler(void *p)
{   
    buffer_t *buffer = p;
    i2c_setBuffer(buffer->data,(size_t)buffer->dataSize);
    i2c_setDataCompleteCallback(NULL,NULL);
    return i2c_restart_read;
}


uint8_t EEPROM_SequentialRead  ( uint32_t readAddress, uint8_t * data, uint32_t count)
{  
    
    buffer_t buffer = {0};
    buffer.data = data;
    *buffer.dataSize =(uint16_t)count;
    
    while(!i2c_open(EEPROM_DEVICE_ADDRESS));
    i2c_setDataCompleteCallback(sequentialReadHandler,&buffer);
    readAddress = (readAddress&0xFF00)>>8|(readAddress&0x00FF)<<8;
    i2c_setBuffer(&readAddress,2);
    i2c_setAddressNACKCallback((i2c_callback)i2c_restart_write,NULL); //NACK polling?
    i2c_masterWrite();
    while(I2C_BUSY == i2c_close()); // sit here until finished.
    return (1);
}  



  