/**
  CAN1 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    can1.c

  @Summary
    This is the generated driver implementation file for the CAN1 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This source file provides APIs for CAN1.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  dsPIC33EP256GM710
        Driver Version    :  1.00
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB 	          :  MPLAB X v5.30
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include <string.h>
#include "../globals.h"
#include "can1.h"
#include "dma.h"

#define CAN1_TX_DMA_CHANNEL DMA_CHANNEL_0
#define CAN1_RX_DMA_CHANNEL DMA_CHANNEL_1

/* Valid options are 4, 6, 8, 12, 16, 24, or 32. */
#define CAN1_MESSAGE_BUFFERS         8

#define CAN1_TX_BUFFER_COUNT 4

#define FCAN    60000000
#define BITRATE 500000
#define NTQ     20  // 20 Time Quanta in a Bit Time
#define BRP_VAL ( (FCAN / (2 * NTQ * BITRATE)) - 1 )

/******************************************************************************/

/******************************************************************************/
/* Private type definitions                                               */
/******************************************************************************/
typedef struct __attribute__((packed))
{
    unsigned priority                   :2;
    unsigned remote_transmit_enable     :1;
    unsigned send_request               :1;
    unsigned error                      :1;
    unsigned lost_arbitration           :1;
    unsigned message_aborted            :1;
    unsigned transmit_enabled           :1;
} CAN1_TX_CONTROLS;

/******************************************************************************/
/* Private variable definitions                                               */
/******************************************************************************/
extern const CanRxObjectDescriptor_t CanRxObjectDescriptor[RX_BUFF_DIM];
/* This alignment is required because of the DMA's peripheral indirect 
 * addressing mode. */
static unsigned int can1msgBuf [CAN1_MESSAGE_BUFFERS][8] __attribute__((aligned(8 * 8 * 2)));

can_msg_buff_t can_msg_buff[SIZE_OF_CAN_BUFF] __attribute__((aligned(8 * 2)));
/******************************************************************************/
/* Private function prototypes                                                */
/******************************************************************************/
static void CAN1_DMACopy(uint8_t buffer_number, uCAN_MSG *message);
static void CAN1_MessageToBuffer(uint16_t* buffer, uCAN_MSG* message);

void __attribute__((__interrupt__, no_auto_psv)) _C1Interrupt(void)  
{   

     uint8_t i=0; 
    
    for(i=TX_BUFF_DIM; i<(RX_BUFF_DIM+TX_BUFF_DIM); i++)
    {
        if( (C1RXFUL1 & (1<<i)) )
        {
            memcpy(CanRxObjectDescriptor[i-TX_BUFF_DIM].msg->byte,(uint8_t *)&can_msg_buff[i].byteBuff[6],8);  
            C1RXFUL1 &= (~(1<<i)); 
        }
    }
    
    IFS2bits.C1IF = 0;      // clear interrupt flag
    C1INTFbits.RBIF = 0;
   
}

/**
  Section: CAN1 APIs
*****************************************************************************************/
  


void CAN1_Initialize(void)
{
    // Disable interrupts before the Initialization
    IEC2bits.C1IE = 0;
    C1INTE = 0;

    // set the CAN1_initialize module to the options selected in the User Interface

    /* put the module in configuration mode CAN_CONFIGURATION_MODE */
    C1CTRL1bits.REQOP = CAN_CONFIGURATION_MODE;
    while(C1CTRL1bits.OPMODE != CAN_CONFIGURATION_MODE);

    /* Set up the baud rate*/	
    //C1CFG1 CANx BAUD RATE CONFIGURATION REGISTER 1
    C1CFG1bits.SJW          = 0x3;//C1CFG1 = 0x03;	//BRP TQ = (2 x 4)/FCAN; SJW 3 x TQ; 
    C1CFG1bits.BRP          = BRP_VAL;
                            //SJW<1:0>: Synchronization Jump Width bits
                                //11 = Length is 4 x TQ
                                //10 = Length is 3 x TQ
                                //01 = Length is 2 x TQ
                                //00 = Length is 1 x TQ
                            //BRP<5:0>: Baud Rate Prescaler bits
                                //11 1111 = TQ = 2 x 64 x 1/FCAN
                                //xx xxxx
                                //00 0010 = TQ = 2 x 3 x 1/FCAN
                                //00 0001 = TQ = 2 x 2 x 1/FCAN
                                //00 0000 = TQ = 2 x 1 x 1/FCAN
    
    //C1CFG2 CANx BAUD RATE CONFIGURATION REGISTER 2
    C1CFG2bits.SEG1PH       = 0x7;// SEG1PH 7 x TQ;
    C1CFG2bits.SEG2PHTS     = 0x1;// SEG2PHTS Freely programmable;
    C1CFG2bits.SEG2PH       = 0x5;// SEG2PH 5 x TQ;
    C1CFG2bits.PRSEG        = 0x4;// PRSEG 4 x TQ; 
    C1CFG2bits.SAM          = 0x1;// SAM Once at the sample point; 
    C1CFG2bits.WAKFIL       = 0x0;//WAKFIL disabled;
                            //WAKFIL: Select CAN Bus Line Filter for Wake-up bit
                                    //1 = Uses CAN bus line filter for wake-up
                                    //0 = CAN bus line filter is not used for wake-up
                            //SEG2PH<2:0>: Phase Segment 2 bits
                                    //111 = Length is 8 x TQ
                                    //xxx
                                    //000 = Length is 1 x TQ
                            //SEG2PHTS: Phase Segment 2 Time Select bit
                                    //1 = Freely programmable
                                    //0 = Maximum of SEG1PHx bits or Information Processing Time (IPT), whichever is greater
                            //SAM: Sample of the CAN Bus Line bit
                                    //1 = Bus line is sampled three times at the sample point
                                    //0 = Bus line is sampled once at the sample point
                            //SEG1PH<2:0>: Phase Segment 1 bits
                                    //111 = Length is 8 x TQ
                                    //xxx
                                    //000 = Length is 1 x TQ
                            //PRSEG<2:0>: Propagation Time Segment bits
                                    //111 = Length is 8 x TQ
                                    //xxx
                                    //000 = Length is 1 x TQ   
    
    //C1FCTRL: CANx FIFO CONTROL REGISTER
    C1FCTRLbits.DMABS       = 0x4;//16buffers inRAM  //C1FCTRL = 0x4001;	//FSA Transmit/Receive Buffer TRB1; DMABS 8; 
    C1FCTRLbits.FSA         = 0x1f;
                            //DMABS<2:0>: DMA Buffer Size bits
                                //111 = Reserved
                                //110 = 32 buffers in RAM
                                //101 = 24 buffers in RAM
                                //100 = 16 buffers in RAM
                                //011 = 12 buffers in RAM
                                //010 = 8 buffers in RAM
                                //001 = 6 buffers in RAM
                                //000 = 4 buffers in RAM
                            //FSA<4:0>: FIFO Area Starts with Buffer bits
                                //11111 = Receive Buffer RB31
                                //11110 = Receive Buffer RB30
                                //xxxxx
                                //00001 = Transmit/Receive Buffer TRB1
                                //00000 = Transmit/Receive Buffer TRB0
  
    //C1CTRL1: CANx CONTROL REGISTER 1
    C1CTRL1 = 0x00;	//CANCKS FOSC/2; CSIDL disabled; ABAT disabled; REQOP Sets Normal Operation Mode; WIN Uses buffer window; CANCAP disabled;         
                    //CSIDL: CANx Stop in Idle Mode bit
                            //1 = Discontinues module operation when device enters Idle mode
                            //0 = Continues module operation in Idle mode
                    //ABAT: Abort All Pending Transmissions bit
                            //1 = Signals all transmit buffers to abort transmission
                            //0 = Module will clear this bit when all transmissions are aborted
                    //CANCKS: CANx Module Clock (FCAN) Source Select bit
                                //1 = FCAN is equal to 2 * FP
                                //0 = FCAN is equal to FP
                    //REQOP<2:0>: Request Operation Mode bits
                                //111 = Set Listen All Messages mode
                                //110 = Reserved
                                //101 = Reserved
                                //100 = Set Configuration mode
                                //011 = Set Listen Only mode
                                //010 = Set Loopback mode
                                //001 = Set Disable mode
                                //000 = Set Normal Operation mode
                    //OPMODE<2:0>: Operation Mode bits
                                //111 = Module is in Listen All Messages mode
                                //110 = Reserved
                                //101 = Reserved
                                //100 = Module is in Configuration mode
                                //011 = Module is in Listen Only mode
                                //010 = Module is in Loopback mode
                                //001 = Module is in Disable mode
                                //000 = Module is in Normal Operation mode                   
                    //CANCAP: CANx Message Receive Timer Capture Event Enable bit
                                //1 = Enables input capture based on CAN message receive
                                //0 = Disables CAN capture         
                    //WIN: SFR Map Window Select bit
                                //1 = Uses filter window
                                //0 = Uses buffer window 
   
    /* configure the device to interrupt on the receive buffer full flag */
    /* clear the buffer full flags */ 	
    //C1INTF: CANx INTERRUPT FLAG REGISTER
    C1INTFbits.RBIF = 0;  
                    //TXBO: Transmitter in Error State Bus Off bit
                            //1 = Transmitter is in Bus Off state
                            //0 = Transmitter is not in Bus Off state
                    //TXBP: Transmitter in Error State Bus Passive bit
                            //1 = Transmitter is in Bus Passive state
                            //0 = Transmitter is not in Bus Passive state
                    //RXBP: Receiver in Error State Bus Passive bit
                            //1 = Receiver is in Bus Passive state
                            //0 = Receiver is not in Bus Passive state
                    //TXWAR: Transmitter in Error State Warning bit
                            //1 = Transmitter is in Error Warning state
                            //0 = Transmitter is not in Error Warning state
                    //RXWAR: Receiver in Error State Warning bit
                            //1 = Receiver is in Error Warning state
                            //0 = Receiver is not in Error Warning state
                    //EWARN: Transmitter or Receiver in Error State Warning bit
                            //1 = Transmitter or receiver is in Error Warning state
                            //0 = Transmitter or receiver is not in Error Warning state
                    //IVRIF: Invalid Message Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //WAKIF: Bus Wake-up Activity Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //ERRIF: Error Interrupt Flag bit (multiple sources in CxINTF<13:8> register)
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //FIFOIF: FIFO Almost Full Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //RBOVIF: RX Buffer Overflow Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //RBIF: RX Buffer Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
                    //TBIF: TX Buffer Interrupt Flag bit
                            //1 = Interrupt request has occurred
                            //0 = Interrupt request has not occurred
    
    
    /*configure CAN1 Transmit/Receive buffer settings*/
    //set-up firts 4 buffer as trasmition buffer
    /*CAN TX Buffer ENABLE*/
    C1TR01CONbits.TXEN0 = 0x1; // Buffer 0 is a Transmit Buffer 
    C1TR01CONbits.TXEN1 = 0x1; // Buffer 1 is a Transmit Buffer 
    C1TR23CONbits.TXEN2 = 0x1; // Buffer 2 is a Transmit Buffer 
    C1TR23CONbits.TXEN3 = 0x1; // Buffer 3 is a Transmit Buffer 
//    C1TR45CONbits.TXEN4 = 0x0; // Buffer 4 is a Receive Buffer 
//    C1TR45CONbits.TXEN5 = 0x0; // Buffer 5 is a Receive Buffer 
//    C1TR67CONbits.TXEN6 = 0x0; // Buffer 6 is a Receive Buffer 
//    C1TR67CONbits.TXEN7 = 0x0; // Buffer 7 is a Receive Buffer 
      /*VSN TX Buffer PRIORITY*/
    C1TR01CONbits.TX0PRI = 0x3; // Message Buffer 0 Priority Level
    C1TR01CONbits.TX1PRI = 0x2; // Message Buffer 1 Priority Level
    C1TR23CONbits.TX2PRI = 0x1; // Message Buffer 2 Priority Level
    C1TR23CONbits.TX3PRI = 0x0; // Message Buffer 3 Priority Level
//    C1TR45CONbits.TX4PRI = 0x0; // Message Buffer 4 Priority Level
//    C1TR45CONbits.TX5PRI = 0x0; // Message Buffer 5 Priority Level
//    C1TR67CONbits.TX6PRI = 0x0; // Message Buffer 6 Priority Level
//    C1TR67CONbits.TX7PRI = 0x0; // Message Buffer 7 Priority Level
  
    /* put the module in normal mode CAN_NORMAL_OPERATION_MODE*/
    C1CTRL1bits.REQOP = CAN_NORMAL_OPERATION_MODE;
    while(C1CTRL1bits.OPMODE != CAN_NORMAL_OPERATION_MODE);	
	
    /* Enable CAN1 Interrupt */
    IEC2bits.C1IE = 1;

}

void canDrvSetFilters(uint32_t *id)
{
    /* Disable CAN1 Interrupt */
    IEC2bits.C1IE = 0;
    
    //set up mask
    //canChangeMode(CONFIGURATION_MODE);
     /* put the module in configuration mode CAN_CONFIGURATION_MODE */
    C1CTRL1bits.REQOP = CAN_CONFIGURATION_MODE;
    while(C1CTRL1bits.OPMODE != CAN_CONFIGURATION_MODE);
    
 /*MCC Filter  Configuration */  
        /*configure CAN1 Transmit/Receive buffer settings*/
        //set-up firts 4 buffer as trasmition buffer
        /*CAN TX Buffer ENABLE*/
//    C1TR01CONbits.TXEN0 = 0x1; // Buffer 0 is a Transmit Buffer 
//    C1TR01CONbits.TXEN1 = 0x1; // Buffer 1 is a Transmit Buffer 
//    C1TR23CONbits.TXEN2 = 0x1; // Buffer 2 is a Transmit Buffer 
//    C1TR23CONbits.TXEN3 = 0x1; // Buffer 3 is a Transmit Buffer 
//    C1TR45CONbits.TXEN4 = 0x0; // Buffer 4 is a Receive Buffer 
//    C1TR45CONbits.TXEN5 = 0x0; // Buffer 5 is a Receive Buffer 
//    C1TR67CONbits.TXEN6 = 0x0; // Buffer 6 is a Receive Buffer 
//    C1TR67CONbits.TXEN7 = 0x0; // Buffer 7 is a Receive Buffer 
//    /*VSN TX Buffer PRIORITY*/
//    C1TR01CONbits.TX0PRI = 0x3; // Message Buffer 0 Priority Level
//    C1TR01CONbits.TX1PRI = 0x2; // Message Buffer 1 Priority Level
//    C1TR23CONbits.TX2PRI = 0x1; // Message Buffer 2 Priority Level
//    C1TR23CONbits.TX3PRI = 0x0; // Message Buffer 3 Priority Level
//    C1TR45CONbits.TX4PRI = 0x0; // Message Buffer 4 Priority Level
//    C1TR45CONbits.TX5PRI = 0x0; // Message Buffer 5 Priority Level
//    C1TR67CONbits.TX6PRI = 0x0; // Message Buffer 6 Priority Level
//    C1TR67CONbits.TX7PRI = 0x0; // Message Buffer 7 Priority Level
    
//    /* clear the buffer and overflow flags */   
//    C1RXFUL1 = 0x0000;
//    C1RXFUL2 = 0x0000;
//    C1RXOVF1 = 0x0000;
//    C1RXOVF2 = 0x0000;	
//    /* select acceptance masks for filters */
//    C1FMSKSEL1bits.F0MSK = 0x0; //Select Mask 0 for Filter 0
//    C1FMSKSEL1bits.F1MSK = 0x0; //Select Mask 0 for Filter 1
//    /* Configure the masks */
//    C1RXM0SIDbits.SID = 0x7ff; 
//    C1RXM1SIDbits.SID = 0x0; 
//    C1RXM2SIDbits.SID = 0x0; 
//    C1RXM0SIDbits.EID = 0x3; 
//    C1RXM1SIDbits.EID = 0x0; 
//    C1RXM2SIDbits.EID = 0x0; 
//    C1RXM0EID = 0xFFFF;     	
//    C1RXM1EID = 0x00;     	
//    C1RXM2EID = 0x00;     	
//    C1RXM0SIDbits.MIDE = 0x0; 
//    C1RXM1SIDbits.MIDE = 0x0; 
//    C1RXM2SIDbits.MIDE = 0x0; 
//    /* Configure the filters */
//    C1RXF0SIDbits.SID = 0x5fc; 
//    C1RXF1SIDbits.SID = 0x5fc; 
//    C1RXF0SIDbits.EID = 0x3; 
//    C1RXF1SIDbits.EID = 0x2; 
//    C1RXF0EID = 0x701; 
//    C1RXF1EID = 0x701; 
//    C1RXF0SIDbits.EXIDE = 0x0; 
//    C1RXF1SIDbits.EXIDE = 0x0; 
//    /* Non FIFO Mode */
//    C1BUFPNT1bits.F0BP = 0x4; //Filter 0 uses Buffer4
//    C1BUFPNT1bits.F1BP = 0x5; //Filter 1 uses Buffer5
//    /* clear window bit to access CAN1 control registers */
//    C1CTRL1bits.WIN=0;    
/*END MCC Filter Configuration*/  
  
     /* Filter configuration */
    C1CTRL1bits.WIN         = 1;

    //C1RXMnSID: CAN1 ACCEPTANCE FILTER MASK n STANDARD IDENTIFIER REGISTER (n = 0-2)
    C1RXM0SIDbits.SID       = 0b00011111111;
    C1RXM0SIDbits.MIDE      = 1;
    C1RXM0SIDbits.EID       = 0b11;
                //SID<10:0>: Standard Identifier bits
                    //1 = Includes bit, SIDx, in filter comparison
                    //0 = Bit, SIDx, is a don?t care in filter comparison             
                //MIDE: Identifier Receive Mode bit
                    //1 = Matches only message types (standard or extended address) that correspond to the EXIDE bit in the filter
                    //0 = Matches either standard or extended address message if filters match
                    //(i.e., if (Filter SIDx) = (Message SIDx) or if (Filter SIDx/EIDx) = (Message SIDx/EIDx))              
                //EID<17:16>: Extended Identifier bits
                    //1 = Includes bit, EIDx, in filter comparison
                    //0 = Bit, EIDx, is a don?t care in filter comparison

    //C1RXMnEID: CAN1 ACCEPTANCE FILTER MASK n EXTENDED IDENTIFIER REGISTER (n = 0-2)
    C1RXM0EID               = 0b1111111100000000;
                //EID<15:0>: Extended Identifier bits
                    //1 = Includes bit, EIDx, in filter comparison
                    //0 = Bit, EIDx, is a don?t care in filter comparison  
    
    
    //set up filters 
    if(((*(id))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF0SIDbits.SID       = (((*(id))>>18)& 0x000003ff);
        C1RXF0SIDbits.EID       = (((*(id))>>16)& 0x00000003);
        C1RXF0SIDbits.EXIDE     = 1;
                //SID<10:0>: Standard Identifier bits
                    //1 = Message address bit, SIDx, must be ?1? to match filter
                    //0 = Message address bit, SIDx, must be ?0? to match filter
                //EXIDE: Extended Identifier Enable bit
                    //If MIDE = 1:
                    //1 = Matches only messages with Extended Identifier addresses
                    //0 = Matches only messages with Standard Identifier addresses
                    //If MIDE = 0:
                    //Ignores EXIDE bit.
                //EID<17:16>: Extended Identifier bits
                    //1 = Message address bit, EIDx, must be ?1? to match filter
                    //0 = Message address bit, EIDx, must be ?0? to match filter      
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF0EID               = ((*(id)) & 0x0000ffff);
                //EID<15:0>: Extended Identifier bits
                    //1 = Message address bit, EIDx, must be ?1? to match filter
                    //0 = Message address bit, EIDx, must be ?0? to match filter
        
        //C1BUFPNT1: CANx FILTERS 0-3 BUFFER POINTER REGISTER 1
        C1BUFPNT1bits.F0BP      = 4; 
                //F3BP<3:0>: RX Buffer Mask for Filter 3 bits
                    //1111 = Filter hits received in RX FIFO buffer
                    //1110 = Filter hits received in RX Buffer 14
                    //xxxx
                    //0001 = Filter hits received in RX Buffer 1
                    //0000 = Filter hits received in RX Buffer 0
                //F2BP<3:0>: RX Buffer Mask for Filter 2 bits (same values as bits 15-12)
                //F1BP<3:0>: RX Buffer Mask for Filter 1 bits (same values as bits 15-12)
                //F0BP<3:0>: RX Buffer Mask for Filter 0 bits (same values as bits 15-12)
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN0       = 1; 
                //FLTEN<15:0>: Enable Filter n to Accept Messages bits
                               //1 = Enables Filter n
                               //0 = Disables Filter n
    }

    if(((*(id+1))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF1SIDbits.SID       = (((*(id+1))>>18)& 0x000000ff);
        C1RXF1SIDbits.EID       = (((*(id+1))>>16)& 0x00000003);
        C1RXF1SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF1EID               = ((*(id+1)) & 0x0000ff00);
        
        //C1BUFPNT1: CANx FILTERS 0-3 BUFFER POINTER REGISTER 1
        C1BUFPNT1bits.F1BP      = 5; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN1       = 1; 
    }
    
    if(((*(id+2))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF2SIDbits.SID       = (((*(id+2))>>18)& 0x000000ff);
        C1RXF2SIDbits.EID       = (((*(id+2))>>16)& 0x00000003);
        C1RXF2SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)  
        C1RXF2EID               = ((*(id+2)) & 0x0000ff00);
        
        //C1BUFPNT1: CANx FILTERS 0-3 BUFFER POINTER REGISTER 1
        C1BUFPNT1bits.F2BP      = 6; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN2       = 1; 
    }
    
    if(((*(id+3))>>16) &0x000003ff)
    {
         //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF3SIDbits.SID       = (((*(id+3))>>18)& 0x000000ff);
        C1RXF3SIDbits.EID       = (((*(id+3))>>16)& 0x00000003);
        C1RXF3SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF3EID               = ((*(id+3)) & 0x0000ff00);
        
        //C1BUFPNT1: CANx FILTERS 0-3 BUFFER POINTER REGISTER 1
        C1BUFPNT1bits.F3BP      = 7; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN3       = 1; 
    }

    if(((*(id+4))>>16) &0x000003ff)
    {
         //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF4SIDbits.SID       = (((*(id+4))>>18)& 0x000000ff);
        C1RXF4SIDbits.EID       = (((*(id+4))>>16)& 0x00000003);
        C1RXF4SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF4EID               = ((*(id+4)) & 0x0000ff00);
        
        //C1BUFPNT2: CANx FILTERS 4-7 BUFFER POINTER REGISTER 2
        C1BUFPNT2bits.F4BP      = 8; 
                //F7BP<3:0>: RX Buffer Mask for Filter 7 bits
                        //1111 = Filter hits received in RX FIFO buffer
                        //1110 = Filter hits received in RX Buffer 14
                        //???
                        //0001 = Filter hits received in RX Buffer 1
                        //0000 = Filter hits received in RX Buffer 0
                //F6BP<3:0>: RX Buffer Mask for Filter 6 bits (same values as bits 15-12)
                //F5BP<3:0>: RX Buffer Mask for Filter 5 bits (same values as bits 15-12)
                //F4BP<3:0>: RX Buffer Mask for Filter 4 bits (same values as bits 15-12)
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN4       = 1; 
    }    

    if(((*(id+5))>>16) &0x000003ff)
    {
         //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF5SIDbits.SID       = (((*(id+5))>>18)& 0x000000ff);
        C1RXF5SIDbits.EID       = (((*(id+5))>>16)& 0x00000003);
        C1RXF5SIDbits.EXIDE     = 1;
       
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF5EID               = ((*(id+5)) & 0x0000ff00);
        
        //C1BUFPNT2: CANx FILTERS 4-7 BUFFER POINTER REGISTER 2
        C1BUFPNT2bits.F5BP      = 9; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN5       = 1; 
    }

    if(((*(id+6))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF6SIDbits.SID       = (((*(id+6))>>18)& 0x000000ff);
        C1RXF6SIDbits.EID       = (((*(id+6))>>16)& 0x00000003);
        C1RXF6SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF6EID               = ((*(id+6)) & 0x0000ff00);
        
         //C1BUFPNT2: CANx FILTERS 4-7 BUFFER POINTER REGISTER 2
        C1BUFPNT2bits.F6BP      = 10; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN6       = 1; 
    }

    if(((*(id+7))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF7SIDbits.SID       = (((*(id+7))>>18)& 0x000000ff);
        C1RXF7SIDbits.EID       = (((*(id+7))>>16)& 0x00000003);
        C1RXF7SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF7EID               = ((*(id+7)) & 0x0000ff00);
        
         //C1BUFPNT2: CANx FILTERS 4-7 BUFFER POINTER REGISTER 2
        C1BUFPNT2bits.F7BP      = 11; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN7       = 1; 
    }

    if(((*(id+8))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15) 
        C1RXF8SIDbits.SID       = (((*(id+8))>>18)& 0x000000ff);
        C1RXF8SIDbits.EID       = (((*(id+8))>>16)& 0x00000003);
        C1RXF8SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF8EID               = ((*(id+8)) & 0x0000ff00);
        
        //C1BUFPNT3: CAN1 FILTERS 8-11 BUFFER POINTER REGISTER 3
        C1BUFPNT3bits.F8BP      = 12; 
                //F11BP<3:0>: RX Buffer Mask for Filter 11 bits
                    //1111 = Filter hits received in RX FIFO buffer
                    //1110 = Filter hits received in RX Buffer 14
                    //???
                    //0001 = Filter hits received in RX Buffer 1
                    //0000 = Filter hits received in RX Buffer 0
                //F10BP<3:0>: RX Buffer Mask for Filter 10 bits (same values as bits 15-12)
                //F9BP<3:0>: RX Buffer Mask for Filter 9 bits (same values as bits 15-12)
                //F8BP<3:0>: RX Buffer Mask for Filter 8 bits (same values as bits 15-12)
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN8       = 1; 
    }

    if(((*(id+9))>>16) &0x000003ff)
    {
        //C1RXFnSID: CAN1 ACCEPTANCE FILTER n STANDARD IDENTIFIER REGISTER (n = 0-15)
        C1RXF9SIDbits.SID       = (((*(id+9))>>18)& 0x000000ff);
        C1RXF9SIDbits.EID       = (((*(id+9))>>16)& 0x00000003);
        C1RXF9SIDbits.EXIDE     = 1;
        
        //C1RXFnEID: CAN1 ACCEPTANCE FILTER n EXTENDED IDENTIFIER REGISTER (n = 0-15)
        C1RXF9EID               = ((*(id+9)) & 0x0000ff00);
        
        //C1BUFPNT3: CAN1 FILTERS 8-11 BUFFER POINTER REGISTER 3
        C1BUFPNT3bits.F9BP      = 13; 
        
        //C1FEN1: CANx ACCEPTANCE FILTER ENABLE REGISTER 1
        C1FEN1bits.FLTEN9       = 1; 
    } 
  
    //C1FMSKSEL1: CAN1 FILTERS 7-0 MASK SELECTION REGISTER 1
    C1FMSKSEL1              = 0x0;
            //F7MSK<1:0>: Mask Source for Filter 7 bit
                //11 = Reserved
                //10 = Acceptance Mask 2 registers contain mask
                //01 = Acceptance Mask 1 registers contain mask
                //00 = Acceptance Mask 0 registers contain mask
            //F6MSK<1:0>: Mask Source for Filter 6 bit (same values as bits 15-14)
            //F5MSK<1:0>: Mask Source for Filter 5 bit (same values as bits 15-14)
            //F4MSK<1:0>: Mask Source for Filter 4 bit (same values as bits 15-14)
            //F3MSK<1:0>: Mask Source for Filter 3 bit (same values as bits 15-14)
            //F2MSK<1:0>: Mask Source for Filter 2 bit (same values as bits 15-14)
            //F1MSK<1:0>: Mask Source for Filter 1 bit (same values as bits 15-14)
            //F0MSK<1:0>: Mask Source for Filter 0 bit (same values as bits 15-14) 

    //C1FMSKSEL2: CAN1 FILTERS 15-8 MASK SELECTION REGISTER 2
    C1FMSKSEL2              = 0x0;
        //F15MSK<1:0>: Mask Source for Filter 15 bit
        //11 = Reserved
        //10 = Acceptance Mask 2 registers contain mask
        //01 = Acceptance Mask 1 registers contain mask
        //00 = Acceptance Mask 0 registers contain mask
        //F14MSK<1:0>: Mask Source for Filter 14 bit (same values as bits 15-14)
        //F13MSK<1:0>: Mask Source for Filter 13 bit (same values as bits 15-14)
        //F12MSK<1:0>: Mask Source for Filter 12 bit (same values as bits 15-14)
        //F11MSK<1:0>: Mask Source for Filter 11 bit (same values as bits 15-14)
        //F10MSK<1:0>: Mask Source for Filter 10 bit (same values as bits 15-14)
        //F9MSK<1:0>: Mask Source for Filter 9 bit (same values as bits 15-14)
        //F8MSK<1:0>: Mask Source for Filter 8 bit (same values as bits 15-14)

    //Interrupt CAN1 FLAG location IFS2 register
    IFS2bits.C1IF           = 0;
    
    //Interrupt CAN1 Event Priority
    IPC8bits.C1IP           = 7;
    
    //C1INTF: CAN1 INTERRUPT FLAG REGISTER
    C1INTFbits.RBIF         = 0;
            //TXBO: Transmitter in Error State Bus Off bit
                //1 = Transmitter is in Bus Off state
                //0 = Transmitter is not in Bus Off state
            //TXBP: Transmitter in Error State Bus Passive bit
                //1 = Transmitter is in Bus Passive state
                //0 = Transmitter is not in Bus Passive state
            //RXBP: Receiver in Error State Bus Passive bit
                //1 = Receiver is in Bus Passive state
                //0 = Receiver is not in Bus Passive state
            //TXWAR: Transmitter in Error State Warning bit
                //1 = Transmitter is in Error Warning state
                //0 = Transmitter is not in Error Warning state
            //RXWAR: Receiver in Error State Warning bit
                //1 = Receiver is in Error Warning state
                //0 = Receiver is not in Error Warning state
            //EWARN: Transmitter or Receiver in Error State Warning bit
                //1 = Transmitter or receiver is in Error Warning state
                //0 = Transmitter or receiver is not in Error Warning state
            //IVRIF: Invalid Message Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
            //WAKIF: Bus Wake-up Activity Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
            //ERRIF: Error Interrupt Flag bit (multiple sources in CxINTF<13:8> register)
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred           
            //FIFOIF: FIFO Almost Full Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
            //RBOVIF: RX Buffer Overflow Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
            //RBIF: RX Buffer Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
            //TBIF: TX Buffer Interrupt Flag bit
                //1 = Interrupt request has occurred
                //0 = Interrupt request has not occurred
    
    
    
    //C1INTE: CAN1 INTERRUPT ENABLE REGISTER
    C1INTEbits.RBIE         = 1;
            //IVRIE: Invalid Message Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
            //WAKIE: Bus Wake-up Activity Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
            //ERRIE: Error Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled         
            //FIFOIE: FIFO Almost Full Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
            //RBOVIE: RX Buffer Overflow Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
            //RBIE: RX Buffer Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
            //TBIE: TX Buffer Interrupt Enable bit
                //1 = Interrupt request is enabled
                //0 = Interrupt request is not enabled
    
    C1CTRL1bits.WIN         = 0;
    
    //canChangeMode(NORMAL_MODE);
    /* put the module in normal mode CAN_NORMAL_OPERATION_MODE*/
    C1CTRL1bits.REQOP = CAN_NORMAL_OPERATION_MODE;
    while(C1CTRL1bits.OPMODE != CAN_NORMAL_OPERATION_MODE);	
    
  /* Enable CAN1 Interrupt */
    IEC2bits.C1IE = 1;   
}





/******************************************************************************
*                                                                             
*    Function:		CAN1_TransmitEnable
*    Description:       Setup the DMA for Transmit from the CAN module.  The 
*                       relevant DMA module APIs are grouped in this function 
*                       and this API needs to be called after DMA_Initialize 
*                       and CAN_Initialize
*                                                                                                                                                       
******************************************************************************/

void CAN1_TransmitEnable()
{
    /* setup channel 0 for peripheral indirect addressing mode 
    normal operation, word operation and select as Tx to peripheral */

    /* DMA_PeripheralIrqNumberSet and DMA_TransferCountSet would be done in the 
    DMA */
    
    /* setup the address of the peripheral CAN1 (C1TXD) */ 
    DMA_PeripheralAddressSet(CAN1_TX_DMA_CHANNEL, (uint16_t) &C1TXD);

    /* DPSRAM start address offset value */ 
    DMA_StartAddressASet(CAN1_TX_DMA_CHANNEL, __builtin_dmaoffset(&can1msgBuf));

    /* enable the channel */
    DMA_ChannelEnable(CAN1_TX_DMA_CHANNEL);
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_ReceiveEnable
*    Description:       Setup the DMA for Receive on the CAN module.  The 
*                       relevant DMA module APIs are grouped in this function 
*                       and this API needs to be called after DMA_Initialize 
*                       and CAN_Initialize
*                                                                                                                                                       
******************************************************************************/

void CAN1_ReceiveEnable()
{
    /* setup DMA channel for peripheral indirect addressing mode 
    normal operation, word operation and select as Rx to peripheral */
    
    /* setup the address of the peripheral CAN1 (C1RXD) */     
    /* DMA_TransferCountSet and DMA_PeripheralIrqNumberSet would be set in 
    the DMA_Initialize function */

    DMA_PeripheralAddressSet(CAN1_RX_DMA_CHANNEL, (uint16_t) &C1RXD);

    /* DPSRAM start address offset value */ 
    DMA_StartAddressASet(CAN1_RX_DMA_CHANNEL, __builtin_dmaoffset(&can1msgBuf) );	  

    /* enable the channel */
    DMA_ChannelEnable(CAN1_RX_DMA_CHANNEL);
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_transmit
*    Description:       Transmits the message from user buffer to CAN buffer
*                       as per the buffer number allocated.
*                       Allocation of the buffer number is done by user 
*                                                                             
*    Arguments:		priority : priority of the message to be transmitted
*                       sendCanMsg: pointer to the message object
*                                             
*    Return Value:      true - Transmit successful
*                       false - Transmit failure                                                                              
******************************************************************************/

bool CAN1_transmit(CAN_TX_PRIOIRTY priority, uCAN_MSG *sendCanMsg) 
{
    CAN1_TX_CONTROLS * pTxControls = (CAN1_TX_CONTROLS*)&C1TR01CON;
    uint_fast8_t i;
    bool messageSent = false;

    if(CAN1_TX_BUFFER_COUNT > 0)
    {
        for(i=0; i<CAN1_TX_BUFFER_COUNT; i++)
        {
            if(pTxControls->transmit_enabled == 1)
            {
                if (pTxControls->send_request == 0)
                {
                    CAN1_MessageToBuffer(&can1msgBuf[i][0], sendCanMsg);
                    pTxControls->priority = priority;

                    /* set the message for transmission */
                    pTxControls->send_request = 1; 

                    messageSent = true;
                    break;
                }
            }

            pTxControls++;
        }
    }    
    return messageSent;
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_receive
*    Description:       Receives the message from CAN buffer to user buffer 
*                                                                             
*    Arguments:		recCanMsg: pointer to the message object
*                                             
*    Return Value:      true - Receive successful
*                       false - Receive failure                                                                              
******************************************************************************/

bool CAN1_receive(uCAN_MSG *recCanMsg) 
{   
    /* We use a static buffer counter so we don't always check buffer 0 first
     * resulting in potential starvation of later buffers.
     */
    static uint_fast8_t currentDedicatedBuffer = 0;
    uint_fast8_t i;
    bool messageReceived = false;
    uint16_t receptionFlags;
    
    receptionFlags = C1RXFUL1;
	
    if (receptionFlags != 0)  
    {
        /* check which message buffer is free */  
        for (i=0 ; i < 16; i++)
        {
            if (((receptionFlags >> currentDedicatedBuffer ) & 0x1) == 0x1)
            {           
               CAN1_DMACopy(currentDedicatedBuffer, recCanMsg);
           
               C1RXFUL1 &= ~(1 << currentDedicatedBuffer);
  
               messageReceived = true;
            }
            
            currentDedicatedBuffer++;
            
            if(currentDedicatedBuffer >= 16)
            {
                currentDedicatedBuffer = 0;
            }
            
            if(messageReceived == true)
            {
                break;
            }
        }
    }
        
    return (messageReceived);
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_isBusOff
*    Description:       Checks whether the transmitter in Bus off state
*                                                                             
                                             
*    Return Value:      true - Transmitter in Bus Off state
*                       false - Transmitter not in Bus Off state                                                                              
******************************************************************************/

bool CAN1_isBusOff() 
{
    return C1INTFbits.TXBO;	
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_isRXErrorPassive
*    Description:       Checks whether the receive in error passive state
*                                             
*    Return Value:      true - Receiver in Error Passive state
*                       false - Receiver not in Error Passive state                                                                              
******************************************************************************/

bool CAN1_isRXErrorPassive()
{
    return C1INTFbits.RXBP;   
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_isTXErrorPassive
*    Description:       Checks whether the transmitter in error passive state                                                                          
*                                             
*    Return Value:      true - Transmitter in Error Passive state
*                       false - Transmitter not in Error Passive state                                                                              
******************************************************************************/

bool CAN1_isTXErrorPassive()
{
    return (C1INTFbits.TXBP);
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_messagesInBuffer
*    Description:       returns the number of messages that are received                                                                           
*                                             
*    Return Value:      Number of message received
******************************************************************************/

uint8_t CAN1_messagesInBuffer() 
{
    uint_fast8_t messageCount;
    uint_fast8_t currentBuffer;
    uint16_t receptionFlags;
   
    messageCount = 0;

    /* Check any message in buffer 0 to buffer 15*/
    receptionFlags = C1RXFUL1;
    if (receptionFlags != 0) 
    {
        /* check whether a message is received */  
        for (currentBuffer=0 ; currentBuffer < 16; currentBuffer++)
        {
            if (((receptionFlags >> currentBuffer ) & 0x1) == 0x1)
            {
                messageCount++;
            }
        }
    }
            
    return (messageCount);
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_sleep
*    Description:       Puts CAN1 module in disable mode.
*                                                                       
******************************************************************************/
void CAN1_sleep(void) 
{
    C1INTFbits.WAKIF = 0;
    C1INTEbits.WAKIE = 1;

    /* put the module in disable mode */
    C1CTRL1bits.REQOP = CAN_DISABLE_MODE;
    while(C1CTRL1bits.OPMODE != CAN_DISABLE_MODE);
    
    //Wake up from sleep should set the CAN module straight into Normal mode
}

/*******************************************************************************
 * PRIVATE FUNCTIONS
 ******************************************************************************/

/******************************************************************************
*                                                                             
*    Function:		CAN1_DMACopy
*    Description:       moves the message from the DMA memory to RAM
*                                                                             
*    Arguments:		*message: a pointer to the message structure in RAM 
*			that will store the message. 
*	                                                                 
*                                                                              
******************************************************************************/

static void CAN1_DMACopy(uint8_t buffer_number, uCAN_MSG *message)
{
    uint16_t ide=0;
    uint16_t rtr=0;
    uint32_t id=0;

    /* read word 0 to see the message type */
    ide=can1msgBuf[buffer_number][0] & 0x0001U;			

    /* check to see what type of message it is */
    /* message is standard identifier */
    if(ide==0U)
    {
        message->frame.id=(can1msgBuf[buffer_number][0] & 0x1FFCU) >> 2U;		
        message->frame.idType = CAN_FRAME_STD;
        rtr=can1msgBuf[buffer_number][0] & 0x0002U;
    }
    /* message is extended identifier */
    else
    {
        id=can1msgBuf[buffer_number][0] & 0x1FFCU;		
        message->frame.id = id << 16U;
        message->frame.id += ( ((uint32_t)can1msgBuf[buffer_number][1] & (uint32_t)0x0FFF) << 6U );
        message->frame.id += ( ((uint32_t)can1msgBuf[buffer_number][2] & (uint32_t)0xFC00U) >> 10U );		
        message->frame.idType = CAN_FRAME_EXT;
        rtr=can1msgBuf[buffer_number][2] & 0x0200;
    }
    /* check to see what type of message it is */
    /* RTR message */
    if(rtr != 0U)
    {
        /* to be defined ?*/
        message->frame.msgtype = CAN_MSG_RTR;	
    }
    /* normal message */
    else
    {
        message->frame.msgtype = CAN_MSG_DATA;
        message->frame.data0 =(unsigned char)can1msgBuf[buffer_number][3];
        message->frame.data1 =(unsigned char)((can1msgBuf[buffer_number][3] & 0xFF00U) >> 8U);
        message->frame.data2 =(unsigned char)can1msgBuf[buffer_number][4];
        message->frame.data3 =(unsigned char)((can1msgBuf[buffer_number][4] & 0xFF00U) >> 8U);
        message->frame.data4 =(unsigned char)can1msgBuf[buffer_number][5];
        message->frame.data5 =(unsigned char)((can1msgBuf[buffer_number][5] & 0xFF00U) >> 8U);
        message->frame.data6 =(unsigned char)can1msgBuf[buffer_number][6];
        message->frame.data7 =(unsigned char)((can1msgBuf[buffer_number][6] & 0xFF00U) >> 8U);
        message->frame.dlc =(unsigned char)(can1msgBuf[buffer_number][2] & 0x000FU);
    }
}

/******************************************************************************
*                                                                             
*    Function:		CAN1_MessageToBuffer
*    Description:       This function takes the input message, reformats it, 
*                       and copies it to the specified CAN module buffer
*                                                                             
*    Arguments:		*buffer: a pointer to the buffer where the message 
*                       would be stored 
*                       *message: pointer to the input message that is received
*                       by the CAN module 	                                                                 
*                                                                              
******************************************************************************/

static void CAN1_MessageToBuffer(uint16_t* buffer, uCAN_MSG* message)
{   
    if(message->frame.idType == CAN_FRAME_STD)
    {
        buffer[0]= (message->frame.id & 0x000007FF) << 2;
        buffer[1]= 0;
        buffer[2]= message->frame.dlc & 0x0F;
    }
    else
    {
        buffer[0]= ( ( (uint16_t)(message->frame.id >> 16 ) & 0x1FFC ) ) | 0b1;
        buffer[1]= (uint16_t)(message->frame.id >> 6) & 0x0FFF;
        buffer[2]= (message->frame.dlc & 0x0F) + ( (uint16_t)(message->frame.id << 10) & 0xFC00);
    }

    buffer[3]= ((message->frame.data1)<<8) + message->frame.data0;
    buffer[4]= ((message->frame.data3)<<8) + message->frame.data2;
    buffer[5]= ((message->frame.data5)<<8) + message->frame.data4;
    buffer[6]= ((message->frame.data7)<<8) + message->frame.data6;
}

/**
 End of File
*/