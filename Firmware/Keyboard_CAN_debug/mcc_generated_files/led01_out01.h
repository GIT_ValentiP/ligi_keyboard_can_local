/**
  LED01_OUT01 Generated Driver API Header File

  @Company
    Microchip Technology Inc.

  @File Name
    led01_out01.h

  @Summary
    This is the generated header file for the LED01_OUT01 driver using Board Support Library

  @Description
    This header file provides APIs for driver for LED01_OUT01.
    Generation Information :
        Product Revision  :  Board Support Library - 0.95.1
        Device            :  dsPIC33EP256GM710
        Driver Version    :  0.95
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef LED01_OUT01_H
#define LED01_OUT01_H

#include <xc.h>

static inline void LED01_OUT01_On() {_LATB12 = 1;}
static inline void LED01_OUT01_Off() {_LATB12 = 0;}
static inline void LED02_OUT02_On() {_LATB13 = 1;}
static inline void LED02_OUT02_Off() {_LATB13 = 0;}
static inline void LED03_OUT03_On() {_LATA10 = 1;}
static inline void LED03_OUT03_Off() {_LATA10 = 0;}
static inline void LED04_OUT04_On() {_LATG15 = 1;}
static inline void LED04_OUT04_Off() {_LATG15 = 0;}
static inline void LED05_OUT05_On() {_LATA7 = 1;}
static inline void LED05_OUT05_Off() {_LATA7 = 0;}
static inline void LED06_OUT06_On() {_LATB14 = 1;}
static inline void LED06_OUT06_Off() {_LATB14 = 0;}
static inline void LED07_OUT07_On() {_LATB15 = 1;}
static inline void LED07_OUT07_Off() {_LATB15 = 0;}
static inline void LED08_OUT08_On() {_LATD1 = 1;}
static inline void LED08_OUT08_Off() {_LATD1 = 0;}
static inline void LED09_OUT09_On() {_LATD2 = 1;}
static inline void LED09_OUT09_Off() {_LATD2 = 0;}
static inline void LED10_OUT10_On() {_LATD3 = 1;}
static inline void LED10_OUT10_Off() {_LATD3 = 0;}
static inline void LED11_OUT11_On() {_LATD4 = 1;}
static inline void LED11_OUT11_Off() {_LATD4 = 0;}
static inline void LED12_OUT12_On() {_LATG6 = 1;}
static inline void LED12_OUT12_Off() {_LATG6 = 0;}
static inline void LED13_OUT13_On() {_LATG7 = 1;}
static inline void LED13_OUT13_Off() {_LATG7 = 0;}
static inline void LED14_OUT14_On() {_LATG8 = 1;}
static inline void LED14_OUT14_Off() {_LATG8 = 0;}

static inline void LED15_OUTB01_On() {_LATG9 = 1;}
static inline void LED15_OUTB01_Off() {_LATG9 = 0;}
static inline void LED16_OUTB02_On() {_LATG10 = 1;}
static inline void LED16_OUTB02_Off() {_LATG10 = 0;}
static inline void LED17_OUTB03_On() {_LATE8 = 1;}
static inline void LED17_OUTB03_Off() {_LATE8 = 0;}
static inline void LED18_OUTB04_On() {_LATE9 = 1;}
static inline void LED18_OUTB04_Off() {_LATE9 = 0;}
static inline void LED19_OUTB05_On() {_LATA12 = 1;}
static inline void LED19_OUTB05_Off() {_LATA12 = 0;}
static inline void LED20_OUTB06_On() {_LATA11 = 1;}
static inline void LED20_OUTB06_Off() {_LATA11 = 0;}
static inline void LED21_OUTB07_On() {_LATA0 = 1;}
static inline void LED21_OUTB07_Off() {_LATA0 = 0;}
static inline void LED22_OUTB08_On() {_LATA1 = 1;}
static inline void LED22_OUTB08_Off() {_LATA1 = 0;}
static inline void LED23_OUTB09_On() {_LATB0 = 1;}
static inline void LED23_OUTB09_Off() {_LATB0 = 0;}
static inline void LED24_OUTB10_On() {_LATB1 = 1;}
static inline void LED24_OUTB10_Off() {_LATB1 = 0;}
static inline void LED25_OUTB11_On() {_LATF9 = 1;}
static inline void LED25_OUTB11_Off() {_LATF9 = 0;}
static inline void LED26_OUTB12_On() {_LATF10 = 1;}
static inline void LED26_OUTB12_Off() {_LATF10 = 0;}
static inline void LED27_OUTB13_On() {_LATC0 = 1;}
static inline void LED27_OUTB13_Off() {_LATC0 = 0;}
static inline void LED28_OUTB14_On() {_LATC1 = 1;}
static inline void LED28_OUTB14_Off() {_LATC1 = 0;}




#endif	/* LED01_OUT01_H */