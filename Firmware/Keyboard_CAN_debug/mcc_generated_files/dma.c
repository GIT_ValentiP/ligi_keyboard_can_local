/**
  DMA Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    dma.c

  @Summary
    This is the generated driver implementation file for the DMA driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This source file provides APIs for DMA.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  dsPIC33EP256GM710
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB 	          :  MPLAB X v5.30
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/

#include "dma.h"
#include "../globals.h"
extern volatile can_msg_buff_t can_msg_buff[SIZE_OF_CAN_BUFF]; 


void DMA_Initialize(void) 
{ 
    // Initialize channels which are enabled    
    //DMA channel 0 for TX CAN 
    // AMODE Peripheral Indirect Addressing mode; CHEN disabled; DIR Reads from RAM address, writes to peripheral address; HALF Initiates interrupt when all of the data has been moved; SIZE 16 bit; NULLW disabled; MODE Continuous, Ping-Pong modes are disabled; 
//    DMA0CON= 0x2020 & 0x7FFF; //Enable DMA Channel later;
//    // FORCE disabled; IRQSEL CAN1 TX; 
//    DMA0REQ= 0x46;//DMA0REQ= 70;
//    // STA 0; 
//    DMA0STAH= 0x00;//DMA0STAH = 0;
//    // STA 4096; 
//    DMA0STAL= 0x1000;//DMA0STAL= (uint16_t)(&can_msg_buff);
//    // STB 0; 
//    DMA0STBH= 0x00;
//    // STB 0; 
//    DMA0STBL= 0x00;
//    // PAD 0; 
//    DMA0PAD= 0x00; //DMA0PAD= (volatile unsigned int)&C1TXD;
//    // CNT 7; 
//    DMA0CNT= 0x07;//DMA0CNT = 7;
//    IFS0bits.DMA0IF = false;// Clearing Channel 0 Interrupt Flag;
//    // Enabling Channel 0 Interrupt
    //DMA0CON: DMA CHANNEL 0 CONTROL REGISTER
    DMA0CONbits.SIZE        = 0x0;
    DMA0CONbits.DIR         = 0x1;
    DMA0CONbits.AMODE       = 0x2;
    DMA0CONbits.MODE        = 0x0;
                           //CHEN: Channel Enable bit
                                //1 = Channel is enabled
                                //0 = Channel is disabled
                            //SIZE: Data Transfer Size bit
                                //1 = Byte
                                //0 = Word
                            //DIR: Transfer Direction bit (source/destination bus select)
                                //1 = Reads from RAM address, writes to peripheral address
                                //0 = Reads from peripheral address, writes to RAM address
                            //HALF: Block Transfer Interrupt Select bit
                                //1 = Initiates interrupt when half of the data has been moved
                                //0 = Initiates interrupt when all of the data has been moved
                            //NULLW: Null Data Peripheral Write Mode Select bit
                                //1 = Null data write to peripheral in addition to RAM write (DIR bit must also be clear)
                                //0 = Normal operation
                            //AMODE<1:0>: DMA Channel Addressing Mode Select bits
                                //11 = Reserved
                                //10 = Peripheral Indirect mode
                                //01 = Register Indirect without Post-Increment mode
                                //00 = Register Indirect with Post-Increment mode
                            //MODE<1:0>: DMA Channel Operating Mode Select bits
                                //11 = One-Shot, Ping-Pong modes are enabled (one block transfer from/to each DMA buffer)
                                //10 = Continuous, Ping-Pong modes are enabled
                                //01 = One-Shot, Ping-Pong modes are disabled
                                //00 = Continuous, Ping-Pong modes are disabled
    
    //DMA0REQ: DMA CHANNEL 0 IRQ SELECT REGISTER
    DMA0REQ                 = 70;
                            //FORCE: Force DMA Transfer bit(1)
                                //1 = Forces a single DMA transfer (Manual mode)
                                //0 = Automatic DMA transfer initiation by DMA request
                            //IRQSEL<7:0>: DMA Peripheral IRQ Number Select bits
                                //01011011 = SPI3 ? Transfer done
                                //01011001 = UART4TX ? UART4 transmitter
                                //01011000 = UART4RX ? UART4 receiver
                                //01010011 = UART3TX ? UART3 transmitter
                                //01010010 = UART3RX ? UART3 receiver
                                //01000111 = CAN2 ? TX data request
                                //01000110 = CAN1 ? TX data request
                                //00111100 = DCI ? Codec transfer done
                                //00110111 = CAN2 ? RX data ready
                                //00101101 = PMP ? PMP data move
                                //00100110 = IC4 ? Input Capture 4
                                //00100101 = IC3 ? Input Capture 3
                                //00100010 = CAN1 ? RX data ready
                                //00100001 = SPI2 ? SPI2 transfer done
                                //00011111 = UART2TX ? UART2 transmitter
                                //00011110 = UART2RX ? UART2 receiver
                                //00011100 = TMR5 ? Timer5
                                //00011011 = TMR4 ? Timer4
                                //00011010 = OC4 ? Output Compare 4
                                //00011001 = OC3 ? Output Compare 3
                                //00010101 = ADC2 ? ADC2 convert done
                                //00001101 = ADC1 ? ADC1 convert done
                                //00001100 = UART1TX ? UART1 transmitter
                                //00001011 = UART1RX ? UART1 receiver
                                //00001010 = SPI1 ? SPI1 transfer done
                                //00001000 = TMR3 ? Timer3
                                //00000111 = TMR2 ? Timer2
                                //00000110 = OC2 ? Output Compare 2
                                //00000101 = IC2 ? Input Capture 2
                                //00000010 = OC1 ? Output Compare 1
                                //00000001 = IC1 ? Input Capture 1
                                //00000000 = INT0 ? External Interrupt 0
    
    //DMA0CNT: DMA CHANNEL 0 TRANSFER COUNT REGISTER
    DMA0CNT                 = 7;
                            //CNT<13:0>: DMA Transfer Count Register bits           
                                       //Note: The number of DMA transfers = CNT<13:0> + 1.
                             
    
    //DMA0PAD: DMA CHANNEL 0 PERIPHERAL ADDRESS REGISTER
    DMA0PAD                 = (volatile unsigned int)&C1TXD;
                            //PAD<15:0>: DMA Peripheral Address Register bits
                                //Note 1: If the channel is enabled (i.e., active), writes to this register may result in unpredictable behavior of the
                                //DMA channel and should be avoided.  
    
    //DMA0STAL: DMA CHANNEL 0 START ADDRESS REGISTER A (LOW)
    DMA0STAL                = (uint16_t)(&can_msg_buff);
                            //STA<15:0>: DMA Primary Start Address bits (source or destination)   
    
    
    //DMA0STAH: DMA CHANNEL 0 START ADDRESS REGISTER A (HIGH)
    DMA0STAH                = 0;
                            //STA<23:16>: DMA Primary Start Address bits (source or destination)
    
    DMA0CONbits.CHEN        = 1;
    
    
     //DMA channel 1 for RX CAN 
    // AMODE Peripheral Indirect Addressing mode; CHEN disabled; SIZE 16 bit; DIR Reads from peripheral address, writes to RAM address; NULLW disabled; HALF Initiates interrupt when all of the data has been moved; MODE Continuous, Ping-Pong modes are disabled; 
//    DMA1CON= 0x20 & 0x7FFF; //Enable DMA Channel later;
//    // FORCE disabled; IRQSEL CAN1 RX; 
//    DMA1REQ= 0x22;// DMA1REQ = 34;
//    // STA 0; 
//    DMA1STAH= 0x00;//DMA1STAH = 0;
//    // STA 4096; 
//    DMA1STAL= 0x1000;//DMA1STAL= (uint16_t)(&can_msg_buff);
//    // STB 0; 
//    DMA1STBH= 0x00;
//    // STB 0; 
//    DMA1STBL= 0x00;
//    // PAD 0; 
//    DMA1PAD= 0x00;//  DMA1PAD= (volatile unsigned int)&C1RXD;
//    // CNT 7; 
//    DMA1CNT= 0x07;//DMA1CNT  = 7;
//    // Clearing Channel 1 Interrupt Flag;
//    IFS0bits.DMA1IF = false;
//    // Enabling Channel 1 Interrupt
//    IEC0bits.DMA1IE = 1;
    //DMA1CON: DMA CHANNEL 1 CONTROL REGISTER
    DMA1CONbits.SIZE        = 0x0;
    DMA1CONbits.DIR         = 0x0;
    DMA1CONbits.AMODE       = 0x2;
    DMA1CONbits.MODE        = 0x0;
   
                            //CHEN: Channel Enable bit
                                //1 = Channel is enabled
                                //0 = Channel is disabled
                            //SIZE: Data Transfer Size bit
                                //1 = Byte
                                //0 = Word
                            //DIR: Transfer Direction bit (source/destination bus select)
                                //1 = Reads from RAM address, writes to peripheral address
                                //0 = Reads from peripheral address, writes to RAM address
                            //HALF: Block Transfer Interrupt Select bit
                                //1 = Initiates interrupt when half of the data has been moved
                                //0 = Initiates interrupt when all of the data has been moved
                            //NULLW: Null Data Peripheral Write Mode Select bit
                                //1 = Null data write to peripheral in addition to RAM write (DIR bit must also be clear)
                                //0 = Normal operation
                            //AMODE<1:0>: DMA Channel Addressing Mode Select bits
                                //11 = Reserved
                                //10 = Peripheral Indirect mode
                                //01 = Register Indirect without Post-Increment mode
                                //00 = Register Indirect with Post-Increment mode
                            //MODE<1:0>: DMA Channel Operating Mode Select bits
                                //11 = One-Shot, Ping-Pong modes are enabled (one block transfer from/to each DMA buffer)
                                //10 = Continuous, Ping-Pong modes are enabled
                                //01 = One-Shot, Ping-Pong modes are disabled
                                //00 = Continuous, Ping-Pong modes are disabled
    
    
    //DMA1REQ: DMA CHANNEL 1 IRQ SELECT REGISTER
    DMA1REQ                 = 34;
                            //FORCE: Force DMA Transfer bit(1)
                                //1 = Forces a single DMA transfer (Manual mode)
                                //0 = Automatic DMA transfer initiation by DMA request
                            //IRQSEL<7:0>: DMA Peripheral IRQ Number Select bits
                                //01011011 = SPI3 ? Transfer done
                                //01011001 = UART4TX ? UART4 transmitter
                                //01011000 = UART4RX ? UART4 receiver
                                //01010011 = UART3TX ? UART3 transmitter
                                //01010010 = UART3RX ? UART3 receiver
                                //01000111 = CAN2 ? TX data request
                                //01000110 = CAN1 ? TX data request
                                //00111100 = DCI ? Codec transfer done
                                //00110111 = CAN2 ? RX data ready
                                //00101101 = PMP ? PMP data move
                                //00100110 = IC4 ? Input Capture 4
                                //00100101 = IC3 ? Input Capture 3
                                //00100010 = CAN1 ? RX data ready
                                //00100001 = SPI2 ? SPI2 transfer done
                                //00011111 = UART2TX ? UART2 transmitter
                                //00011110 = UART2RX ? UART2 receiver
                                //00011100 = TMR5 ? Timer5
                                //00011011 = TMR4 ? Timer4
                                //00011010 = OC4 ? Output Compare 4
                                //00011001 = OC3 ? Output Compare 3
                                //00010101 = ADC2 ? ADC2 convert done
                                //00001101 = ADC1 ? ADC1 convert done
                                //00001100 = UART1TX ? UART1 transmitter
                                //00001011 = UART1RX ? UART1 receiver
                                //00001010 = SPI1 ? SPI1 transfer done
                                //00001000 = TMR3 ? Timer3
                                //00000111 = TMR2 ? Timer2
                                //00000110 = OC2 ? Output Compare 2
                                //00000101 = IC2 ? Input Capture 2
                                //00000010 = OC1 ? Output Compare 1
                                //00000001 = IC1 ? Input Capture 1
                                //00000000 = INT0 ? External Interrupt 0
    
    //DMA1CNT: DMA CHANNEL 1 TRANSFER COUNT REGISTER
    DMA1CNT                 = 7;
                            //CNT<13:0>: DMA Transfer Count Register bits           
                                       //Note: The number of DMA transfers = CNT<13:0> + 1.
    
    //DMA1PAD: DMA CHANNEL 1 PERIPHERAL ADDRESS REGISTER
    DMA1PAD                 = (volatile unsigned int)&C1RXD;
                            //PAD<15:0>: DMA Peripheral Address Register bits
                                //Note 1: If the channel is enabled (i.e., active), writes to this register may result in unpredictable behavior of the
                                //DMA channel and should be avoided. 
    
    //DMA1STAL: DMA CHANNEL 1 START ADDRESS REGISTER A (LOW)
    DMA1STAL                = (uint16_t)(&can_msg_buff);
                            //STA<15:0>: DMA Primary Start Address bits (source or destination)
    
    //DMA1STAH: DMA CHANNEL 1 START ADDRESS REGISTER A (HIGH)
    DMA1STAH                = 0;
                            //STA<23:16>: DMA Primary Start Address bits (source or destination)
    
    
    DMA1CONbits.CHEN        = 1;
    
    
 /*DMA Channel 2*/   
    // AMODE Peripheral Indirect Addressing mode; CHEN disabled; SIZE 16 bit; DIR Reads from RAM address, writes to peripheral address; NULLW disabled; HALF Initiates interrupt when all of the data has been moved; MODE Continuous, Ping-Pong modes are disabled; 
    DMA2CON= 0x2020 & 0x7FFF; //Enable DMA Channel later;
    // IRQSEL CAN1 TX; FORCE disabled; 
    DMA2REQ= 0x46;
    // STA 0; 
    DMA2STAH= 0x00;
    // STA 4096; 
    DMA2STAL= 0x1000;
    // STB 0; 
    DMA2STBH= 0x00;
    // STB 0; 
    DMA2STBL= 0x00;
    // PAD 0; 
    DMA2PAD= 0x00;
    // CNT 7; 
    DMA2CNT= 0x07;
    // Clearing Channel 2 Interrupt Flag;
    IFS1bits.DMA2IF = false;
    // Enabling Channel 2 Interrupt

    // MODE Continuous, Ping-Pong modes are disabled; AMODE Peripheral Indirect Addressing mode; CHEN disabled; HALF Initiates interrupt when all of the data has been moved; SIZE 16 bit; DIR Reads from peripheral address, writes to RAM address; NULLW disabled; 
    DMA3CON= 0x20 & 0x7FFF; //Enable DMA Channel later;
    // IRQSEL CAN1 RX; FORCE disabled; 
    DMA3REQ= 0x22;
    // STA 0; 
    DMA3STAH= 0x00;
    // STA 4096; 
    DMA3STAL= 0x1000;
    // STB 0; 
    DMA3STBH= 0x00;
    // STB 0; 
    DMA3STBL= 0x00;
    // PAD 0; 
    DMA3PAD= 0x00;
    // CNT 7; 
    DMA3CNT= 0x07;
    // Clearing Channel 3 Interrupt Flag;
    IFS2bits.DMA3IF = false;
    // Enabling Channel 3 Interrupt

}
void __attribute__ ((weak)) DMA_Channel0_CallBack(void)
{
    // Add your custom callback code here
}

void DMA_Channel0_Tasks( void )
{
	if(IFS0bits.DMA0IF)
	{
		// DMA Channel0 callback function 
		DMA_Channel0_CallBack();
		
		IFS0bits.DMA0IF = 0;
	}
}
void __attribute__ ((weak)) DMA_Channel1_CallBack(void)
{
    // Add your custom callback code here
}

void __attribute__ ( ( interrupt, no_auto_psv ) ) _DMA1Interrupt( void )
{
	DMA_Channel1_CallBack();
	
    IFS0bits.DMA1IF = 0;
}
void __attribute__ ((weak)) DMA_Channel2_CallBack(void)
{
    // Add your custom callback code here
}

void DMA_Channel2_Tasks( void )
{
	if(IFS1bits.DMA2IF)
	{
		// DMA Channel2 callback function 
		DMA_Channel2_CallBack();
		
		IFS1bits.DMA2IF = 0;
	}
}
void __attribute__ ((weak)) DMA_Channel3_CallBack(void)
{
    // Add your custom callback code here
}

void DMA_Channel3_Tasks( void )
{
	if(IFS2bits.DMA3IF)
	{
		// DMA Channel3 callback function 
		DMA_Channel3_CallBack();
		
		IFS2bits.DMA3IF = 0;
	}
}

/**
  End of File
*/
