/**
  Generated Valenti P.

  @Company
  K-TREONIC

  @File Name
    keyboard_led.c

  @Summary
 Read matrix of switches and animation of leds

  @Description
    This source file provides main entry point for sys
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  dsPIC33EP256GM710
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB 	          :  MPLAB X v5.30
*/



/**
  Section: Included Files
*/
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "globals.h"
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/sw01_in01.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/led01_out01.h"
#include "mcc_generated_files/uart1.h"
#include "mcc_generated_files/watchdog.h"
#include "mcc_generated_files/pwm.h"
/*DEFINE*/
#define BTN_ID_A 0//push button pressed for at least T2 time
#define BTN_ID_H 1//push button pressed for at least T2 time
#define BTN_ID_G 2//push button pressed for at least T2 time
#define BTN_ID_I 3//push button pressed for at least T2 time
#define BTN_ID_N 4//push button pressed for at least T2 time
#define BTN_ID_O 5//push button pressed for at least T2 time
#define BTN_ID_M 6//push button pressed for at least T2 time
#define BTN_ID_C 7//push button pressed for at least T2 time
#define BTN_ID_L 8//push button pressed for at least T2 time
#define BTN_ID_D 9//push button pressed for at least T2 time
#define BTN_ID_K 10//push button pressed for at least T2 time
#define BTN_ID_E 11//push button pressed for at least T2 time
#define BTN_ID_J 12//push button pressed for at least T2 time
#define BTN_ID_F 13//push button pressed for at least T2 time


#define LED_01 0//led on keypad
#define LED_02 1//led on keypad
#define LED_03 2//led on keypad
#define LED_04 3//led on keypad
#define LED_05 4//led on keypad
#define LED_06 5//led on keypad
#define LED_07 6//led on keypad
#define LED_08 7//led on keypad
#define LED_09 8//led on keypad
#define LED_10 9//led on keypad
#define LED_11 10//led on keypad
#define LED_12 11//led on keypad
#define LED_13 12//led on keypad
#define LED_14 13//led on keypad


/*EXTERN Variables*/
extern uint16_t keyboardIdleRateMillisec;
extern system_time_struct_t main_clock;

extern canMsg_t sbcuStat;
extern canMsg_t sbcuInfo;
//extern canMsg_t sbcuErr;
extern canMsg_t sbcuCmd;

/*LOCAL Variables*/
keys_keyboard_t kb_keys;
keys_leds_t kb_led[NUM_COL_MAX];
status_keypad_t kb_status;


/*Prototypes*/
uint16_t ReadColStatus(uint8_t row,uint8_t col);
void SetLedStatus(uint8_t ledx,uint8_t status);
void loadStatusKeysOnCan(void);
void SetLedCmdFromCan(void);
void SetLedStatusDebug(uint8_t ledx,uint8_t status);

void SetLedStatusDebug(uint8_t ledx,uint8_t status){   
     switch(ledx){
        case(LED_08): 
               if(status==1){         
                   LED02_OUT02_On();
                   LED16_OUTB02_On();
                }else{
                   LED02_OUT02_Off();
                   LED16_OUTB02_Off();
                }
                break;
        case(LED_09): 
                  if(status==1){         
                   LED07_OUT07_On();
                   LED21_OUTB07_On();
                }else{
                   LED07_OUT07_Off();
                   LED21_OUTB07_Off();
                }
                break;
        case(LED_12): 
                 if(status==1){         
                   LED04_OUT04_On();
                   LED18_OUTB04_On();
                }else{
                   LED04_OUT04_Off();
                   LED18_OUTB04_Off();
                }
            
                
                break;        
        case(LED_14): 
                 if(status==1){         
                   LED05_OUT05_On();
                   LED19_OUTB05_On();
                }else{
                   LED05_OUT05_Off();
                   LED19_OUTB05_Off();
                }
                break;        
        case(LED_05): 
                if(status==1){         
                   LED06_OUT06_On();
                   LED20_OUTB06_On();
                }else{
                   LED06_OUT06_Off();
                   LED20_OUTB06_Off();
                }
                break;        
        case(LED_10):                 
                if(status==1){         
                   LED01_OUT01_On();
                   LED15_OUTB01_On();
                }else{
                   LED01_OUT01_Off();
                   LED15_OUTB01_Off();
                }
                break;        
        case(LED_06): 
                if(status==1){         
                   LED03_OUT03_On();
                   LED17_OUTB03_On();
                }else{
                   LED03_OUT03_Off();
                   LED17_OUTB03_Off();
                }
                break;        
        case(LED_13): 
                if(status==1){         
                   LED10_OUT10_On();
                   LED24_OUTB10_On();
                }else{
                   LED10_OUT10_Off();
                   LED24_OUTB10_Off();
                }
                
                break;   
         case(LED_11): 
                if(status==1){         
                   LED09_OUT09_On();
                   LED23_OUTB09_On();
                }else{
                   LED09_OUT09_Off();
                   LED23_OUTB09_Off();
                }
                break;
        case(LED_07): 
                if(status==1){         
                   LED08_OUT08_On();
                   LED22_OUTB08_On();
                }else{
                   LED08_OUT08_Off();
                   LED22_OUTB08_Off();
                }
                break;
        case(LED_04): 
                if(status==1){         
                   LED11_OUT11_On();
                   LED26_OUTB12_On();
                }else{
                   LED11_OUT11_Off();
                   LED26_OUTB12_Off();
                }
                break;        
        case(LED_02): 
                if(status==1){         
                   LED12_OUT12_On();
                   LED25_OUTB11_On();
                }else{
                   LED12_OUT12_Off();
                   LED25_OUTB11_Off();
                }
                break;        
        case(LED_03): 
                if(status==1){         
                   LED13_OUT13_On();
                   LED28_OUTB14_On();
                }else{
                   LED13_OUT13_Off();
                   LED28_OUTB14_Off();
                }
                break;        
        case(LED_01): 
                if(status==1){         
                   LED14_OUT14_On();
                   LED27_OUTB13_On();
                }else{
                   LED14_OUT14_Off();
                   LED27_OUTB13_Off();
                }
                break;        
                  
        default:
                break;
      }                          
  
}

void SetLedStatus(uint8_t ledx,uint8_t status){
     switch(ledx){
        case(LED_08): 
               if(status==1){         
                   LED02_OUT02_On();
                   LED16_OUTB02_On();
                }else{
                   LED02_OUT02_Off();
                   LED16_OUTB02_Off();
                }
                break;
        case(LED_07): 
                  if(status==1){         
                   LED07_OUT07_On();
                   LED21_OUTB07_On();
                }else{
                   LED07_OUT07_Off();
                   LED21_OUTB07_Off();
                }
                break;
        case(LED_12): 
                 if(status==1){         
                   LED04_OUT04_On();
                   LED18_OUTB04_On();
                }else{
                   LED04_OUT04_Off();
                   LED18_OUTB04_Off();
                }
            
                
                break;        
        case(LED_14): 
                 if(status==1){         
                   LED05_OUT05_On();
                   LED19_OUTB05_On();
                }else{
                   LED05_OUT05_Off();
                   LED19_OUTB05_Off();
                }
                break;        
        case(LED_05): 
                if(status==1){         
                   LED06_OUT06_On();
                   LED20_OUTB06_On();
                }else{
                   LED06_OUT06_Off();
                   LED20_OUTB06_Off();
                }
                break;        
        case(LED_06):                 
                if(status==1){         
                   LED01_OUT01_On();
                   LED15_OUTB01_On();
                }else{
                   LED01_OUT01_Off();
                   LED15_OUTB01_Off();
                }
                break;        
        case(LED_10): 
                if(status==1){         
                   LED03_OUT03_On();
                   LED17_OUTB03_On();
                }else{
                   LED03_OUT03_Off();
                   LED17_OUTB03_Off();
                }
                break;        
        case(LED_13): 
                if(status==1){         
                   LED10_OUT10_On();
                   LED24_OUTB10_On();
                }else{
                   LED10_OUT10_Off();
                   LED24_OUTB10_Off();
                }
                
                break;   
         case(LED_11): 
                if(status==1){         
                   LED09_OUT09_On();
                   LED23_OUTB09_On();
                }else{
                   LED09_OUT09_Off();
                   LED23_OUTB09_Off();
                }
                break;
        case(LED_09): 
                if(status==1){         
                   LED08_OUT08_On();
                   LED22_OUTB08_On();
                }else{
                   LED08_OUT08_Off();
                   LED22_OUTB08_Off();
                }
                break;
        case(LED_04): 
                if(status==1){         
                   LED11_OUT11_On();
                   LED26_OUTB12_On();
                }else{
                   LED11_OUT11_Off();
                   LED26_OUTB12_Off();
                }
                break;        
        case(LED_02): 
                if(status==1){         
                   LED12_OUT12_On();
                   LED25_OUTB11_On();
                }else{
                   LED12_OUT12_Off();
                   LED25_OUTB11_Off();
                }
                break;        
        case(LED_03): 
                if(status==1){         
                   LED13_OUT13_On();
                   LED28_OUTB14_On();
                }else{
                   LED13_OUT13_Off();
                   LED28_OUTB14_Off();
                }
                break;        
        case(LED_01): 
                if(status==1){         
                   LED14_OUT14_On();
                   LED27_OUTB13_On();
                }else{
                   LED14_OUT14_Off();
                   LED27_OUTB13_Off();
                }
                break;        
                  
        default:
                break;
      }                          
}


void APP_KeyboardInit(void){
    uint8_t nrow,ncol;
     for(nrow=0;nrow< NUM_ROW_MAX;nrow++){//LEFT and RIGHT
      kb_keys.old[nrow]=0;//register old state
      kb_keys.val[nrow]=0;  
      kb_keys.val_long[nrow].wrd16=0;
      kb_keys.val_middle[nrow].wrd16=0; 
      kb_keys.val_short[nrow].wrd16=0;  
      for(ncol=0;ncol < NUM_COL_MAX;ncol++){
        kb_keys.deb_rel_ms[nrow][ncol]=0;
        kb_keys.deb_ms[nrow][ncol]=0;
        SetLedStatus(ncol,LED_STATUS_OFF);
        kb_led[ncol].dimming_100ms=SOLID_LED_OFF;
        kb_led[ncol].timing_on_200ms=0;
        kb_led[ncol].timing_off_200ms=0;
      }     
     } 
}



void SetLeds(void){
    uint8_t nled;
    for(nled=0;nled<NUM_LED_MAX;nled++){
       if (kb_led[nled].dimming_100ms==SOLID_LED_ON){
            SetLedStatus(nled,LED_STATUS_ON);
            kb_led[nled].timing_on_200ms=0;
            kb_led[nled].timing_off_200ms=0;
       }else if (kb_led[nled].dimming_100ms==SOLID_LED_OFF){
                SetLedStatus(nled,LED_STATUS_OFF);
                kb_led[nled].timing_on_200ms=0;
                kb_led[nled].timing_off_200ms=0;
             }else if ((kb_led[nled].dimming_100ms<SOLID_LED_ON)&&(kb_led[nled].dimming_100ms>SOLID_LED_OFF)){
                  if((kb_led[nled].timing_on_200ms==0)&&(kb_led[nled].timing_off_200ms==0)){//restart period
                       SetLedStatus(nled,LED_STATUS_ON);
                       kb_led[nled].timing_on_200ms=((kb_led[nled].dimming_100ms)-1);
                       kb_led[nled].timing_off_200ms=(kb_led[nled].dimming_100ms);
                  }else if((kb_led[nled].timing_on_200ms>0)&&(kb_led[nled].timing_off_200ms>0)){//semipiriod on
                          SetLedStatus(nled,LED_STATUS_ON);
                          kb_led[nled].timing_on_200ms--;
                          //kb_led[nled].timing_off_200ms=0;
                        }else if((kb_led[nled].timing_on_200ms==0)&&(kb_led[nled].timing_off_200ms>0)){//semiperiod off
                                SetLedStatus(nled,LED_STATUS_OFF);
                                kb_led[nled].timing_on_200ms=0;
                                kb_led[nled].timing_off_200ms--;
                             }
             } 
    }
}

void SetLedsDebug(void){
    uint8_t nled;
    uint16_t key_val;
    uint16_t mask=0x0001;
    key_val= (kb_keys.val_short[0].wrd16 | kb_keys.val_short[1].wrd16);
    for(nled=0;nled<NUM_LED_MAX;nled++){  
       mask=0x0001<<nled;
       if ((key_val&mask)!=0 ){
            SetLedStatus(nled,LED_STATUS_ON);
            kb_led[nled].timing_on_200ms=0;
            kb_led[nled].timing_off_200ms=0;
       }else {
                SetLedStatus(nled,LED_STATUS_OFF);
                kb_led[nled].timing_on_200ms=0;
                kb_led[nled].timing_off_200ms=0;
             }
    }
//    MyPrintfMsg("Keys_50ms:");
//    MyPrintfValFormat(key_val,BIN);
//    key_val= (kb_keys.val_middle[0].wrd16 | kb_keys.val_middle[1].wrd16);
//    MyPrintfMsg("Keys_1s:");
//    MyPrintfValFormat(key_val,BIN);   
//    key_val= (kb_keys.val_long[0].wrd16 | kb_keys.val_long[1].wrd16);
//    MyPrintfMsg("Keys_5s:");
//    MyPrintfValFormat(key_val,BIN);
    
      
}

void LedsStartDebug(void){
    uint8_t nled;
    //uint16_t key_val;
    //uint16_t mask=0x0001;
    while( main_clock.flag_100ms==0){
         WATCHDOG_TimerClear();//Reset Watchdog Timer 
    }
    main_clock.flag_100ms=0;
    for(nled=0;nled<NUM_LED_APP_MAX;nled++){
        while(main_clock.flag_100ms==0){
         //MyPrintfMsg("Elapsed 1 seconds");
          WATCHDOG_TimerClear();//Reset Watchdog Timer    
        }
         main_clock.flag_100ms=0;
        SetLedStatusDebug(nled,LED_STATUS_ON);
    } 
    while( main_clock.flag_100ms==0){
         WATCHDOG_TimerClear();//Reset Watchdog Timer 
    }
     main_clock.flag_100ms=0;
    for(nled=0;nled<NUM_LED_APP_MAX;nled++){
        while( main_clock.flag_100ms==0){
         //MyPrintfMsg("Elapsed 1 seconds");
          WATCHDOG_TimerClear();//Reset Watchdog Timer    
        }
        main_clock.flag_100ms=0;
        SetLedStatusDebug(nled,LED_STATUS_OFF);
    } 
    
}

uint16_t ReadColStatus(uint8_t row,uint8_t col){
   bool pin_status;  
   uint8_t result=0xFF;
   if(row==KEYBOARD_RIGHT){
            switch(col){
                case(BTN_ID_J):                               
                                pin_status=SW14_IN14_IsPressed();//BTN F
                                break;
                case(BTN_ID_K):                              
                                pin_status=SW13_IN13_IsPressed();//BTN J
                                break;     
                case(BTN_ID_L):
                                
                                pin_status=SW12_IN12_IsPressed();//BTN E
                                break;
                case(BTN_ID_M):
                                pin_status=SW11_IN11_IsPressed();//BTN K
                                
                                break;   
                case(BTN_ID_N):
                                pin_status=SW10_IN10_IsPressed();//BTN D
                                
                                break;
                case(BTN_ID_G):
                                pin_status=SW09_IN09_IsPressed();//BTN L
                                
                                break;     
                case(BTN_ID_A):
                                pin_status=SW08_IN08_IsPressed();//BTN C
                                
                                break;
                case(BTN_ID_F):
                                pin_status=SW07_IN07_IsPressed();//BTN M
                                
                                break;   
                case(BTN_ID_E):                             
                                pin_status=SW06_IN06_IsPressed();//BTN O
                                break;
                case(BTN_ID_D):                               
                                pin_status=SW05_IN05_IsPressed();//BTN N
                                break;     
                case(BTN_ID_C):                              
                                pin_status=SW04_IN04_IsPressed();//BTN I
                                break;
                case(BTN_ID_O):
                                pin_status=SW03_IN03_IsPressed();//BTN G
                                
                                break;   
                case(BTN_ID_I):
                                pin_status=SW02_IN02_IsPressed();//BTN H
                                
                                break;
                case(BTN_ID_H):
                                pin_status=SW01_IN01_IsPressed();//BTN A
                               
                                break;                    
                default:
                               result=0x80;      
                    break;
            } 
   }else{
           switch(col){
                case(BTN_ID_J):
                                pin_status=SW28_INB14_IsPressed();//BTN F 
                                
                                break;
                case(BTN_ID_K):
                                pin_status=SW27_INB13_IsPressed();//BTN J
                                
                                break;     
                case(BTN_ID_L):
                                pin_status=SW26_INB12_IsPressed();//BTN E
                                
                                break;
                case(BTN_ID_M):
                                pin_status=SW25_INB11_IsPressed();//BTN K
                                
                                break;   
                case(BTN_ID_N):
                                pin_status=SW24_INB10_IsPressed();//BTN D
                                
                                break;
                case(BTN_ID_G):
                                pin_status=SW23_INB09_IsPressed();//BTN L
                                
                                break;     
                case(BTN_ID_A):
                                pin_status=SW22_INB08_IsPressed();//BTN C
                                
                                break;
                case(BTN_ID_F):
                                
                                pin_status=SW21_INB07_IsPressed();//BTN M
                                break;   
                case(BTN_ID_E):
                                
                                pin_status=SW20_INB06_IsPressed();//BTN O
                                break;
                case(BTN_ID_D):
                                
                                pin_status=SW19_INB05_IsPressed();//BTN N
                                break;     
                case(BTN_ID_C):
                                
                                pin_status=SW18_INB04_IsPressed();//BTN I
                                break;
                case(BTN_ID_O):
                                
                                pin_status=SW17_INB03_IsPressed();//BTN G
                                break;   
                case(BTN_ID_I):
                                
                                pin_status=SW16_INB02_IsPressed();//BTN H
                                break;
                case(BTN_ID_H):
                                
                                pin_status=SW15_INB01_IsPressed();//BTN A
                                break;                    
                default:
                    result=0x80;   
                    break;
            } 
   }
   if (result==0xFF){
       result=((pin_status==false)?0:1);
   }
   return(result);
}

/******************************************************************************
 * Function:       KeyboardScanRead(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        
 *                  
 *
 * Note:            None
 *****************************************************************************/
void KeyboardScanRead(void)
{
  uint8_t nrow,ncol;
  //uint8_t pos_key=0;
  //uint8_t ret_val;
  uint16_t key_val;
  uint16_t key_old;
  uint16_t mask=0x0001;
  
  for(nrow=0;nrow< NUM_ROW_MAX;nrow++){//LEFT and RIGHT
      kb_keys.old[nrow]= kb_keys.val[nrow];//register old state
      for(ncol=0;ncol < NUM_COL_MAX;ncol++){
           key_val=ReadColStatus(nrow,ncol);  
           mask=(0x0001<<ncol);        
           if (key_val==1){//pressed
             kb_keys.val[nrow]|=mask;  
             key_old=(kb_keys.old[nrow]& mask);
             key_old>>=ncol; 
             if (key_old==key_val){
               if ( kb_keys.deb_ms[nrow][ncol] > KEY_DEB_10MS_LONG){
                   kb_keys.deb_ms[nrow][ncol] = KEY_DEB_10MS_LONG; 
                   kb_keys.val_long[nrow].wrd16|=mask;  
               }else if ( kb_keys.deb_ms[nrow][ncol] > KEY_DEB_10MS_MIDDLE){
                        kb_keys.val_middle[nrow].wrd16|=mask;  
                     }else if ( kb_keys.deb_ms[nrow][ncol] > KEY_DEB_10MS_SHORT){
                               kb_keys.val_short[nrow].wrd16|=mask;  
                           }              
             }
             kb_keys.deb_ms[nrow][ncol]++;
             kb_keys.deb_rel_ms[nrow][ncol]=0;
           }else{//released
             kb_keys.val[nrow]&=(~mask);    
             kb_keys.deb_ms[nrow][ncol]=0;
             key_old=(kb_keys.old[nrow]& mask);
             key_old>>=ncol; 
             if (key_old==key_val){
                if (kb_keys.deb_rel_ms[nrow][ncol] >KEY_DEB_10MS_RELEASE){
                   kb_keys.val_long[nrow].wrd16 &=(~mask);  
                   kb_keys.val_middle[nrow].wrd16 &=(~mask);  
                   kb_keys.val_short[nrow].wrd16 &=(~mask); 
                }else{
                    kb_keys.deb_rel_ms[nrow][ncol]++;
                }
             }
           }
       
           //ret_val=1;
           //ret_val=ReadColStatus(ncol); 
           //else if (num_keys_pressed==0){
           //    NUMLOCK_LED2_SetLow(); 
           // }
      }
  }  
   
    
}

void loadStatusKeysOnCan(void)
{
  sbcuStat.Stat.status=(uint8_t)kb_status;
  
  sbcuStat.Stat.keys_high=(uint8_t)(kb_keys.val_short[0].byt8.high | kb_keys.val_short[1].byt8.high );
  sbcuStat.Stat.keys_low=(uint8_t)(kb_keys.val_short[0].byt8.low | kb_keys.val_short[1].byt8.low );
  
  sbcuStat.Stat.keysT1_high=(uint8_t)(kb_keys.val_middle[0].byt8.high | kb_keys.val_middle[1].byt8.high );
  sbcuStat.Stat.keysT1_low=(uint8_t)(kb_keys.val_middle[0].byt8.low | kb_keys.val_middle[1].byt8.low );
  
  sbcuStat.Stat.keysT2_high=(uint8_t)(kb_keys.val_long[0].byt8.high | kb_keys.val_long[1].byt8.high );
  sbcuStat.Stat.keysT2_low=(uint8_t)(kb_keys.val_long[0].byt8.low | kb_keys.val_long[1].byt8.low );
}
void SetLedCmdFromCan(void)
{
 uint32_t dutyCycle;  
 kb_led[0].dimming_100ms=sbcuCmd.Cmd.LED01_status;
 kb_led[1].dimming_100ms=sbcuCmd.Cmd.LED02_status;
 kb_led[2].dimming_100ms=sbcuCmd.Cmd.LED03_status;
 kb_led[3].dimming_100ms=sbcuCmd.Cmd.LED04_status;
 kb_led[4].dimming_100ms=sbcuCmd.Cmd.LED05_status;
 kb_led[5].dimming_100ms=sbcuCmd.Cmd.LED06_status;
 kb_led[6].dimming_100ms=sbcuCmd.Cmd.LED07_status;
 kb_led[7].dimming_100ms=sbcuCmd.Cmd.LED08_status;
 kb_led[8].dimming_100ms=sbcuCmd.Cmd.LED09_status;
 kb_led[9].dimming_100ms=sbcuCmd.Cmd.LED10_status;
 kb_led[10].dimming_100ms=0;
 kb_led[11].dimming_100ms=0;
 kb_led[12].dimming_100ms=0;
 kb_led[13].dimming_100ms=sbcuCmd.Cmd.PWM_Led_duty;//0;
 if(kb_led[13].dimming_100ms>10){
    dutyCycle=((kb_led[13].dimming_100ms*PWM3_PERIOD)/100);        
    PWM_DutyCycleSet(PWM_GENERATOR_3, (uint16_t)dutyCycle);
 }
//sbcuCmd;
}


void APP_KeyboardTasks(void){
 uint16_t key_val;     
    switch(kb_status){
        case(KEYPAD_INIT):
                           //APP_KeyboardInit();
                           kb_status=KEYPAD_READY;
                           break;
                           
        case(KEYPAD_CHK_PWR):
            
                           break;
                           
        case(KEYPAD_READY):
                            // Add your application code
                            if (main_clock.flag_10ms==1){   
                                KeyboardScanRead();    
                                loadStatusKeysOnCan();
                            }
                            if (main_clock.flag_100ms==1){  
                                SetLedCmdFromCan();
                                //SetLedsDebug();
                                SetLeds(); 
                            }
                            if (main_clock.flag_5sec==1){
                               RED_LED_SERVICE_Toggle(); 
                            }
                            kb_status=KEYPAD_READY;
                           break;
                           
        case(KEYPAD_ERROR):
                            if (main_clock.flag_1sec==1){
                              RED_LED_SERVICE_Toggle();
                            }
                           break;
                           
        default: 
            break;
    }
    
    sbcuStat.Stat.status=(uint8_t)kb_status;//update Status Message for CAN bus
    
    if (main_clock.flag_10ms==1){   
        main_clock.flag_10ms=0;
    }    
    if (main_clock.flag_100ms==1){
        MyPrintfMsg("Keys_50ms:");
        key_val= (kb_keys.val_short[0].wrd16 | kb_keys.val_short[1].wrd16);
        MyPrintfValFormat(key_val,BIN); 
        main_clock.flag_100ms=0;
    }
    if (main_clock.flag_1sec==1){
        MyPrintfMsg("Status:");
        //MyPrintfValFormat(kb_status,DECIMAL);
        MyPrintfVal(kb_status);
        MyPrintfMsg("\n");
        key_val= (kb_keys.val_middle[0].wrd16 | kb_keys.val_middle[1].wrd16);
        MyPrintfMsg("Keys_1s:");
        MyPrintfValFormat(key_val,BIN);   
        key_val= (kb_keys.val_long[0].wrd16 | kb_keys.val_long[1].wrd16);
        MyPrintfMsg("Keys_5s:");
        MyPrintfValFormat(key_val,BIN);   
        main_clock.flag_1sec=0;
    }
    if (main_clock.flag_5sec==1){         
        main_clock.flag_5sec=0;
    }
    if (main_clock.flag_10sec==1){
        main_clock.flag_10sec=0;
    }
    if (main_clock.flag_1min==1){    
        main_clock.flag_1min=0;
    }
            
    
}