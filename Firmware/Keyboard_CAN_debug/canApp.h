
// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CAN_APP_H
#define	CAN_APP_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "globals.h"
#include "mcc_generated_files/tmr1.h"
#include "mcc_generated_files/can1.h"
#include "canProto.h"



#define TX_REQ_BUFF_0 C1TR01CONbits.TXREQ0
#define TX_REQ_BUFF_1 C1TR01CONbits.TXREQ1
#define TX_REQ_BUFF_2 C1TR23CONbits.TXREQ2
#define TX_REQ_BUFF_3 C1TR23CONbits.TXREQ3

extern volatile can_msg_buff_t can_msg_buff[SIZE_OF_CAN_BUFF]; 
extern canMsg_t sbcuStat;
extern canMsg_t sbcuInfo;
extern canMsg_t sbcuErr;
extern canMsg_t sbcuCmd;



void canAppInit(void);
void canAppTxManager(void);
void loadFwVersionOnCan(fw_version_t fw);



#endif	/* XC_HEADER_TEMPLATE_H */

