#ifndef IO_H
#define	IO_H

#include <xc.h> // include processor files - each processor file is guarded.  

#define     OS_BED          LATDbits.LATD1      // OS BED,      PIN 6
#define     OD_BED          LATDbits.LATD2      // OD BED,      PIN 7
#define     HEAD_UP         LATDbits.LATD3      // HEAD UP,     PIN 8
#define     HEAD_DOWN       LATDbits.LATD4      // HEAD DOWN,   PIN 9
#define     RESET_BED       LATDbits.LATD5      // RESET,       PIN 82
#define     CMD_MODE        LATDbits.LATD6      // CMD MODE,    PIN 83


#define     DEBUG_LED       LATFbits.LATF7      // DEBUG LED,   PIN 92
#define     WATCHDOG        LATEbits.LATE0      // WATCHDOG,    PIN 52

#define     BED_RESET_STATE PORTEbits.RE12      // RESET STATUS,PIN 41

void IOConfig(void);

#endif

