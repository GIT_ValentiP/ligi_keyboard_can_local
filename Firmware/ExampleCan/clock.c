#include "clock.h"

void ClockConfig(void)
{
    #if FOSC > 120 
        #warning  "frequency too high"
    #endif 
    
    CLKDIVbits.PLLPRE   = 0;
    CLKDIVbits.PLLPOST  = 0;
    PLLFBDbits.PLLDIV   = (unsigned int)((FOSC/2)-2); 
    
    // Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(OSCCON | 0x01);
        // Wait for Clock switch to occur
    while (OSCCONbits.COSC!= 0b011);
     // Wait for PLL to lock
    while (OSCCONbits.LOCK!= 1);
}