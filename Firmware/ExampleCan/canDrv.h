

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CAN_DRV_H
#define	CAN_DRV_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "timer.h"

#define NORMAL_MODE         0
#define DISABLE_MODE        1
#define LOOPBACK_MODE       2
#define LISTEN_ONLY_MODE    3
#define CONFIGURATION_MODE  4
#define LISTEN_ALL_MSG_MODE 7

#define SIZE_OF_CAN_BUFF   16


#define TX_REQ_BUFF_0 C1TR01CONbits.TXREQ0
#define TX_REQ_BUFF_1 C1TR01CONbits.TXREQ1
#define TX_REQ_BUFF_2 C1TR23CONbits.TXREQ2
#define TX_REQ_BUFF_3 C1TR23CONbits.TXREQ3


typedef union
{
    struct{
        struct{
            uint16_t ide     :  1;
            uint16_t srr     :  1;
            uint16_t sid     :  11;
            uint16_t unusued :  3;
        }stdid;

        struct{
            uint16_t extidh  :  12;
            uint16_t unusued :  4;
        }extid;

        struct{
            uint16_t dlc     :  4;
            uint16_t rb0     :  1;
            uint16_t unusued :  3;
            uint16_t rb1     :  1;
            uint16_t rtr     :  1;
            uint16_t extidl  :  6;        
        }ctrl;

        struct{
            uint16_t byte0   :  8;
            uint16_t byte1   :  8;
        }wrd01;

        struct{
            uint16_t byte2   :  8;
            uint16_t byte3   :  8;
        }wrd23;

        struct{
            uint16_t byte4   :  8;
            uint16_t byte5   :  8;
        }wrd45;

        struct{
            uint16_t byte6   :  8;
            uint16_t byte7   :  8;
        }wrd67;

        struct{
            uint16_t unusued :  8;
            uint16_t filhit  :  5;
            uint16_t padding :  3;    
        }filt;  
    }msg;
    uint16_t    wordBuff[8];
    uint8_t     byteBuff[16];
}can_msg_buff_t;




void canDrvInit(void);
void canDrvSetFilters(uint32_t *id);

#endif	/* XC_HEADER_TEMPLATE_H */

