/* 
 * File:   timer.c
 * Author: area.rs3
 *
 * Created on 12 settembre 2019, 14.47
 */

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include "timer.h"


Timer_t swTimer_1ms;
Timer_t swTimer_10ms;
Timer_t swTimer_100ms;
Timer_t swTimer_500ms;


void Timer1Init(void)
{
    T1CON = 0;
    TMR1 = 0x00; // Clear timer register
    PR1 = 60000; // Load the period value
    
    IPC0bits.T1IP = 7; // Set Timer 1 Interrupt Priority Level
    IFS0bits.T1IF = 0; // Clear Timer 1 Interrupt Flag  
    IEC0bits.T1IE = 1; // Enable Timer1 interrupt
   
    T1CONbits.TON = 1; // Start Timer
}

void SystemTimerInit(void)
{
    Timer_Init();
    Timer_Set(&swTimer_1ms,T_1MS);
    Timer_Set(&swTimer_10ms,T_10MS);
    Timer_Set(&swTimer_100ms,T_100MS);
    Timer_Set(&swTimer_500ms,T_500MS);
    Timer_Enable();
}