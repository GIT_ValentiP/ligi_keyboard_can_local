/* 
 * File:   interrupts.c
 * Author: area.rs3
 *
 * Created on 13 settembre 2019, 16.18
 */
#include <xc.h>
#include "timer.h"
#include <stdio.h>
#include <stdlib.h>



void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void)
{
    /* Interrupt Service Routine code goes here */
    Timer_Isr1ms();
    IFS0bits.T1IF = 0; //Clear Timer1 interrupt flag
}

