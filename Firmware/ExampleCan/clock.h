#ifndef _CLOCK_H
#define	_CLOCK_H

#include <xc.h> // include processor files - each processor file is guarded.

#define FOSC 120
#define FCY  (FOSC/2)
 

void ClockConfig(void);
#endif	/* XC_HEADER_TEMPLATE_H */
