
// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CAN_APP_H
#define	CAN_APP_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "timer.h"
#include "canDrv.h"
#include "canProto.h"

#define RX_BUFF_DIM 10
#define TX_BUFF_DIM 4



extern volatile can_msg_buff_t can_msg_buff[SIZE_OF_CAN_BUFF]; 
extern canMsg_t sbcuStat;
extern canMsg_t sbcuInfo;
extern canMsg_t sbcuErr;
extern canMsg_t sbcuCmd;


typedef enum
{
    highest     = 0,
    veryHigh    = 1,
    high        = 2,
    mediumHigh  = 3,
    medium      = 4,
    mediumLow   = 5,
    low         = 6,               
    lower       = 7,        
}priority_t;


typedef enum
{
    BroadcastMsg        = 0,
    MainPcBoard         = 1,
    RotationalPcBoard   = 2,
    ControlBoard        = 3,
    LightAndGasBoard    = 4,
    MotorBoard          = 5,   
    SurgicalBedBoard    = 6,
}boards_t;

typedef struct 
{
    priority_t  priority; 
    msgid_t     msgId;
    boards_t    recipient;
    uint16_t    period;
    canMsg_t    *msg;
}CanTxObjectDescriptor_t;

typedef struct 
{
    msgid_t     msgId;
    boards_t    recipient;
    canMsg_t    *msg;
}CanRxObjectDescriptor_t;

void canAppInit(void);
void canAppTxManager(void);
void loadFwVersionOnCan(const char* fw);


#endif	/* XC_HEADER_TEMPLATE_H */

