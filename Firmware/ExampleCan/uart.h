#ifndef _UART_H
#define	_UART_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include "clock.h" // include processor files - each processor file is guarded.

#define     BAUDRATE    9600
#define     BRGVAL      (unsigned int)(((unsigned long)FCY*1000000/(unsigned long)BAUDRATE)/16)-1


void UartInit(void);
//void SendStringToMainPC(char *Message);
void MyPrintfVal(long val);
void MyPrintfMsg(char* mex);
void MyPrintfFwVersion(const char *fw);
#endif	

