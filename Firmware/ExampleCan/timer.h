// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef TIMER_H
#define	TIMER_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "timer.h"


#define T_1MS       1
#define T_10MS      10
#define T_100MS     100
#define T_500MS     500



extern Timer_t swTimer_1ms;
extern Timer_t swTimer_10ms;
extern Timer_t swTimer_100ms;
extern Timer_t swTimer_500ms;


void Timer1Init(void);
void SystemTimerInit(void);

#endif	/* XC_HEADER_TEMPLATE_H */

