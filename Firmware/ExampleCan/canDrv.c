/*
 * File:   can.c
 * Author: area.rs19
 *
 * Created on 12 luglio 2019, 16.02
 */


// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

//#include <pps.h>
#include <string.h>
#include "canDrv.h"
#include "canApp.h"

static void canChangeMode(uint8_t mode);
static void canRemapPin(void);

extern const CanRxObjectDescriptor_t CanRxObjectDescriptor[];

volatile can_msg_buff_t can_msg_buff[SIZE_OF_CAN_BUFF]__attribute__((aligned(SIZE_OF_CAN_BUFF * 16)));

 //
 // uint16_t can_msg_buff[SIZE_OF_CAN_BUFF][8]__attribute__((aligned(SIZE_OF_CAN_BUFF * 16)));




   #define FCAN    60000000
   #define BITRATE 500000
   #define NTQ     20  // 20 Time Quanta in a Bit Time
   #define BRP_VAL ( (FCAN / (2 * NTQ * BITRATE)) - 1 )

void canDrvInit(void)
{
    //uint32_t id=0;
    canRemapPin();
    
    canChangeMode(CONFIGURATION_MODE);
    
    C1CTRL1bits.WIN         = 0;
    
    //baud rate configuration 500kbs
    C1CFG1bits.SJW          = 0x3;
    C1CFG1bits.BRP          = BRP_VAL;
    C1CFG2bits.SEG1PH       = 0x7;
    C1CFG2bits.SEG2PHTS     = 0x1;
    C1CFG2bits.SEG2PH       = 0x5;
    C1CFG2bits.PRSEG        = 0x4;
    C1CFG2bits.SAM          = 0x1;
    C1CFG2bits.WAKFIL       = 0x0;
    
    //no fifo, 16 message buffers
    C1FCTRLbits.DMABS       = 0x4;
    C1FCTRLbits.FSA         = 0x1f;
    
    //DMA channel 0 for tx  
    DMA0CONbits.SIZE        = 0x0;
    DMA0CONbits.DIR         = 0x1;
    DMA0CONbits.AMODE       = 0x2;
    DMA0CONbits.MODE        = 0x0;
    DMA0REQ                 = 70;
    DMA0CNT                 = 7;
    DMA0PAD                 = (volatile unsigned int)&C1TXD;
    DMA0STAL                = (uint16_t)(&can_msg_buff);
    DMA0STAH                = 0;
    DMA0CONbits.CHEN        = 1;
    
    
    //set-up firts 4 buffer as trasmition buffer
    C1TR01CONbits.TXEN0     = 1;
    C1TR01CONbits.TX0PRI    = 3;
    C1TR01CONbits.TXEN1     = 1;
    C1TR01CONbits.TX0PRI    = 2;
    C1TR23CONbits.TXEN2     = 1;
    C1TR23CONbits.TX2PRI    = 1;
    C1TR23CONbits.TXEN3     = 1;
    C1TR23CONbits.TX3PRI    = 0;
   
    
    canChangeMode(NORMAL_MODE);
   
}


void canDrvSetFilters(uint32_t *id)
{
    //set up mask
    canChangeMode(CONFIGURATION_MODE);
    
    C1CTRL1bits.WIN         = 1;

    C1RXM0SIDbits.SID       = 0b00011111111;
    C1RXM0SIDbits.MIDE      = 1;
    C1RXM0SIDbits.EID       = 0b11;
    C1RXM0EID               = 0b1111111100000000;
    
    
    //set up filters 
    if(((*(id))>>16) &0x000003ff)
    {
        C1RXF0SIDbits.SID       = (((*(id))>>18)& 0x000000ff);
        C1RXF0SIDbits.EID       = (((*(id))>>16)& 0x00000003);
        C1RXF0SIDbits.EXIDE     = 1;
        C1RXF0EID               = ((*(id)) & 0x0000ff00);
        C1BUFPNT1bits.F0BP      = 4; 
        C1FEN1bits.FLTEN0       = 1; 
    }

    if(((*(id+1))>>16) &0x000003ff)
    {
        C1RXF1SIDbits.SID       = (((*(id+1))>>18)& 0x000000ff);
        C1RXF1SIDbits.EID       = (((*(id+1))>>16)& 0x00000003);
        C1RXF1SIDbits.EXIDE     = 1;
        C1RXF1EID               = ((*(id+1)) & 0x0000ff00);
        C1BUFPNT1bits.F1BP      = 5; 
        C1FEN1bits.FLTEN1       = 1; 
    }
    
    if(((*(id+2))>>16) &0x000003ff)
    {
        C1RXF2SIDbits.SID       = (((*(id+2))>>18)& 0x000000ff);
        C1RXF2SIDbits.EID       = (((*(id+2))>>16)& 0x00000003);
        C1RXF2SIDbits.EXIDE     = 1;
        C1RXF2EID               = ((*(id+2)) & 0x0000ff00);
        C1BUFPNT1bits.F2BP      = 6; 
        C1FEN1bits.FLTEN2       = 1; 
    }
    
    if(((*(id+3))>>16) &0x000003ff)
    {
        C1RXF3SIDbits.SID       = (((*(id+3))>>18)& 0x000000ff);
        C1RXF3SIDbits.EID       = (((*(id+3))>>16)& 0x00000003);
        C1RXF3SIDbits.EXIDE     = 1;
        C1RXF3EID               = ((*(id+3)) & 0x0000ff00);
        C1BUFPNT1bits.F3BP      = 7; 
        C1FEN1bits.FLTEN3       = 1; 
    }

    if(((*(id+4))>>16) &0x000003ff)
    {
        C1RXF4SIDbits.SID       = (((*(id+4))>>18)& 0x000000ff);
        C1RXF4SIDbits.EID       = (((*(id+4))>>16)& 0x00000003);
        C1RXF4SIDbits.EXIDE     = 1;
        C1RXF4EID               = ((*(id+4)) & 0x0000ff00);
        C1BUFPNT2bits.F4BP      = 8; 
        C1FEN1bits.FLTEN4       = 1; 
    }    

    if(((*(id+5))>>16) &0x000003ff)
    {
        C1RXF5SIDbits.SID       = (((*(id+5))>>18)& 0x000000ff);
        C1RXF5SIDbits.EID       = (((*(id+5))>>16)& 0x00000003);
        C1RXF5SIDbits.EXIDE     = 1;
        C1RXF5EID               = ((*(id+5)) & 0x0000ff00);
        C1BUFPNT2bits.F5BP      = 9; 
        C1FEN1bits.FLTEN5       = 1; 
    }

    if(((*(id+6))>>16) &0x000003ff)
    {
        C1RXF6SIDbits.SID       = (((*(id+6))>>18)& 0x000000ff);
        C1RXF6SIDbits.EID       = (((*(id+6))>>16)& 0x00000003);
        C1RXF6SIDbits.EXIDE     = 1;
        C1RXF6EID               = ((*(id+6)) & 0x0000ff00);
        C1BUFPNT2bits.F6BP      = 10; 
        C1FEN1bits.FLTEN6       = 1; 
    }

    if(((*(id+7))>>16) &0x000003ff)
    {
        C1RXF7SIDbits.SID       = (((*(id+7))>>18)& 0x000000ff);
        C1RXF7SIDbits.EID       = (((*(id+7))>>16)& 0x00000003);
        C1RXF7SIDbits.EXIDE     = 1;
        C1RXF7EID               = ((*(id+7)) & 0x0000ff00);
        C1BUFPNT2bits.F7BP      = 11; 
        C1FEN1bits.FLTEN7       = 1; 
    }

    if(((*(id+8))>>16) &0x000003ff)
    {
        C1RXF8SIDbits.SID       = (((*(id+8))>>18)& 0x000000ff);
        C1RXF8SIDbits.EID       = (((*(id+8))>>16)& 0x00000003);
        C1RXF8SIDbits.EXIDE     = 1;
        C1RXF8EID               = ((*(id+8)) & 0x0000ff00);
        C1BUFPNT3bits.F8BP      = 12; 
        C1FEN1bits.FLTEN8       = 1; 
    }

    if(((*(id+9))>>16) &0x000003ff)
    {
        C1RXF9SIDbits.SID       = (((*(id+9))>>18)& 0x000000ff);
        C1RXF9SIDbits.EID       = (((*(id+9))>>16)& 0x00000003);
        C1RXF9SIDbits.EXIDE     = 1;
        C1RXF9EID               = ((*(id+9)) & 0x0000ff00);
        C1BUFPNT3bits.F9BP      = 13; 
        C1FEN1bits.FLTEN9       = 1; 
    } 
    /*
    
    C1FEN1 = 0;
   
    C1RXF0SIDbits.SID       = 0x7ff;
    C1RXF0SIDbits.EID       = 0;
    C1RXF0SIDbits.EXIDE     = 1;
    C1RXF0EID               = 0;
    C1BUFPNT1bits.F0BP      = 4; 
    C1FEN1bits.FLTEN0       = 1; 
    
    
 
    
    C1FMSKSEL1bits.F0MSK        = 0;
  */
    
    C1FMSKSEL1              = 0x0;
    C1FMSKSEL2              = 0x0;
    
    DMA1CONbits.SIZE        = 0x0;
    DMA1CONbits.DIR         = 0x0;
    DMA1CONbits.AMODE       = 0x2;
    DMA1CONbits.MODE        = 0x0;
    DMA1REQ                 = 34;
    DMA1CNT                 = 7;
    DMA1PAD                 = (volatile unsigned int)&C1RXD;
    DMA1STAL                = (uint16_t)(&can_msg_buff);
    DMA1STAH                = 0;
    DMA1CONbits.CHEN        = 1;
    
   
 
    
    
    IFS2bits.C1IF = 0;
    IEC2bits.C1IE   = 1;     
    IPC8bits.C1IP   = 7;
    C1INTFbits.RBIF = 0;
    C1INTEbits.RBIE = 1;
    
    
    C1CTRL1bits.WIN         = 0;
    
    canChangeMode(NORMAL_MODE);  
}

static void canChangeMode(uint8_t mode)
{
    C1CTRL1bits.REQOP = mode;
    while(C1CTRL1bits.OPMODE != mode);
}

static void canRemapPin(void)
{
    PPSUnLock;
    RPINR26bits.C1RXR   = 24;
    RPOR1bits.RP36R     = 14; 
    PPSLock;
}


void __attribute__ ( (interrupt, no_auto_psv) ) _C1Interrupt( void )
{
    uint8_t i=0; 
    
    for(i=TX_BUFF_DIM; i<(RX_BUFF_DIM+TX_BUFF_DIM); i++)
    {
        if( (C1RXFUL1 & (1<<i)) )
        {
            memcpy(CanRxObjectDescriptor[i-TX_BUFF_DIM].msg->byte,(uint8_t *)&can_msg_buff[i].byteBuff[6],8); 
            C1RXFUL1 &= (~(1<<i)); 
        }
    }
    
    IFS2bits.C1IF = 0;      // clear interrupt flag
    C1INTFbits.RBIF = 0;
}































