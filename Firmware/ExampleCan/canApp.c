/*
 * File:   can.c
 * Author: area.rs19
 *
 * Created on 12 luglio 2019, 16.02
 */


// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include "canApp.h"


canMsg_t dummy;
canMsg_t sbcuStat;
canMsg_t sbcuInfo;
canMsg_t sbcuErr;
canMsg_t sbcuCmd;


Timer_t canTmr[4];


const boards_t  MyBoard = SurgicalBedBoard;
const CanTxObjectDescriptor_t CanTxObjectDescriptor[TX_BUFF_DIM]=
{
/*n*/   //priority  msgId       recipient       period  &msgptr
/*0*/   {high,      SBCUstat,   MainPcBoard,    50,     &sbcuStat},
/*1*/   {medium,    SBCUerr,    MainPcBoard,    500,    &sbcuErr},
/*2*/   {lower,     SBCUinfo,   MainPcBoard,    5000,   &sbcuInfo},
/*3*/   {lower,     nullMsg,    MotorBoard,     100,    &dummy},
};

const CanRxObjectDescriptor_t CanRxObjectDescriptor[RX_BUFF_DIM]=
{
//space reserved for "MyBoard" recipient  messages
/*n*/   //msgId      recipient          &msgptr
/*0*/   {SBCUcmd,    SurgicalBedBoard,  &sbcuCmd},
/*1*/   {nullMsg,    SurgicalBedBoard,  &dummy},
/*2*/   {nullMsg,    SurgicalBedBoard,  &dummy},
/*3*/   {nullMsg,    SurgicalBedBoard,  &dummy},
/*4*/   {nullMsg,    SurgicalBedBoard,  &dummy},

 
//space reserved for "Broadcast" messages
/*5*/   {nullMsg,    BroadcastMsg,     &dummy},
/*6*/   {nullMsg,    BroadcastMsg,     &dummy},
/*7*/   {nullMsg,    BroadcastMsg,     &dummy},
/*8*/   {nullMsg,    BroadcastMsg,     &dummy},
/*9*/   {nullMsg,    BroadcastMsg,     &dummy},

};




void canAppInit(void)
{
    uint8_t i;
    uint32_t id =0;
    uint32_t rcv_id[10];
    
    
    
    for(i=0; i<4; i++)
    {
        id =  ( (((uint32_t)(CanTxObjectDescriptor[i].priority))<<26) |\
                (((uint32_t)(CanTxObjectDescriptor[i].msgId))<<16)|\
                (((uint32_t)(CanTxObjectDescriptor[i].recipient))<<8)|\
                ((uint32_t)(MyBoard)));
        
        can_msg_buff[i].msg.stdid.ide       = 1;
        can_msg_buff[i].msg.stdid.srr       = 0;
        can_msg_buff[i].msg.stdid.sid       = ((id>>18)&0x000007ff);
        can_msg_buff[i].msg.extid.extidh    = ((id>>6)&0x00000fff);
        can_msg_buff[i].msg.ctrl.extidl     = ((id)&0x0000003f);
        can_msg_buff[i].msg.ctrl.rb1        = 0;
        can_msg_buff[i].msg.ctrl.rb0        = 0;
        can_msg_buff[i].msg.ctrl.rtr        = 0;
        can_msg_buff[i].msg.ctrl.dlc        = 8;
        
        can_msg_buff[i].msg.wrd01.byte0     = *(CanTxObjectDescriptor[i].msg->byte);
        can_msg_buff[i].msg.wrd01.byte1     = *(CanTxObjectDescriptor[i].msg->byte+1);
        can_msg_buff[i].msg.wrd23.byte2     = *(CanTxObjectDescriptor[i].msg->byte+2);
        can_msg_buff[i].msg.wrd23.byte3     = *(CanTxObjectDescriptor[i].msg->byte+3);
        can_msg_buff[i].msg.wrd45.byte4     = *(CanTxObjectDescriptor[i].msg->byte+4);
        can_msg_buff[i].msg.wrd45.byte5     = *(CanTxObjectDescriptor[i].msg->byte+5);
        can_msg_buff[i].msg.wrd67.byte6     = *(CanTxObjectDescriptor[i].msg->byte+6);
        can_msg_buff[i].msg.wrd67.byte7     = *(CanTxObjectDescriptor[i].msg->byte+7);
        Timer_Set(&canTmr[i],CanTxObjectDescriptor[i].period);
    }
    
    if(CanTxObjectDescriptor[0].msgId != nullMsg)
    {
        TX_REQ_BUFF_0 = 1;
    }
    
    if(CanTxObjectDescriptor[1].msgId != nullMsg)
    {
        TX_REQ_BUFF_1 = 1;
    }
    
    if(CanTxObjectDescriptor[2].msgId != nullMsg)
    {
        TX_REQ_BUFF_2 = 1;
    }
    
    if(CanTxObjectDescriptor[3].msgId != nullMsg)
    {
        TX_REQ_BUFF_3 = 1;
    } 
    
    for(i=0; i<10; i++)
    {
        rcv_id[i] = 0;
        rcv_id[i] =  ((((uint32_t)(CanRxObjectDescriptor[i].msgId))<<16)|\
        (((uint32_t)(CanRxObjectDescriptor[i].recipient))<<8));
    }
    
    canDrvSetFilters(rcv_id);
    
}


void canAppTxManager(void)
{
    if(true == Timer_IsExpired(&canTmr[0]))
    {
        can_msg_buff[0].msg.wrd01.byte0     = *(CanTxObjectDescriptor[0].msg->byte);
        can_msg_buff[0].msg.wrd01.byte1     = *(CanTxObjectDescriptor[0].msg->byte+1);
        can_msg_buff[0].msg.wrd23.byte2     = *(CanTxObjectDescriptor[0].msg->byte+2);
        can_msg_buff[0].msg.wrd23.byte3     = *(CanTxObjectDescriptor[0].msg->byte+3);
        can_msg_buff[0].msg.wrd45.byte4     = *(CanTxObjectDescriptor[0].msg->byte+4);
        can_msg_buff[0].msg.wrd45.byte5     = *(CanTxObjectDescriptor[0].msg->byte+5);
        can_msg_buff[0].msg.wrd67.byte6     = *(CanTxObjectDescriptor[0].msg->byte+6);
        can_msg_buff[0].msg.wrd67.byte7     = *(CanTxObjectDescriptor[0].msg->byte+7);
        
        if(CanTxObjectDescriptor[0].msgId != nullMsg)
        {
            TX_REQ_BUFF_0 = 1;
        }
        Timer_Set(&canTmr[0],CanTxObjectDescriptor[0].period); 
    }
    
    if(true == Timer_IsExpired(&canTmr[1]))
    {
        can_msg_buff[1].msg.wrd01.byte0     = *(CanTxObjectDescriptor[1].msg->byte);
        can_msg_buff[1].msg.wrd01.byte1     = *(CanTxObjectDescriptor[1].msg->byte+1);
        can_msg_buff[1].msg.wrd23.byte2     = *(CanTxObjectDescriptor[1].msg->byte+2);
        can_msg_buff[1].msg.wrd23.byte3     = *(CanTxObjectDescriptor[1].msg->byte+3);
        can_msg_buff[1].msg.wrd45.byte4     = *(CanTxObjectDescriptor[1].msg->byte+4);
        can_msg_buff[1].msg.wrd45.byte5     = *(CanTxObjectDescriptor[1].msg->byte+5);
        can_msg_buff[1].msg.wrd67.byte6     = *(CanTxObjectDescriptor[1].msg->byte+6);
        can_msg_buff[1].msg.wrd67.byte7     = *(CanTxObjectDescriptor[1].msg->byte+7);
        if(CanTxObjectDescriptor[1].msgId != nullMsg)
        {
            TX_REQ_BUFF_1 = 1;
        } 
        Timer_Set(&canTmr[1],CanTxObjectDescriptor[1].period); 
    }
    
    if(true == Timer_IsExpired(&canTmr[2]))
    {
        can_msg_buff[2].msg.wrd01.byte0     = *(CanTxObjectDescriptor[2].msg->byte);
        can_msg_buff[2].msg.wrd01.byte1     = *(CanTxObjectDescriptor[2].msg->byte+1);
        can_msg_buff[2].msg.wrd23.byte2     = *(CanTxObjectDescriptor[2].msg->byte+2);
        can_msg_buff[2].msg.wrd23.byte3     = *(CanTxObjectDescriptor[2].msg->byte+3);
        can_msg_buff[2].msg.wrd45.byte4     = *(CanTxObjectDescriptor[2].msg->byte+4);
        can_msg_buff[2].msg.wrd45.byte5     = *(CanTxObjectDescriptor[2].msg->byte+5);
        can_msg_buff[2].msg.wrd67.byte6     = *(CanTxObjectDescriptor[2].msg->byte+6);
        can_msg_buff[2].msg.wrd67.byte7     = *(CanTxObjectDescriptor[2].msg->byte+7);
        if(CanTxObjectDescriptor[2].msgId != nullMsg)
        {
            TX_REQ_BUFF_2 = 1;
        }
        Timer_Set(&canTmr[2],CanTxObjectDescriptor[2].period); 
    }
    
    if(true == Timer_IsExpired(&canTmr[3]))
    {
        can_msg_buff[3].msg.wrd01.byte0     = *(CanTxObjectDescriptor[3].msg->byte);
        can_msg_buff[3].msg.wrd01.byte1     = *(CanTxObjectDescriptor[3].msg->byte+1);
        can_msg_buff[3].msg.wrd23.byte2     = *(CanTxObjectDescriptor[3].msg->byte+2);
        can_msg_buff[3].msg.wrd23.byte3     = *(CanTxObjectDescriptor[3].msg->byte+3);
        can_msg_buff[3].msg.wrd45.byte4     = *(CanTxObjectDescriptor[3].msg->byte+4);
        can_msg_buff[3].msg.wrd45.byte5     = *(CanTxObjectDescriptor[3].msg->byte+5);
        can_msg_buff[3].msg.wrd67.byte6     = *(CanTxObjectDescriptor[3].msg->byte+6);
        can_msg_buff[3].msg.wrd67.byte7     = *(CanTxObjectDescriptor[3].msg->byte+7);
        if(CanTxObjectDescriptor[3].msgId != nullMsg)
        {
            TX_REQ_BUFF_3 = 1;
        }
        Timer_Set(&canTmr[3],CanTxObjectDescriptor[3].period); 
    }   
}



void loadFwVersionOnCan(const char* fw)
{
    uint8_t temp = 0; 
    temp = (uint8_t)(fw[2]*10);
    temp += (uint8_t)fw[3];
    if(temp > 0xf)
    {
        temp = 0xf;
    }
    sbcuInfo.Info.vers.major = temp;
    
    temp = 0; 
    temp = (uint8_t)(fw[0]*10);
    temp += (uint8_t)fw[1];
    if(temp > 0xf)
    {
        temp = 0xf;
    }
    sbcuInfo.Info.vers.minor = temp;
}
