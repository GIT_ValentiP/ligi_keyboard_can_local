#include "io.h"
#include "stdbool.h"

static void LATset(void); //per assicurarsi che i pin siano spenti quando la direzione (TRIS) � definita



void IOConfig(void) 
{
    LATset();
    
    ANSELA = ANSELB = ANSELC = ANSELD = ANSELE = ANSELF = ANSELG = 0x0000; //deactivates analog featureS
    TRISA = TRISB = TRISC = TRISD = TRISE = TRISF = TRISG = 0x0000; // Setting all GPIO as output, by default
           
    TRISEbits.TRISE12  = 1;   //INPUT DEFINITION FOR BED RESET STATE
    TRISAbits.TRISA8   = 1;   //INPUT RX CAN  
    
    /*ADC1 inputs*/
    TRISAbits.TRISA0   = 1;   //INPUT DEFINITION FOR AN0       
    TRISAbits.TRISA1   = 1;   //INPUT DEFINITION FOR AN1 
    TRISBbits.TRISB0   = 1;   //INPUT DEFINITION FOR AN2

    
}

static void LATset(void) 
{ 
    CMD_MODE                = 0;
    RESET_BED               = 1;
    HEAD_DOWN               = 0;
    HEAD_UP                 = 0;
    OD_BED                  = 0;
    OS_BED                  = 0;
    WATCHDOG                = 1;    
    DEBUG_LED               = 0; 
}


