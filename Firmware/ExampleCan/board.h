#ifndef _BOARD_H
#define	_BOARD_H

#include <xc.h> // include processor files - each processor file is guarded.
#include <stdio.h>
#include <stdlib.h>
#include "clock.h"
#include "timer.h"
//#include "swtimer.h" 
#include "io.h"
#include "uart.h"
#include "canDrv.h"

void InitBoard(void);
#endif	/* XC_HEADER_TEMPLATE_H */
