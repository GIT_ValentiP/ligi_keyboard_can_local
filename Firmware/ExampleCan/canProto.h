

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef CAN_PROTO_H
#define	CAN_PROTO_H

#include <xc.h> // include processor files - each processor file is guarded. 

typedef enum
{
    nullMsg     = 0x0,
    SBCUstat    = 0x3F9,
    SBCUerr     = 0x3F8,
    SBCUinfo    = 0x3F7,
    SBCUcmd     = 0x3F6,
    lastMsg     = 0x3FF               
}msgid_t;

typedef struct
{
    uint8_t status;
    uint8_t X_pos_high;
    uint8_t X_pos_low;
    uint8_t Y_pos_high;
    uint8_t Y_pos_low;
    uint8_t Z_pos_high;
    uint8_t Z_pos_low;
    uint8_t unusued;
}sbcuStat_t;

typedef struct
{
    struct
    {
        uint8_t major :4;
        uint8_t minor :4;
    }vers;
    uint8_t unusued[7];
}sbcuInfo_t;

typedef struct
{
    struct
    {
        uint8_t err_24V_supply_out_of_range         :  1; //LSB
        uint8_t err_7V_supply_out_of_range          :  1; 
        uint8_t err_5V_supply_out_of_range          :  1; 
        uint8_t err_unable_to_complete_selftest     :  1;
        uint8_t err_unable_to_complete_reset        :  1;
        uint8_t err_cannot_move_bed                 :  1;
        uint8_t err_no_encoder_info_received        :  1; 
        uint8_t err_received_setpoint_out_of_range  :  1;
    }byte1;
    
    uint8_t unusued0;
    uint8_t unusued1;
    uint8_t unusued2;
    uint8_t unusued3;
    uint8_t unusued4;
    uint8_t unusued5;

    struct
    {
        uint8_t wrn_24V_supply_low                  :  1; //LSB
        uint8_t wrn_24V_supply_high                 :  1; 
        uint8_t wrn_forbidden_cmd_received          :  1;  
        uint8_t padding                             :  5;
    }byte8;
}sbcuErr_t;


typedef struct
{
    struct
    {
        uint8_t x_data_valid        :1;//LSB
        uint8_t y_data_valid        :1;
        uint8_t z_data_valid        :1; 
        uint8_t padding             :5; 
    }datavalid;
    uint8_t unusued0;
    uint8_t Vx;
    uint8_t unusued1;
    uint8_t Vy;
    uint8_t unusued2;
    uint8_t Vz;
    struct
    {
        uint8_t os_bed              :1;//LSB
        uint8_t od_bed              :1;
        uint8_t head_up             :1; 
        uint8_t head_down           :1;
        uint8_t reset_bed           :1; 
        uint8_t cmd_mode            :1;
        uint8_t errors_reset_board  :1;
        uint8_t padding             :1; //MSB
    }cmdflags; 
}sbcuCmd_t;


typedef union 
{
    sbcuStat_t          Stat;
    sbcuInfo_t          Info;
    sbcuErr_t           Err;
    sbcuCmd_t           Cmd;
    uint8_t             byte[8];
}canMsg_t;

#endif	/* XC_HEADER_TEMPLATE_H */

