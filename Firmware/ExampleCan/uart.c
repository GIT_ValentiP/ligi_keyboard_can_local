/* 
 * File:   uart.c
 * Author: area.rs3
 *
 * Created on 18 settembre 2019, 14.59
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uart.h"

char msg[255];


static void SendStringToMainPC(char *Message);

void UartInit(void)
{
    U1MODE             = 0x0000;       // UxTX and UxRX pins are enabled and used; UxCTS and UxRTS/BCLKx pins are controlled by PORT latches
    U1STA              = 0x0000;
    
    __builtin_write_OSCCONL(OSCCON & 0xbf); // unlock PPS

    RPOR11bits.RP125R  = 0x0001;       // PIN 97 -> UART1:U1TX
    RPINR18bits.U1RXR  = 124;          // RC13->UART1:U1RX (si scrive direttamente del relativo RPI)

    __builtin_write_OSCCONL(OSCCON | 0x40); // lock PPS
    
    U1BRG              = BRGVAL;
    U1STAbits.URXISEL  = 0;            // Interrupt after one RX character is received;

    IFS0bits.U1RXIF    = 0;            //reset interrupt flag
    IEC0bits.U1RXIE    = 1;            //Enable UART RX interrupt
    //IPC2bits.U1RXIP    = 7;            //set priority
    IPC2bits.U1RXIP    = 5;
    
    U1MODEbits.UARTEN  = 1;            // Enable UART 
    U1STAbits.UTXEN    = 1;            // Transmit is enabled, UxTX pin is controlled by UARTx (da inserire dopo UARTEN)   
    
}

void __attribute__((interrupt,no_auto_psv)) _U1RXInterrupt(void)
{
   // char var = U1RXREG;
     IFS0bits.U1RXIF = 0;
}

//SEND STRING TO MAIN PC FUNCTION (UART 2)
void SendString(void)
{
    U1TXREG = 'a';
    /*int  i = 0;
    int var;
    while (Message[i] != '\0')
    {
        var = Message[i];
        U1TXREG = var;
        while (!U1STAbits.TRMT);
        i++;
    }*/
}

//SEND STRING TO MAIN PC FUNCTION (UART 1)
static void SendStringToMainPC(char *Message)
{
    int i = 0;
    int var;
    while (Message[i] != '\0')
    {
        var = Message[i];
        U1TXREG = var;
        while (!U1STAbits.TRMT);
        i++;
    }
}

void MyPrintfVal(long val)
{
    sprintf(msg, "%ld", val);
    SendStringToMainPC(msg);
}

void MyPrintfMsg(char* mex)
{ 
    sprintf(msg, mex);
    SendStringToMainPC(msg);
}

void MyPrintfFwVersion(const char* fw)
{   
    char tmp[2];
    
    sprintf(msg, "Firmware version: ");
    tmp[0] = *fw + '0';
    tmp[1] = *(fw+1) + '0';
    strncat(msg, tmp, 2);
    strncat(msg, ".", 1);
    tmp[0] = *(fw+2) + '0';
    tmp[1] = *(fw+3) + '0';
    strncat(msg, tmp, 2);
    strncat(msg, "\n", 1);
    SendStringToMainPC(msg);          
}