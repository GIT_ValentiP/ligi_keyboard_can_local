/* 
 * File:   main.c
 * Author: area.rs3
 *
 * Created on 12 settembre 2019, 14.46
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <libpic30.h>
#include "board.h" 
#include "timer.h"
//#include "swtimer.h"
#include "canApp.h"
#include "uart.h"




extern const char firmware_version[];

int main(void)
{
    /************************************/
    /******Low level Initialization******/
    /************************************/
    InitBoard();
    MyPrintfMsg("Start...\n");
    MyPrintfFwVersion(firmware_version);
    MyPrintfMsg("Board Init...\n");
    
    /************************************/
    /*******Buttons Initialization*******/
    /************************************/
   // key_init(); 
    MyPrintfMsg("Buttons Init...\n");
    
    /************************************/
    /*******Application ADC init*********/
    /************************************/
  //  AppAdcInit();
    MyPrintfMsg("Adc Init...\n");
    
    /************************************/
    /********System software timer*******/
    /************************************/
    SystemTimerInit();
    MyPrintfMsg("Systimer Init...\n");
    
    /************************************/
    /********CAN system initialization***/
    /************************************/
    canAppInit();
    MyPrintfMsg("Can Init...\n");
    loadFwVersionOnCan(firmware_version);
    
    /************************************/
    /*************App manager Init*******/
    /************************************/
   // AppManagerInit();
    MyPrintfMsg("Application Init...\n");
    
    


    while(1)
    {
        if(true == Timer_IsExpired(&swTimer_1ms))
        {
            WATCHDOG = 0;        
            Timer_Rearm(&swTimer_1ms);
        }
        
        if(true == Timer_IsExpired(&swTimer_10ms))
        {
           //Manage_Tasti();
          // StartOfConvertion();         
           Timer_Rearm(&swTimer_10ms);
        }
        
        if(true == Timer_IsExpired(&swTimer_100ms))
        {
            WATCHDOG = 1;
            DEBUG_LED ^= 1;  
            Timer_Rearm(&swTimer_100ms);
        }

        if(true == Timer_IsExpired(&swTimer_500ms))
        {  
            Timer_Rearm(&swTimer_500ms);
        }
       
       // AppManager();
       // ManageAdc();    
       canAppTxManager();
    }
   
    return 0;
}



            
