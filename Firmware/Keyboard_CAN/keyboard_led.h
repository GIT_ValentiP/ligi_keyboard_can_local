/* 
 * File:   keyboard_led.h
 * Author: VALENTI
 *
 * Created on March 28, 2020, 1:03 PM
 */

#ifndef KEYBOARD_LED_H
#define	KEYBOARD_LED_H

#ifdef	__cplusplus
extern "C" {
#endif

void SetLeds(void);
void SetLedsDebug(void);
void LedsStartDebug(void);
void KeyboardScanRead(void);


void APP_KeyboardInit(void);
void APP_KeyboardTasks(void);


#ifdef	__cplusplus
}
#endif

#endif	/* KEYBOARD_LED_H */

