/* 
 * File:   globals.h
 * Author: firmware
 *
 * Created on 27 March 2020, 13.50
 */
#include <xc.h>
//#include "mcc_generated_files/pin_manager.h"
#include <stdint.h>
#include <stdbool.h>

#ifndef GLOBALS_H
#define	GLOBALS_H

#ifdef	__cplusplus
extern "C" {
#endif

/********* DEFINE ********/
#define FW_VERSION_HIGH  1  //max 15    
#define FW_VERSION_LOW   3  //max 15
    
#define KEYBOARD_LEFT       0
#define KEYBOARD_RIGHT      1    
#define NUM_ROW_MAX         2
#define NUM_COL_MAX        14
#define NUM_LED_MAX        14
#define NUM_LED_APP_MAX    10    

#define OUTPUT_PIN_PORT     0
#define INPUT_PIN_PORT      1    
#define SOLID_LED_ON       15
#define SOLID_LED_OFF       0 

#define LED_STATUS_ON        1
#define LED_STATUS_OFF       0     
    
#define KEY_PRESSED_VALUE          0    
#define KEY_PRESSED_DEBOUNCE_MS   10    
#define KEY_DEB_10MS_SHORT         5    //T0=  50ms  
#define KEY_DEB_10MS_MIDDLE      100  //T1=1000ms  
#define KEY_DEB_10MS_LONG        300  //T2=3000ms      
#define KEY_DEB_10MS_RELEASE       1  //20ms   
    
#define MAX_TASTI_LEFT 14
#define MAX_TASTI_RIGHT 14  
#define NUM_LEDS_RIGHT 14
#define NUM_LEDS_LEFT  14    
#define KEYBOARD_TIMEOUT_IDLE 50000 //in milliseconds    
    
/*CAN MESSASE ID*/
#define CAN_MSG_ID_STATUS     0x13F50107  //Status Message - 0x13F50107   Status Byte1   Keys from Byte2 to Byte8 
#define CAN_MSG_ID_INFO       0x1FF40107  //Info Message   - 0x1FF40107   FW version Byte 1 
#define CAN_MSG_ID_CMD_LED    0x17F30701  //KEYPAD Command Message - 0x17F30701   Leds Command from Byte1 to Byte7
    
//#define CAN_MSG_ID_CMD2       0x13Fxxxxx  //Status Message - 0x13F50107    
//#define CAN_MSG_ID_CMD3       0x13Fxxxxx  //Status Message - 0x13F50107    
//#define CAN_MSG_ID_CMD4       0x13Fxxxxx  //Status Message - 0x13F50107    
 
    
/*UART1 */ 
#define     FCY_APP           60000000
#define     BAUDRATE_UART1    115200
    
#define     BRGVAL_UART1      (unsigned int)(((unsigned long)FCY_APP/(unsigned long)BAUDRATE_UART1)/4)-1    

/*PWM*/    
#define FREQ_SYSTEM 120000000   
#define PWM3_FREQ   10000  
#define PWM_PRESCALER 16    
#define PWM3_PERIOD   (unsigned int)(((unsigned long)FREQ_SYSTEM/(unsigned long)PWM3_FREQ)/PWM_PRESCALER)  
#define PWM3L_DUTY_INIT (unsigned int)(PWM3_PERIOD/2) //(unsigned int)(((unsigned long)(PWM3_PERIOD*100))/100) //100%
#define PWM3H_DUTY_INIT 0    
    
/*KEYS BITMAP ORDER*/
#define BTN_ID_A 0//push button pressed for at least T2 time
#define BTN_ID_H 1//push button pressed for at least T2 time
#define BTN_ID_G 2//push button pressed for at least T2 time
#define BTN_ID_I 3//push button pressed for at least T2 time
#define BTN_ID_N 4//push button pressed for at least T2 time
#define BTN_ID_O 5//push button pressed for at least T2 time
#define BTN_ID_M 6//push button pressed for at least T2 time
#define BTN_ID_C 7//push button pressed for at least T2 time
#define BTN_ID_L 8//push button pressed for at least T2 time
#define BTN_ID_D 9//push button pressed for at least T2 time
#define BTN_ID_K 10//push button pressed for at least T2 time
#define BTN_ID_E 11//push button pressed for at least T2 time
#define BTN_ID_J 12//push button pressed for at least T2 time
#define BTN_ID_F 13//push button pressed for at least T2 time
    
    
/*******  VARIABLES KEYBOARD AND MOUSE MANAGEMENT ********/  


/******* VARIABLES KEYBOARD ********/    
typedef union
{
        struct{
            uint16_t low   :  8;
            uint16_t high   :  8;
        }byt8;
        uint16_t wrd16; 
}word_bit_t;

typedef struct
{
    //uint8_t pin;
    //uint8_t port;
    uint16_t val[NUM_ROW_MAX];
    word_bit_t val_short[NUM_ROW_MAX];//uint16_t
    word_bit_t val_middle[NUM_ROW_MAX];// uint16_t
    word_bit_t val_long[NUM_ROW_MAX];//uint16_t
    word_bit_t val_free[NUM_ROW_MAX];// uint16_t 
    uint16_t old[NUM_ROW_MAX];
    uint16_t deb_ms[NUM_ROW_MAX][NUM_COL_MAX];
    uint16_t deb_rel_ms[NUM_ROW_MAX][NUM_COL_MAX];
    //uint8_t chr;
} keys_keyboard_t;

 typedef struct
 {   
        uint8_t  status             :1;
        uint8_t  dimming_100ms      :4;
        uint8_t  new                :1;
        uint8_t  free               :2;
        uint16_t timing_on_200ms;
        uint16_t timing_off_200ms;
 }keys_leds_t;
 
typedef enum {
    KEYPAD_INIT   =0x00, // Inizialization
    KEYPAD_CHK_PWR=0x01,  // Check Power.
    KEYPAD_READY  =0x05,   // Ready
    KEYPAD_ERROR  =0x06,   // General Error
    KEYPAD_MAX    =0xFF,
} status_keypad_t;
 

typedef enum {
    BIN     =0x00, // 
    DECIMAL =0x01,  // 
    ESA     =0x03,   // 
    OCTA    =0x04,   //  
} print_format_t;

 typedef struct
 {   
        uint8_t  version          :4;
        uint8_t  subversion       :4;
     
 }fw_version_t;
typedef struct
{
    uint16_t ms;
    uint16_t ms10;        
    uint16_t ms100;
    uint8_t  sec;
    uint8_t  min;
    uint32_t hour;
    bool flag_10ms;
    bool flag_100ms;
    bool flag_1sec;
    bool flag_1min;
    bool flag_5sec;
    bool flag_10sec;
} system_time_struct_t;

/*CAN Bus Messages */
#define SIZE_OF_CAN_BUFF   16
#define RX_BUFF_DIM 10
#define TX_BUFF_DIM 4


typedef enum
{
    nullMsg     = 0x0,
    SBCUstat    = 0x3F5,
    SBCUerr     = 0x3F8,
    SBCUinfo    = 0x3F4,
    SBCUcmd     = 0x3F3,
    lastMsg     = 0x3FF               
}msgid_t;

typedef struct
{
    uint8_t status;
    uint8_t keys_high;
    uint8_t keys_low;
    uint8_t keysT1_high;
    uint8_t keysT1_low;
    uint8_t keysT2_high;
    uint8_t keysT2_low;
    uint8_t unusued;
}sbcuStat_t;

typedef struct
{
    struct
    {
        uint8_t minor :4;
        uint8_t major :4;   
    }vers;
    uint8_t unusued[7];
}sbcuInfo_t;

typedef struct
{
    struct
    {
        uint8_t err_1          :  1; //LSB
        uint8_t err_2          :  1; 
        uint8_t err_3          :  1; 
        uint8_t err_4          :  1;
        uint8_t err_5          :  1;
        uint8_t err_6          :  1;
        uint8_t err_7          :  1; 
        uint8_t err_8          :  1;
    }byte1;
    
    uint8_t unusued0;
    uint8_t unusued1;
    uint8_t unusued2;
    uint8_t unusued3;
    uint8_t unusued4;
    uint8_t unusued5;

    struct
    {
        uint8_t wrn_1          :  1; //LSB
        uint8_t wrn_2          :  1; 
        uint8_t wrn_3          :  1;  
        uint8_t padding        :  5;
    }byte8;
}sbcuErr_t;


typedef struct
{
   
    uint8_t LED02_status       :4;//LSB
    uint8_t LED01_status       :4;
    uint8_t LED04_status       :4;//LSB
    uint8_t LED03_status       :4;
    uint8_t LED06_status       :4;//LSB
    uint8_t LED05_status       :4;
    uint8_t LED08_status       :4;//LSB
    uint8_t LED07_status       :4;
    uint8_t LED10_status       :4;//LSB
    uint8_t LED09_status       :4;
    uint8_t unusued0;
    uint8_t unusued1;
    uint8_t PWM_Led_duty;//unusued2;
}sbcuCmd_t;


typedef union 
{
    sbcuStat_t          Stat;
    sbcuInfo_t          Info;
    sbcuErr_t           Err;
    sbcuCmd_t           Cmd;
    uint8_t             byte[8];
}canMsg_t;


typedef enum
{
    highest     = 0,
    veryHigh    = 1,
    high        = 2,
    mediumHigh  = 3,
    medium      = 4,
    mediumLow   = 5,
    low         = 6,               
    lower       = 7,        
}priority_t;


typedef enum
{
    BroadcastMsg        = 0,
    MainPcBoard         = 1,
    RotationalPcBoard   = 2,
    ControlBoard        = 3,
    LightAndGasBoard    = 4,
    MotorBoard          = 5,   
    SurgicalBedBoard    = 6,
    KeypadBoard         = 7,        
}boards_t;

typedef struct 
{
    priority_t  priority; 
    msgid_t     msgId;
    boards_t    recipient;
    uint16_t    period;
    canMsg_t    *msg;
}CanTxObjectDescriptor_t;

typedef struct 
{
    priority_t  priority; 
    msgid_t     msgId;
    boards_t    recipient;
    canMsg_t    *msg;
}CanRxObjectDescriptor_t;

typedef union
{
    struct{
        struct{
            uint16_t ide     :  1;
            uint16_t srr     :  1;
            uint16_t sid     :  11;
            uint16_t unusued :  3;
        }stdid;

        struct{
            uint16_t extidh  :  12;
            uint16_t unusued :  4;
        }extid;

        struct{
            uint16_t dlc     :  4;
            uint16_t rb0     :  1;
            uint16_t unusued :  3;
            uint16_t rb1     :  1;
            uint16_t rtr     :  1;
            uint16_t extidl  :  6;        
        }ctrl;

        struct{
            uint16_t byte0   :  8;
            uint16_t byte1   :  8;
        }wrd01;

        struct{
            uint16_t byte2   :  8;
            uint16_t byte3   :  8;
        }wrd23;

        struct{
            uint16_t byte4   :  8;
            uint16_t byte5   :  8;
        }wrd45;

        struct{
            uint16_t byte6   :  8;
            uint16_t byte7   :  8;
        }wrd67;

        struct{
            uint16_t unusued :  8;
            uint16_t filhit  :  5;
            uint16_t padding :  3;    
        }filt;  
    }msg;
    uint16_t    wordBuff[8];
    uint8_t     byteBuff[16];
}can_msg_buff_t;


#ifdef	__cplusplus
}
#endif
//keys_keyboard_t kb_status[NUM_ROW_MAX][NUM_COL_MAX];   
//uint8_t num_keys_pressed;   
//uint8_t keys_multi[MAX_TASTI_LEFT];
//uint8_t keypad_multi[MAX_TASTI_LEFT];
//keys_leds_t keys_special;
//uint8_t id_kp;
 /******* VARIABLES SYSTEM TIMER********/    
//uint16_t duty_pwm_backlight;
//uint16_t keyboardIdleRateMillisec;
//system_time_struct_t main_clock;
 
#endif	/* GLOBALS_H */


