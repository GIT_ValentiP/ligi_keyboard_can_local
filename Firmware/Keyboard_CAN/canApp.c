/*
 * File:   can.c
 * Author: area.rs19
 *
 * Created on 12 luglio 2019, 16.02
 */


// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include "canApp.h"
#include "globals.h"
#include "mcc_generated_files/uart1.h"

/*EXTERN Variables*/
extern keys_keyboard_t kb_keys;
extern keys_leds_t kb_led[NUM_COL_MAX];
extern status_keypad_t kb_status;
extern fw_version_t fw_ver;

/*LOCAL Variables*/
canMsg_t dummy;
canMsg_t sbcuStat;
canMsg_t sbcuInfo;
canMsg_t sbcuErr;
canMsg_t sbcuCmd;


uint16_t canTmr[4];


const boards_t  MyBoard = KeypadBoard;
const CanTxObjectDescriptor_t CanTxObjectDescriptor[TX_BUFF_DIM]=
{
/*n*/   //priority  msgId       recipient       period[ms]  &msgptr
/*0*/   {medium,    SBCUstat,   MainPcBoard,    50,         &sbcuStat},
/*1*/   {lower,     SBCUinfo,   MainPcBoard,    5000,       &sbcuInfo},
/*2*/   {lower,     nullMsg,    MainPcBoard,    100,        &dummy   },//{medium,    SBCUerr,    MainPcBoard,    500,        &sbcuErr },
/*3*/   {lower,     nullMsg,    MainPcBoard,    100,        &dummy   },
};

const CanRxObjectDescriptor_t CanRxObjectDescriptor[RX_BUFF_DIM]=
{
//space reserved for "MyBoard" recipient  messages
/*n*/  //priority  //msgId      recipient          &msgptr
/*0*/  {mediumLow, SBCUcmd,    KeypadBoard,        &sbcuCmd},
/*1*/  {lower,     nullMsg,    KeypadBoard,        &dummy},
/*2*/  {lower,     nullMsg,    KeypadBoard,        &dummy},
/*3*/  {lower,     nullMsg,    KeypadBoard,        &dummy},
/*4*/  {lower,     nullMsg,    KeypadBoard,        &dummy},

 
//space reserved for "Broadcast" messages
/*5*/  {lower,     nullMsg,    BroadcastMsg,       &dummy},
/*6*/  {lower,     nullMsg,    BroadcastMsg,       &dummy},
/*7*/  {lower,     nullMsg,    BroadcastMsg,       &dummy},
/*8*/  {lower,     nullMsg,    BroadcastMsg,       &dummy},
/*9*/  {lower,     nullMsg,    BroadcastMsg,       &dummy},

};




void canAppInit(void)
{
    uint8_t i;
    uint32_t id =0;
    uint32_t rcv_id[10];
    
    for(i=0; i<4; i++)
    {
        id =  ( (((uint32_t)(CanTxObjectDescriptor[i].priority))<<26) |\
                (((uint32_t)(CanTxObjectDescriptor[i].msgId))<<16)|\
                (((uint32_t)(CanTxObjectDescriptor[i].recipient))<<8)|\
                ((uint32_t)(MyBoard)));
        
        can_msg_buff[i].msg.stdid.ide       = 1;
        can_msg_buff[i].msg.stdid.srr       = 0;
        can_msg_buff[i].msg.stdid.sid       = ((id>>18)&0x000007ff);
        can_msg_buff[i].msg.extid.extidh    = ((id>>6)&0x00000fff);
        can_msg_buff[i].msg.ctrl.extidl     = ((id)&0x0000003f);
        can_msg_buff[i].msg.ctrl.rb1        = 0;
        can_msg_buff[i].msg.ctrl.rb0        = 0;
        can_msg_buff[i].msg.ctrl.rtr        = 0;
        can_msg_buff[i].msg.ctrl.dlc        = 8;
        
        can_msg_buff[i].msg.wrd01.byte0     = *(CanTxObjectDescriptor[i].msg->byte);
        can_msg_buff[i].msg.wrd01.byte1     = *(CanTxObjectDescriptor[i].msg->byte+1);
        can_msg_buff[i].msg.wrd23.byte2     = *(CanTxObjectDescriptor[i].msg->byte+2);
        can_msg_buff[i].msg.wrd23.byte3     = *(CanTxObjectDescriptor[i].msg->byte+3);
        can_msg_buff[i].msg.wrd45.byte4     = *(CanTxObjectDescriptor[i].msg->byte+4);
        can_msg_buff[i].msg.wrd45.byte5     = *(CanTxObjectDescriptor[i].msg->byte+5);
        can_msg_buff[i].msg.wrd67.byte6     = *(CanTxObjectDescriptor[i].msg->byte+6);
        can_msg_buff[i].msg.wrd67.byte7     = *(CanTxObjectDescriptor[i].msg->byte+7);
        canTmr[i]=CanTxObjectDescriptor[i].period;
    }
    
    if(CanTxObjectDescriptor[0].msgId != nullMsg)
    {
        TX_REQ_BUFF_0 = 1;
    }
    
    if(CanTxObjectDescriptor[1].msgId != nullMsg)
    {
        TX_REQ_BUFF_1 = 1;
    }
    
    if(CanTxObjectDescriptor[2].msgId != nullMsg)
    {
        TX_REQ_BUFF_2 = 1;
    }
    
    if(CanTxObjectDescriptor[3].msgId != nullMsg)
    {
        TX_REQ_BUFF_3 = 1;
    } 
    
    for(i=0; i<10; i++)
    {
        rcv_id[i] = 0;
        rcv_id[i] =( (((uint32_t)(CanRxObjectDescriptor[i].priority))<<26) |\
                     (((uint32_t)(CanRxObjectDescriptor[i].msgId))<<16)|\
                     (((uint32_t)(CanRxObjectDescriptor[i].recipient))<<8)|\
                      ((uint32_t)(MainPcBoard)));
      //  rcv_id[i] =  ((((uint32_t)(CanRxObjectDescriptor[i].msgId))<<16)|(((uint32_t)(CanRxObjectDescriptor[i].recipient))<<8));
   
    }
    
    canDrvSetFilters(rcv_id);
    
}


void canAppTxManager(void)
{
    if(canTmr[0]==0)
    {
        can_msg_buff[0].msg.wrd01.byte0     = *(CanTxObjectDescriptor[0].msg->byte);
        can_msg_buff[0].msg.wrd01.byte1     = *(CanTxObjectDescriptor[0].msg->byte+1);
        can_msg_buff[0].msg.wrd23.byte2     = *(CanTxObjectDescriptor[0].msg->byte+2);
        can_msg_buff[0].msg.wrd23.byte3     = *(CanTxObjectDescriptor[0].msg->byte+3);
        can_msg_buff[0].msg.wrd45.byte4     = *(CanTxObjectDescriptor[0].msg->byte+4);
        can_msg_buff[0].msg.wrd45.byte5     = *(CanTxObjectDescriptor[0].msg->byte+5);
        can_msg_buff[0].msg.wrd67.byte6     = *(CanTxObjectDescriptor[0].msg->byte+6);
        can_msg_buff[0].msg.wrd67.byte7     = *(CanTxObjectDescriptor[0].msg->byte+7);
        
        if(CanTxObjectDescriptor[0].msgId != nullMsg)
        {
            TX_REQ_BUFF_0 = 1;         
        }
        canTmr[0]=CanTxObjectDescriptor[0].period; 
        //MyPrintfMsg("Tx CAN:STATUS Keys\n");
   
    }
 
    if(canTmr[1]==0)
    {
        //loadFwVersionOnCan(fw_ver);
        can_msg_buff[1].msg.wrd01.byte0     = *(CanTxObjectDescriptor[1].msg->byte);
        can_msg_buff[1].msg.wrd01.byte1     = *(CanTxObjectDescriptor[1].msg->byte+1);
        can_msg_buff[1].msg.wrd23.byte2     = *(CanTxObjectDescriptor[1].msg->byte+2);
        can_msg_buff[1].msg.wrd23.byte3     = *(CanTxObjectDescriptor[1].msg->byte+3);
        can_msg_buff[1].msg.wrd45.byte4     = *(CanTxObjectDescriptor[1].msg->byte+4);
        can_msg_buff[1].msg.wrd45.byte5     = *(CanTxObjectDescriptor[1].msg->byte+5);
        can_msg_buff[1].msg.wrd67.byte6     = *(CanTxObjectDescriptor[1].msg->byte+6);
        can_msg_buff[1].msg.wrd67.byte7     = *(CanTxObjectDescriptor[1].msg->byte+7);
        if(CanTxObjectDescriptor[1].msgId != nullMsg)
        {
            TX_REQ_BUFF_1 = 1; 
            MyPrintfMsg("Tx CAN:INFO Fw\n");
            //MyPrintfVal(can_msg_buff[1].msg.wrd01.byte0 );
        } 
        canTmr[1]=CanTxObjectDescriptor[1].period; 
       
    }
    
//    if(canTmr[2]==0)
//    {
//        can_msg_buff[2].msg.wrd01.byte0     = *(CanTxObjectDescriptor[2].msg->byte);
//        can_msg_buff[2].msg.wrd01.byte1     = *(CanTxObjectDescriptor[2].msg->byte+1);
//        can_msg_buff[2].msg.wrd23.byte2     = *(CanTxObjectDescriptor[2].msg->byte+2);
//        can_msg_buff[2].msg.wrd23.byte3     = *(CanTxObjectDescriptor[2].msg->byte+3);
//        can_msg_buff[2].msg.wrd45.byte4     = *(CanTxObjectDescriptor[2].msg->byte+4);
//        can_msg_buff[2].msg.wrd45.byte5     = *(CanTxObjectDescriptor[2].msg->byte+5);
//        can_msg_buff[2].msg.wrd67.byte6     = *(CanTxObjectDescriptor[2].msg->byte+6);
//        can_msg_buff[2].msg.wrd67.byte7     = *(CanTxObjectDescriptor[2].msg->byte+7);
//        if(CanTxObjectDescriptor[2].msgId != nullMsg)
//        {
//            TX_REQ_BUFF_2 = 1;
//        }
//        canTmr[2]=CanTxObjectDescriptor[2].period; 
//    }
//    
//    if(canTmr[3]==0)
//    {
//        can_msg_buff[3].msg.wrd01.byte0     = *(CanTxObjectDescriptor[3].msg->byte);
//        can_msg_buff[3].msg.wrd01.byte1     = *(CanTxObjectDescriptor[3].msg->byte+1);
//        can_msg_buff[3].msg.wrd23.byte2     = *(CanTxObjectDescriptor[3].msg->byte+2);
//        can_msg_buff[3].msg.wrd23.byte3     = *(CanTxObjectDescriptor[3].msg->byte+3);
//        can_msg_buff[3].msg.wrd45.byte4     = *(CanTxObjectDescriptor[3].msg->byte+4);
//        can_msg_buff[3].msg.wrd45.byte5     = *(CanTxObjectDescriptor[3].msg->byte+5);
//        can_msg_buff[3].msg.wrd67.byte6     = *(CanTxObjectDescriptor[3].msg->byte+6);
//        can_msg_buff[3].msg.wrd67.byte7     = *(CanTxObjectDescriptor[3].msg->byte+7);
//        if(CanTxObjectDescriptor[3].msgId != nullMsg)
//        {
//            TX_REQ_BUFF_3 = 1;
//        }
//        canTmr[3]=CanTxObjectDescriptor[3].period; 
//    }   
}



void loadFwVersionOnCan(fw_version_t fw)
{
   
    sbcuInfo.Info.vers.major = fw.version;
    
    sbcuInfo.Info.vers.minor = fw.subversion;
}


