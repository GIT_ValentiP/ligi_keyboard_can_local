/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  dsPIC33EP256GM710
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB 	          :  MPLAB X v5.30
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include <xc.h>
#include <libpic30.h>
#include "globals.h"
#include "canApp.h"
#include "keyboard_led.h"
#include "mcc_generated_files/system.h"
#include "mcc_generated_files/uart1.h"
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/watchdog.h"

extern uint16_t keyboardIdleRateMillisec;
extern system_time_struct_t main_clock;
extern status_keypad_t kb_status;

fw_version_t fw_ver;
/*
                         Main application
 */
int main(void)
{
    __C30_UART=1;//Standard output on UART1 for printf()
    
    //Disable Watchdog 
    WATCHDOG_TimerSoftwareEnable();//WATCHDOG_TimerSoftwareDisable();
    WATCHDOG_TimerClear();//Reset Watchdog Timer
    
    // initialize the device
    SYSTEM_Initialize();
    WATCHDOG_TimerClear();//Reset Watchdog Timer
    MyPrintfMsg("Start...\n");
    MyPrintfMsg("Board Init...\n");
    /************************************/
    /*******Buttons Initialization*******/
    /************************************/
    APP_KeyboardInit();
    MyPrintfMsg("Keyboard Init...\n");  
    /************************************/
    /********System software timer*******/
    /************************************/
    SystemTimerInit();
    MyPrintfMsg("Systimer Init...\n");
  
    fw_ver.subversion =FW_VERSION_LOW;
    fw_ver.version    =FW_VERSION_HIGH;
    kb_status=KEYPAD_INIT;
    WATCHDOG_TimerClear();//Reset Watchdog Timer 
    /************************************/
    /********CAN system initialization***/
    /************************************/
    canAppInit();
    MyPrintfMsg("Can Init...\n");
    loadFwVersionOnCan(fw_ver);
    MyPrintfFwVersion();
    WATCHDOG_TimerClear();//Reset Watchdog Timer
      /************************************/
    /*************App manager Init*******/
    /************************************/
    MyPrintfMsg("Application Init...\n");
    LedsStartDebug();
    SystemTimerInit();
    WATCHDOG_TimerClear();//Reset Watchdog Timer
    while (1)
    {
        APP_KeyboardTasks();
        WATCHDOG_TimerClear();//Reset Watchdog Timer 
        canAppTxManager();
        WATCHDOG_TimerClear();//Reset Watchdog Timer 
                          
//        if (main_clock.flag_10ms==1){   
//             KeyboardScanRead();    
//             main_clock.flag_10ms=0;
//        }
//        if (main_clock.flag_100ms==1){   
//            SetLedsDebug();  
//            main_clock.flag_100ms=0;
//        }
//        if (main_clock.flag_1sec==1){
//            RED_LED_SERVICE_Toggle();  
//            MyPrintfMsg("Elapsed 1 seconds");
//            main_clock.flag_1sec=0;
//        }
//        if (main_clock.flag_5sec==1){         
//            main_clock.flag_5sec=0;
//        }
//          if (main_clock.flag_10sec==1){         
//            main_clock.flag_10sec=0;
//          }
//          if (main_clock.flag_1min==1){    
//            main_clock.flag_1min=0;
//          }
    }
    return 1; 
}
/**
 End of File
*/

