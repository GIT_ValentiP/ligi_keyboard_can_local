/**
  PIN MANAGER Generated Driver File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the generated manager file for the PIC24 / dsPIC33 / PIC32MM MCUs device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description:
    This source file provides implementations for PIN MANAGER.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.166.1
        Device            :  dsPIC33EP256GM710
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.41
        MPLAB 	          :  MPLAB X v5.30
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#ifndef _PIN_MANAGER_H
#define _PIN_MANAGER_H
/**
    Section: Includes
*/
#include <xc.h>

/**
    Section: Device Pin Macros
*/
/**
  @Summary
    Sets the GPIO pin, RA0, high using LATA0.

  @Description
    Sets the GPIO pin, RA0, high using LATA0.

  @Preconditions
    The RA0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA0 high (1)
    LED21_OUTB07_SetHigh();
    </code>

*/
#define LED21_OUTB07_SetHigh()          (_LATA0 = 1)
/**
  @Summary
    Sets the GPIO pin, RA0, low using LATA0.

  @Description
    Sets the GPIO pin, RA0, low using LATA0.

  @Preconditions
    The RA0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA0 low (0)
    LED21_OUTB07_SetLow();
    </code>

*/
#define LED21_OUTB07_SetLow()           (_LATA0 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA0, using LATA0.

  @Description
    Toggles the GPIO pin, RA0, using LATA0.

  @Preconditions
    The RA0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA0
    LED21_OUTB07_Toggle();
    </code>

*/
#define LED21_OUTB07_Toggle()           (_LATA0 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA0.

  @Description
    Reads the value of the GPIO pin, RA0.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA0
    postValue = LED21_OUTB07_GetValue();
    </code>

*/
#define LED21_OUTB07_GetValue()         _RA0
/**
  @Summary
    Configures the GPIO pin, RA0, as an input.

  @Description
    Configures the GPIO pin, RA0, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA0 as an input
    LED21_OUTB07_SetDigitalInput();
    </code>

*/
#define LED21_OUTB07_SetDigitalInput()  (_TRISA0 = 1)
/**
  @Summary
    Configures the GPIO pin, RA0, as an output.

  @Description
    Configures the GPIO pin, RA0, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA0 as an output
    LED21_OUTB07_SetDigitalOutput();
    </code>

*/
#define LED21_OUTB07_SetDigitalOutput() (_TRISA0 = 0)
/**
  @Summary
    Sets the GPIO pin, RA1, high using LATA1.

  @Description
    Sets the GPIO pin, RA1, high using LATA1.

  @Preconditions
    The RA1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA1 high (1)
    LED22_OUTB08_SetHigh();
    </code>

*/
#define LED22_OUTB08_SetHigh()          (_LATA1 = 1)
/**
  @Summary
    Sets the GPIO pin, RA1, low using LATA1.

  @Description
    Sets the GPIO pin, RA1, low using LATA1.

  @Preconditions
    The RA1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA1 low (0)
    LED22_OUTB08_SetLow();
    </code>

*/
#define LED22_OUTB08_SetLow()           (_LATA1 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA1, using LATA1.

  @Description
    Toggles the GPIO pin, RA1, using LATA1.

  @Preconditions
    The RA1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA1
    LED22_OUTB08_Toggle();
    </code>

*/
#define LED22_OUTB08_Toggle()           (_LATA1 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA1.

  @Description
    Reads the value of the GPIO pin, RA1.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA1
    postValue = LED22_OUTB08_GetValue();
    </code>

*/
#define LED22_OUTB08_GetValue()         _RA1
/**
  @Summary
    Configures the GPIO pin, RA1, as an input.

  @Description
    Configures the GPIO pin, RA1, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA1 as an input
    LED22_OUTB08_SetDigitalInput();
    </code>

*/
#define LED22_OUTB08_SetDigitalInput()  (_TRISA1 = 1)
/**
  @Summary
    Configures the GPIO pin, RA1, as an output.

  @Description
    Configures the GPIO pin, RA1, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA1 as an output
    LED22_OUTB08_SetDigitalOutput();
    </code>

*/
#define LED22_OUTB08_SetDigitalOutput() (_TRISA1 = 0)
/**
  @Summary
    Sets the GPIO pin, RA10, high using LATA10.

  @Description
    Sets the GPIO pin, RA10, high using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA10 high (1)
    LED03_OUT03_SetHigh();
    </code>

*/
#define LED03_OUT03_SetHigh()          (_LATA10 = 1)
/**
  @Summary
    Sets the GPIO pin, RA10, low using LATA10.

  @Description
    Sets the GPIO pin, RA10, low using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA10 low (0)
    LED03_OUT03_SetLow();
    </code>

*/
#define LED03_OUT03_SetLow()           (_LATA10 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA10, using LATA10.

  @Description
    Toggles the GPIO pin, RA10, using LATA10.

  @Preconditions
    The RA10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA10
    LED03_OUT03_Toggle();
    </code>

*/
#define LED03_OUT03_Toggle()           (_LATA10 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA10.

  @Description
    Reads the value of the GPIO pin, RA10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA10
    postValue = LED03_OUT03_GetValue();
    </code>

*/
#define LED03_OUT03_GetValue()         _RA10
/**
  @Summary
    Configures the GPIO pin, RA10, as an input.

  @Description
    Configures the GPIO pin, RA10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA10 as an input
    LED03_OUT03_SetDigitalInput();
    </code>

*/
#define LED03_OUT03_SetDigitalInput()  (_TRISA10 = 1)
/**
  @Summary
    Configures the GPIO pin, RA10, as an output.

  @Description
    Configures the GPIO pin, RA10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA10 as an output
    LED03_OUT03_SetDigitalOutput();
    </code>

*/
#define LED03_OUT03_SetDigitalOutput() (_TRISA10 = 0)
/**
  @Summary
    Sets the GPIO pin, RA11, high using LATA11.

  @Description
    Sets the GPIO pin, RA11, high using LATA11.

  @Preconditions
    The RA11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA11 high (1)
    LED20_OUTB06_SetHigh();
    </code>

*/
#define LED20_OUTB06_SetHigh()          (_LATA11 = 1)
/**
  @Summary
    Sets the GPIO pin, RA11, low using LATA11.

  @Description
    Sets the GPIO pin, RA11, low using LATA11.

  @Preconditions
    The RA11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA11 low (0)
    LED20_OUTB06_SetLow();
    </code>

*/
#define LED20_OUTB06_SetLow()           (_LATA11 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA11, using LATA11.

  @Description
    Toggles the GPIO pin, RA11, using LATA11.

  @Preconditions
    The RA11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA11
    LED20_OUTB06_Toggle();
    </code>

*/
#define LED20_OUTB06_Toggle()           (_LATA11 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA11.

  @Description
    Reads the value of the GPIO pin, RA11.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA11
    postValue = LED20_OUTB06_GetValue();
    </code>

*/
#define LED20_OUTB06_GetValue()         _RA11
/**
  @Summary
    Configures the GPIO pin, RA11, as an input.

  @Description
    Configures the GPIO pin, RA11, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA11 as an input
    LED20_OUTB06_SetDigitalInput();
    </code>

*/
#define LED20_OUTB06_SetDigitalInput()  (_TRISA11 = 1)
/**
  @Summary
    Configures the GPIO pin, RA11, as an output.

  @Description
    Configures the GPIO pin, RA11, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA11 as an output
    LED20_OUTB06_SetDigitalOutput();
    </code>

*/
#define LED20_OUTB06_SetDigitalOutput() (_TRISA11 = 0)
/**
  @Summary
    Sets the GPIO pin, RA12, high using LATA12.

  @Description
    Sets the GPIO pin, RA12, high using LATA12.

  @Preconditions
    The RA12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA12 high (1)
    LED19_OUTB05_SetHigh();
    </code>

*/
#define LED19_OUTB05_SetHigh()          (_LATA12 = 1)
/**
  @Summary
    Sets the GPIO pin, RA12, low using LATA12.

  @Description
    Sets the GPIO pin, RA12, low using LATA12.

  @Preconditions
    The RA12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA12 low (0)
    LED19_OUTB05_SetLow();
    </code>

*/
#define LED19_OUTB05_SetLow()           (_LATA12 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA12, using LATA12.

  @Description
    Toggles the GPIO pin, RA12, using LATA12.

  @Preconditions
    The RA12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA12
    LED19_OUTB05_Toggle();
    </code>

*/
#define LED19_OUTB05_Toggle()           (_LATA12 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA12.

  @Description
    Reads the value of the GPIO pin, RA12.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA12
    postValue = LED19_OUTB05_GetValue();
    </code>

*/
#define LED19_OUTB05_GetValue()         _RA12
/**
  @Summary
    Configures the GPIO pin, RA12, as an input.

  @Description
    Configures the GPIO pin, RA12, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA12 as an input
    LED19_OUTB05_SetDigitalInput();
    </code>

*/
#define LED19_OUTB05_SetDigitalInput()  (_TRISA12 = 1)
/**
  @Summary
    Configures the GPIO pin, RA12, as an output.

  @Description
    Configures the GPIO pin, RA12, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA12 as an output
    LED19_OUTB05_SetDigitalOutput();
    </code>

*/
#define LED19_OUTB05_SetDigitalOutput() (_TRISA12 = 0)
/**
  @Summary
    Sets the GPIO pin, RA14, high using LATA14.

  @Description
    Sets the GPIO pin, RA14, high using LATA14.

  @Preconditions
    The RA14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA14 high (1)
    SW20_INB06_SetHigh();
    </code>

*/
#define SW20_INB06_SetHigh()          (_LATA14 = 1)
/**
  @Summary
    Sets the GPIO pin, RA14, low using LATA14.

  @Description
    Sets the GPIO pin, RA14, low using LATA14.

  @Preconditions
    The RA14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA14 low (0)
    SW20_INB06_SetLow();
    </code>

*/
#define SW20_INB06_SetLow()           (_LATA14 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA14, using LATA14.

  @Description
    Toggles the GPIO pin, RA14, using LATA14.

  @Preconditions
    The RA14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA14
    SW20_INB06_Toggle();
    </code>

*/
#define SW20_INB06_Toggle()           (_LATA14 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA14.

  @Description
    Reads the value of the GPIO pin, RA14.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA14
    postValue = SW20_INB06_GetValue();
    </code>

*/
#define SW20_INB06_GetValue()         _RA14
/**
  @Summary
    Configures the GPIO pin, RA14, as an input.

  @Description
    Configures the GPIO pin, RA14, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA14 as an input
    SW20_INB06_SetDigitalInput();
    </code>

*/
#define SW20_INB06_SetDigitalInput()  (_TRISA14 = 1)
/**
  @Summary
    Configures the GPIO pin, RA14, as an output.

  @Description
    Configures the GPIO pin, RA14, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA14 as an output
    SW20_INB06_SetDigitalOutput();
    </code>

*/
#define SW20_INB06_SetDigitalOutput() (_TRISA14 = 0)
/**
  @Summary
    Sets the GPIO pin, RA15, high using LATA15.

  @Description
    Sets the GPIO pin, RA15, high using LATA15.

  @Preconditions
    The RA15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA15 high (1)
    SW21_INB07_SetHigh();
    </code>

*/
#define SW21_INB07_SetHigh()          (_LATA15 = 1)
/**
  @Summary
    Sets the GPIO pin, RA15, low using LATA15.

  @Description
    Sets the GPIO pin, RA15, low using LATA15.

  @Preconditions
    The RA15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA15 low (0)
    SW21_INB07_SetLow();
    </code>

*/
#define SW21_INB07_SetLow()           (_LATA15 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA15, using LATA15.

  @Description
    Toggles the GPIO pin, RA15, using LATA15.

  @Preconditions
    The RA15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA15
    SW21_INB07_Toggle();
    </code>

*/
#define SW21_INB07_Toggle()           (_LATA15 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA15.

  @Description
    Reads the value of the GPIO pin, RA15.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA15
    postValue = SW21_INB07_GetValue();
    </code>

*/
#define SW21_INB07_GetValue()         _RA15
/**
  @Summary
    Configures the GPIO pin, RA15, as an input.

  @Description
    Configures the GPIO pin, RA15, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA15 as an input
    SW21_INB07_SetDigitalInput();
    </code>

*/
#define SW21_INB07_SetDigitalInput()  (_TRISA15 = 1)
/**
  @Summary
    Configures the GPIO pin, RA15, as an output.

  @Description
    Configures the GPIO pin, RA15, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA15 as an output
    SW21_INB07_SetDigitalOutput();
    </code>

*/
#define SW21_INB07_SetDigitalOutput() (_TRISA15 = 0)
/**
  @Summary
    Sets the GPIO pin, RA4, high using LATA4.

  @Description
    Sets the GPIO pin, RA4, high using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA4 high (1)
    SW12_IN12_SetHigh();
    </code>

*/
#define SW12_IN12_SetHigh()          (_LATA4 = 1)
/**
  @Summary
    Sets the GPIO pin, RA4, low using LATA4.

  @Description
    Sets the GPIO pin, RA4, low using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA4 low (0)
    SW12_IN12_SetLow();
    </code>

*/
#define SW12_IN12_SetLow()           (_LATA4 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA4, using LATA4.

  @Description
    Toggles the GPIO pin, RA4, using LATA4.

  @Preconditions
    The RA4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA4
    SW12_IN12_Toggle();
    </code>

*/
#define SW12_IN12_Toggle()           (_LATA4 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA4.

  @Description
    Reads the value of the GPIO pin, RA4.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA4
    postValue = SW12_IN12_GetValue();
    </code>

*/
#define SW12_IN12_GetValue()         _RA4
/**
  @Summary
    Configures the GPIO pin, RA4, as an input.

  @Description
    Configures the GPIO pin, RA4, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA4 as an input
    SW12_IN12_SetDigitalInput();
    </code>

*/
#define SW12_IN12_SetDigitalInput()  (_TRISA4 = 1)
/**
  @Summary
    Configures the GPIO pin, RA4, as an output.

  @Description
    Configures the GPIO pin, RA4, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA4 as an output
    SW12_IN12_SetDigitalOutput();
    </code>

*/
#define SW12_IN12_SetDigitalOutput() (_TRISA4 = 0)
/**
  @Summary
    Sets the GPIO pin, RA7, high using LATA7.

  @Description
    Sets the GPIO pin, RA7, high using LATA7.

  @Preconditions
    The RA7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA7 high (1)
    LED05_OUT05_SetHigh();
    </code>

*/
#define LED05_OUT05_SetHigh()          (_LATA7 = 1)
/**
  @Summary
    Sets the GPIO pin, RA7, low using LATA7.

  @Description
    Sets the GPIO pin, RA7, low using LATA7.

  @Preconditions
    The RA7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA7 low (0)
    LED05_OUT05_SetLow();
    </code>

*/
#define LED05_OUT05_SetLow()           (_LATA7 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA7, using LATA7.

  @Description
    Toggles the GPIO pin, RA7, using LATA7.

  @Preconditions
    The RA7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA7
    LED05_OUT05_Toggle();
    </code>

*/
#define LED05_OUT05_Toggle()           (_LATA7 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA7.

  @Description
    Reads the value of the GPIO pin, RA7.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA7
    postValue = LED05_OUT05_GetValue();
    </code>

*/
#define LED05_OUT05_GetValue()         _RA7
/**
  @Summary
    Configures the GPIO pin, RA7, as an input.

  @Description
    Configures the GPIO pin, RA7, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA7 as an input
    LED05_OUT05_SetDigitalInput();
    </code>

*/
#define LED05_OUT05_SetDigitalInput()  (_TRISA7 = 1)
/**
  @Summary
    Configures the GPIO pin, RA7, as an output.

  @Description
    Configures the GPIO pin, RA7, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA7 as an output
    LED05_OUT05_SetDigitalOutput();
    </code>

*/
#define LED05_OUT05_SetDigitalOutput() (_TRISA7 = 0)
/**
  @Summary
    Sets the GPIO pin, RA9, high using LATA9.

  @Description
    Sets the GPIO pin, RA9, high using LATA9.

  @Preconditions
    The RA9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA9 high (1)
    SW14_IN14_SetHigh();
    </code>

*/
#define SW14_IN14_SetHigh()          (_LATA9 = 1)
/**
  @Summary
    Sets the GPIO pin, RA9, low using LATA9.

  @Description
    Sets the GPIO pin, RA9, low using LATA9.

  @Preconditions
    The RA9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RA9 low (0)
    SW14_IN14_SetLow();
    </code>

*/
#define SW14_IN14_SetLow()           (_LATA9 = 0)
/**
  @Summary
    Toggles the GPIO pin, RA9, using LATA9.

  @Description
    Toggles the GPIO pin, RA9, using LATA9.

  @Preconditions
    The RA9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RA9
    SW14_IN14_Toggle();
    </code>

*/
#define SW14_IN14_Toggle()           (_LATA9 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RA9.

  @Description
    Reads the value of the GPIO pin, RA9.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RA9
    postValue = SW14_IN14_GetValue();
    </code>

*/
#define SW14_IN14_GetValue()         _RA9
/**
  @Summary
    Configures the GPIO pin, RA9, as an input.

  @Description
    Configures the GPIO pin, RA9, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA9 as an input
    SW14_IN14_SetDigitalInput();
    </code>

*/
#define SW14_IN14_SetDigitalInput()  (_TRISA9 = 1)
/**
  @Summary
    Configures the GPIO pin, RA9, as an output.

  @Description
    Configures the GPIO pin, RA9, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RA9 as an output
    SW14_IN14_SetDigitalOutput();
    </code>

*/
#define SW14_IN14_SetDigitalOutput() (_TRISA9 = 0)
/**
  @Summary
    Sets the GPIO pin, RB0, high using LATB0.

  @Description
    Sets the GPIO pin, RB0, high using LATB0.

  @Preconditions
    The RB0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB0 high (1)
    LED23_OUTB09_SetHigh();
    </code>

*/
#define LED23_OUTB09_SetHigh()          (_LATB0 = 1)
/**
  @Summary
    Sets the GPIO pin, RB0, low using LATB0.

  @Description
    Sets the GPIO pin, RB0, low using LATB0.

  @Preconditions
    The RB0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB0 low (0)
    LED23_OUTB09_SetLow();
    </code>

*/
#define LED23_OUTB09_SetLow()           (_LATB0 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB0, using LATB0.

  @Description
    Toggles the GPIO pin, RB0, using LATB0.

  @Preconditions
    The RB0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB0
    LED23_OUTB09_Toggle();
    </code>

*/
#define LED23_OUTB09_Toggle()           (_LATB0 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB0.

  @Description
    Reads the value of the GPIO pin, RB0.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB0
    postValue = LED23_OUTB09_GetValue();
    </code>

*/
#define LED23_OUTB09_GetValue()         _RB0
/**
  @Summary
    Configures the GPIO pin, RB0, as an input.

  @Description
    Configures the GPIO pin, RB0, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB0 as an input
    LED23_OUTB09_SetDigitalInput();
    </code>

*/
#define LED23_OUTB09_SetDigitalInput()  (_TRISB0 = 1)
/**
  @Summary
    Configures the GPIO pin, RB0, as an output.

  @Description
    Configures the GPIO pin, RB0, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB0 as an output
    LED23_OUTB09_SetDigitalOutput();
    </code>

*/
#define LED23_OUTB09_SetDigitalOutput() (_TRISB0 = 0)
/**
  @Summary
    Sets the GPIO pin, RB1, high using LATB1.

  @Description
    Sets the GPIO pin, RB1, high using LATB1.

  @Preconditions
    The RB1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB1 high (1)
    LED24_OUTB10_SetHigh();
    </code>

*/
#define LED24_OUTB10_SetHigh()          (_LATB1 = 1)
/**
  @Summary
    Sets the GPIO pin, RB1, low using LATB1.

  @Description
    Sets the GPIO pin, RB1, low using LATB1.

  @Preconditions
    The RB1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB1 low (0)
    LED24_OUTB10_SetLow();
    </code>

*/
#define LED24_OUTB10_SetLow()           (_LATB1 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB1, using LATB1.

  @Description
    Toggles the GPIO pin, RB1, using LATB1.

  @Preconditions
    The RB1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB1
    LED24_OUTB10_Toggle();
    </code>

*/
#define LED24_OUTB10_Toggle()           (_LATB1 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB1.

  @Description
    Reads the value of the GPIO pin, RB1.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB1
    postValue = LED24_OUTB10_GetValue();
    </code>

*/
#define LED24_OUTB10_GetValue()         _RB1
/**
  @Summary
    Configures the GPIO pin, RB1, as an input.

  @Description
    Configures the GPIO pin, RB1, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB1 as an input
    LED24_OUTB10_SetDigitalInput();
    </code>

*/
#define LED24_OUTB10_SetDigitalInput()  (_TRISB1 = 1)
/**
  @Summary
    Configures the GPIO pin, RB1, as an output.

  @Description
    Configures the GPIO pin, RB1, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB1 as an output
    LED24_OUTB10_SetDigitalOutput();
    </code>

*/
#define LED24_OUTB10_SetDigitalOutput() (_TRISB1 = 0)
/**
  @Summary
    Sets the GPIO pin, RB12, high using LATB12.

  @Description
    Sets the GPIO pin, RB12, high using LATB12.

  @Preconditions
    The RB12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB12 high (1)
    LED01_OUT01_SetHigh();
    </code>

*/
#define LED01_OUT01_SetHigh()          (_LATB12 = 1)
/**
  @Summary
    Sets the GPIO pin, RB12, low using LATB12.

  @Description
    Sets the GPIO pin, RB12, low using LATB12.

  @Preconditions
    The RB12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB12 low (0)
    LED01_OUT01_SetLow();
    </code>

*/
#define LED01_OUT01_SetLow()           (_LATB12 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB12, using LATB12.

  @Description
    Toggles the GPIO pin, RB12, using LATB12.

  @Preconditions
    The RB12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB12
    LED01_OUT01_Toggle();
    </code>

*/
#define LED01_OUT01_Toggle()           (_LATB12 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB12.

  @Description
    Reads the value of the GPIO pin, RB12.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB12
    postValue = LED01_OUT01_GetValue();
    </code>

*/
#define LED01_OUT01_GetValue()         _RB12
/**
  @Summary
    Configures the GPIO pin, RB12, as an input.

  @Description
    Configures the GPIO pin, RB12, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB12 as an input
    LED01_OUT01_SetDigitalInput();
    </code>

*/
#define LED01_OUT01_SetDigitalInput()  (_TRISB12 = 1)
/**
  @Summary
    Configures the GPIO pin, RB12, as an output.

  @Description
    Configures the GPIO pin, RB12, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB12 as an output
    LED01_OUT01_SetDigitalOutput();
    </code>

*/
#define LED01_OUT01_SetDigitalOutput() (_TRISB12 = 0)
/**
  @Summary
    Sets the GPIO pin, RB13, high using LATB13.

  @Description
    Sets the GPIO pin, RB13, high using LATB13.

  @Preconditions
    The RB13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB13 high (1)
    LED02_OUT02_SetHigh();
    </code>

*/
#define LED02_OUT02_SetHigh()          (_LATB13 = 1)
/**
  @Summary
    Sets the GPIO pin, RB13, low using LATB13.

  @Description
    Sets the GPIO pin, RB13, low using LATB13.

  @Preconditions
    The RB13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB13 low (0)
    LED02_OUT02_SetLow();
    </code>

*/
#define LED02_OUT02_SetLow()           (_LATB13 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB13, using LATB13.

  @Description
    Toggles the GPIO pin, RB13, using LATB13.

  @Preconditions
    The RB13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB13
    LED02_OUT02_Toggle();
    </code>

*/
#define LED02_OUT02_Toggle()           (_LATB13 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB13.

  @Description
    Reads the value of the GPIO pin, RB13.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB13
    postValue = LED02_OUT02_GetValue();
    </code>

*/
#define LED02_OUT02_GetValue()         _RB13
/**
  @Summary
    Configures the GPIO pin, RB13, as an input.

  @Description
    Configures the GPIO pin, RB13, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB13 as an input
    LED02_OUT02_SetDigitalInput();
    </code>

*/
#define LED02_OUT02_SetDigitalInput()  (_TRISB13 = 1)
/**
  @Summary
    Configures the GPIO pin, RB13, as an output.

  @Description
    Configures the GPIO pin, RB13, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB13 as an output
    LED02_OUT02_SetDigitalOutput();
    </code>

*/
#define LED02_OUT02_SetDigitalOutput() (_TRISB13 = 0)
/**
  @Summary
    Sets the GPIO pin, RB14, high using LATB14.

  @Description
    Sets the GPIO pin, RB14, high using LATB14.

  @Preconditions
    The RB14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB14 high (1)
    LED06_OUT06_SetHigh();
    </code>

*/
#define LED06_OUT06_SetHigh()          (_LATB14 = 1)
/**
  @Summary
    Sets the GPIO pin, RB14, low using LATB14.

  @Description
    Sets the GPIO pin, RB14, low using LATB14.

  @Preconditions
    The RB14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB14 low (0)
    LED06_OUT06_SetLow();
    </code>

*/
#define LED06_OUT06_SetLow()           (_LATB14 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB14, using LATB14.

  @Description
    Toggles the GPIO pin, RB14, using LATB14.

  @Preconditions
    The RB14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB14
    LED06_OUT06_Toggle();
    </code>

*/
#define LED06_OUT06_Toggle()           (_LATB14 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB14.

  @Description
    Reads the value of the GPIO pin, RB14.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB14
    postValue = LED06_OUT06_GetValue();
    </code>

*/
#define LED06_OUT06_GetValue()         _RB14
/**
  @Summary
    Configures the GPIO pin, RB14, as an input.

  @Description
    Configures the GPIO pin, RB14, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB14 as an input
    LED06_OUT06_SetDigitalInput();
    </code>

*/
#define LED06_OUT06_SetDigitalInput()  (_TRISB14 = 1)
/**
  @Summary
    Configures the GPIO pin, RB14, as an output.

  @Description
    Configures the GPIO pin, RB14, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB14 as an output
    LED06_OUT06_SetDigitalOutput();
    </code>

*/
#define LED06_OUT06_SetDigitalOutput() (_TRISB14 = 0)
/**
  @Summary
    Sets the GPIO pin, RB15, high using LATB15.

  @Description
    Sets the GPIO pin, RB15, high using LATB15.

  @Preconditions
    The RB15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB15 high (1)
    LED07_OUT07_SetHigh();
    </code>

*/
#define LED07_OUT07_SetHigh()          (_LATB15 = 1)
/**
  @Summary
    Sets the GPIO pin, RB15, low using LATB15.

  @Description
    Sets the GPIO pin, RB15, low using LATB15.

  @Preconditions
    The RB15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB15 low (0)
    LED07_OUT07_SetLow();
    </code>

*/
#define LED07_OUT07_SetLow()           (_LATB15 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB15, using LATB15.

  @Description
    Toggles the GPIO pin, RB15, using LATB15.

  @Preconditions
    The RB15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB15
    LED07_OUT07_Toggle();
    </code>

*/
#define LED07_OUT07_Toggle()           (_LATB15 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB15.

  @Description
    Reads the value of the GPIO pin, RB15.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB15
    postValue = LED07_OUT07_GetValue();
    </code>

*/
#define LED07_OUT07_GetValue()         _RB15
/**
  @Summary
    Configures the GPIO pin, RB15, as an input.

  @Description
    Configures the GPIO pin, RB15, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB15 as an input
    LED07_OUT07_SetDigitalInput();
    </code>

*/
#define LED07_OUT07_SetDigitalInput()  (_TRISB15 = 1)
/**
  @Summary
    Configures the GPIO pin, RB15, as an output.

  @Description
    Configures the GPIO pin, RB15, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB15 as an output
    LED07_OUT07_SetDigitalOutput();
    </code>

*/
#define LED07_OUT07_SetDigitalOutput() (_TRISB15 = 0)
/**
  @Summary
    Sets the GPIO pin, RB5, high using LATB5.

  @Description
    Sets the GPIO pin, RB5, high using LATB5.

  @Preconditions
    The RB5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB5 high (1)
    SW23_INB09_SetHigh();
    </code>

*/
#define SW23_INB09_SetHigh()          (_LATB5 = 1)
/**
  @Summary
    Sets the GPIO pin, RB5, low using LATB5.

  @Description
    Sets the GPIO pin, RB5, low using LATB5.

  @Preconditions
    The RB5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB5 low (0)
    SW23_INB09_SetLow();
    </code>

*/
#define SW23_INB09_SetLow()           (_LATB5 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB5, using LATB5.

  @Description
    Toggles the GPIO pin, RB5, using LATB5.

  @Preconditions
    The RB5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB5
    SW23_INB09_Toggle();
    </code>

*/
#define SW23_INB09_Toggle()           (_LATB5 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB5.

  @Description
    Reads the value of the GPIO pin, RB5.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB5
    postValue = SW23_INB09_GetValue();
    </code>

*/
#define SW23_INB09_GetValue()         _RB5
/**
  @Summary
    Configures the GPIO pin, RB5, as an input.

  @Description
    Configures the GPIO pin, RB5, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB5 as an input
    SW23_INB09_SetDigitalInput();
    </code>

*/
#define SW23_INB09_SetDigitalInput()  (_TRISB5 = 1)
/**
  @Summary
    Configures the GPIO pin, RB5, as an output.

  @Description
    Configures the GPIO pin, RB5, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB5 as an output
    SW23_INB09_SetDigitalOutput();
    </code>

*/
#define SW23_INB09_SetDigitalOutput() (_TRISB5 = 0)
/**
  @Summary
    Sets the GPIO pin, RB6, high using LATB6.

  @Description
    Sets the GPIO pin, RB6, high using LATB6.

  @Preconditions
    The RB6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB6 high (1)
    SW24_INB10_SetHigh();
    </code>

*/
#define SW24_INB10_SetHigh()          (_LATB6 = 1)
/**
  @Summary
    Sets the GPIO pin, RB6, low using LATB6.

  @Description
    Sets the GPIO pin, RB6, low using LATB6.

  @Preconditions
    The RB6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB6 low (0)
    SW24_INB10_SetLow();
    </code>

*/
#define SW24_INB10_SetLow()           (_LATB6 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB6, using LATB6.

  @Description
    Toggles the GPIO pin, RB6, using LATB6.

  @Preconditions
    The RB6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB6
    SW24_INB10_Toggle();
    </code>

*/
#define SW24_INB10_Toggle()           (_LATB6 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB6.

  @Description
    Reads the value of the GPIO pin, RB6.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB6
    postValue = SW24_INB10_GetValue();
    </code>

*/
#define SW24_INB10_GetValue()         _RB6
/**
  @Summary
    Configures the GPIO pin, RB6, as an input.

  @Description
    Configures the GPIO pin, RB6, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB6 as an input
    SW24_INB10_SetDigitalInput();
    </code>

*/
#define SW24_INB10_SetDigitalInput()  (_TRISB6 = 1)
/**
  @Summary
    Configures the GPIO pin, RB6, as an output.

  @Description
    Configures the GPIO pin, RB6, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB6 as an output
    SW24_INB10_SetDigitalOutput();
    </code>

*/
#define SW24_INB10_SetDigitalOutput() (_TRISB6 = 0)
/**
  @Summary
    Sets the GPIO pin, RB7, high using LATB7.

  @Description
    Sets the GPIO pin, RB7, high using LATB7.

  @Preconditions
    The RB7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB7 high (1)
    SW26_INB12_SetHigh();
    </code>

*/
#define SW26_INB12_SetHigh()          (_LATB7 = 1)
/**
  @Summary
    Sets the GPIO pin, RB7, low using LATB7.

  @Description
    Sets the GPIO pin, RB7, low using LATB7.

  @Preconditions
    The RB7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB7 low (0)
    SW26_INB12_SetLow();
    </code>

*/
#define SW26_INB12_SetLow()           (_LATB7 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB7, using LATB7.

  @Description
    Toggles the GPIO pin, RB7, using LATB7.

  @Preconditions
    The RB7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB7
    SW26_INB12_Toggle();
    </code>

*/
#define SW26_INB12_Toggle()           (_LATB7 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB7.

  @Description
    Reads the value of the GPIO pin, RB7.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB7
    postValue = SW26_INB12_GetValue();
    </code>

*/
#define SW26_INB12_GetValue()         _RB7
/**
  @Summary
    Configures the GPIO pin, RB7, as an input.

  @Description
    Configures the GPIO pin, RB7, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB7 as an input
    SW26_INB12_SetDigitalInput();
    </code>

*/
#define SW26_INB12_SetDigitalInput()  (_TRISB7 = 1)
/**
  @Summary
    Configures the GPIO pin, RB7, as an output.

  @Description
    Configures the GPIO pin, RB7, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB7 as an output
    SW26_INB12_SetDigitalOutput();
    </code>

*/
#define SW26_INB12_SetDigitalOutput() (_TRISB7 = 0)
/**
  @Summary
    Sets the GPIO pin, RB8, high using LATB8.

  @Description
    Sets the GPIO pin, RB8, high using LATB8.

  @Preconditions
    The RB8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB8 high (1)
    SW28_INB14_SetHigh();
    </code>

*/
#define SW28_INB14_SetHigh()          (_LATB8 = 1)
/**
  @Summary
    Sets the GPIO pin, RB8, low using LATB8.

  @Description
    Sets the GPIO pin, RB8, low using LATB8.

  @Preconditions
    The RB8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RB8 low (0)
    SW28_INB14_SetLow();
    </code>

*/
#define SW28_INB14_SetLow()           (_LATB8 = 0)
/**
  @Summary
    Toggles the GPIO pin, RB8, using LATB8.

  @Description
    Toggles the GPIO pin, RB8, using LATB8.

  @Preconditions
    The RB8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RB8
    SW28_INB14_Toggle();
    </code>

*/
#define SW28_INB14_Toggle()           (_LATB8 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RB8.

  @Description
    Reads the value of the GPIO pin, RB8.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RB8
    postValue = SW28_INB14_GetValue();
    </code>

*/
#define SW28_INB14_GetValue()         _RB8
/**
  @Summary
    Configures the GPIO pin, RB8, as an input.

  @Description
    Configures the GPIO pin, RB8, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB8 as an input
    SW28_INB14_SetDigitalInput();
    </code>

*/
#define SW28_INB14_SetDigitalInput()  (_TRISB8 = 1)
/**
  @Summary
    Configures the GPIO pin, RB8, as an output.

  @Description
    Configures the GPIO pin, RB8, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RB8 as an output
    SW28_INB14_SetDigitalOutput();
    </code>

*/
#define SW28_INB14_SetDigitalOutput() (_TRISB8 = 0)
/**
  @Summary
    Sets the GPIO pin, RC0, high using LATC0.

  @Description
    Sets the GPIO pin, RC0, high using LATC0.

  @Preconditions
    The RC0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC0 high (1)
    LED27_OUTB13_SetHigh();
    </code>

*/
#define LED27_OUTB13_SetHigh()          (_LATC0 = 1)
/**
  @Summary
    Sets the GPIO pin, RC0, low using LATC0.

  @Description
    Sets the GPIO pin, RC0, low using LATC0.

  @Preconditions
    The RC0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC0 low (0)
    LED27_OUTB13_SetLow();
    </code>

*/
#define LED27_OUTB13_SetLow()           (_LATC0 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC0, using LATC0.

  @Description
    Toggles the GPIO pin, RC0, using LATC0.

  @Preconditions
    The RC0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC0
    LED27_OUTB13_Toggle();
    </code>

*/
#define LED27_OUTB13_Toggle()           (_LATC0 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC0.

  @Description
    Reads the value of the GPIO pin, RC0.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC0
    postValue = LED27_OUTB13_GetValue();
    </code>

*/
#define LED27_OUTB13_GetValue()         _RC0
/**
  @Summary
    Configures the GPIO pin, RC0, as an input.

  @Description
    Configures the GPIO pin, RC0, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC0 as an input
    LED27_OUTB13_SetDigitalInput();
    </code>

*/
#define LED27_OUTB13_SetDigitalInput()  (_TRISC0 = 1)
/**
  @Summary
    Configures the GPIO pin, RC0, as an output.

  @Description
    Configures the GPIO pin, RC0, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC0 as an output
    LED27_OUTB13_SetDigitalOutput();
    </code>

*/
#define LED27_OUTB13_SetDigitalOutput() (_TRISC0 = 0)
/**
  @Summary
    Sets the GPIO pin, RC1, high using LATC1.

  @Description
    Sets the GPIO pin, RC1, high using LATC1.

  @Preconditions
    The RC1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC1 high (1)
    LED28_OUTB14_SetHigh();
    </code>

*/
#define LED28_OUTB14_SetHigh()          (_LATC1 = 1)
/**
  @Summary
    Sets the GPIO pin, RC1, low using LATC1.

  @Description
    Sets the GPIO pin, RC1, low using LATC1.

  @Preconditions
    The RC1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC1 low (0)
    LED28_OUTB14_SetLow();
    </code>

*/
#define LED28_OUTB14_SetLow()           (_LATC1 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC1, using LATC1.

  @Description
    Toggles the GPIO pin, RC1, using LATC1.

  @Preconditions
    The RC1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC1
    LED28_OUTB14_Toggle();
    </code>

*/
#define LED28_OUTB14_Toggle()           (_LATC1 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC1.

  @Description
    Reads the value of the GPIO pin, RC1.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC1
    postValue = LED28_OUTB14_GetValue();
    </code>

*/
#define LED28_OUTB14_GetValue()         _RC1
/**
  @Summary
    Configures the GPIO pin, RC1, as an input.

  @Description
    Configures the GPIO pin, RC1, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC1 as an input
    LED28_OUTB14_SetDigitalInput();
    </code>

*/
#define LED28_OUTB14_SetDigitalInput()  (_TRISC1 = 1)
/**
  @Summary
    Configures the GPIO pin, RC1, as an output.

  @Description
    Configures the GPIO pin, RC1, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC1 as an output
    LED28_OUTB14_SetDigitalOutput();
    </code>

*/
#define LED28_OUTB14_SetDigitalOutput() (_TRISC1 = 0)
/**
  @Summary
    Sets the GPIO pin, RC10, high using LATC10.

  @Description
    Sets the GPIO pin, RC10, high using LATC10.

  @Preconditions
    The RC10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC10 high (1)
    SW25_INB11_SetHigh();
    </code>

*/
#define SW25_INB11_SetHigh()          (_LATC10 = 1)
/**
  @Summary
    Sets the GPIO pin, RC10, low using LATC10.

  @Description
    Sets the GPIO pin, RC10, low using LATC10.

  @Preconditions
    The RC10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC10 low (0)
    SW25_INB11_SetLow();
    </code>

*/
#define SW25_INB11_SetLow()           (_LATC10 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC10, using LATC10.

  @Description
    Toggles the GPIO pin, RC10, using LATC10.

  @Preconditions
    The RC10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC10
    SW25_INB11_Toggle();
    </code>

*/
#define SW25_INB11_Toggle()           (_LATC10 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC10.

  @Description
    Reads the value of the GPIO pin, RC10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC10
    postValue = SW25_INB11_GetValue();
    </code>

*/
#define SW25_INB11_GetValue()         _RC10
/**
  @Summary
    Configures the GPIO pin, RC10, as an input.

  @Description
    Configures the GPIO pin, RC10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC10 as an input
    SW25_INB11_SetDigitalInput();
    </code>

*/
#define SW25_INB11_SetDigitalInput()  (_TRISC10 = 1)
/**
  @Summary
    Configures the GPIO pin, RC10, as an output.

  @Description
    Configures the GPIO pin, RC10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC10 as an output
    SW25_INB11_SetDigitalOutput();
    </code>

*/
#define SW25_INB11_SetDigitalOutput() (_TRISC10 = 0)
/**
  @Summary
    Sets the GPIO pin, RC11, high using LATC11.

  @Description
    Sets the GPIO pin, RC11, high using LATC11.

  @Preconditions
    The RC11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC11 high (1)
    SW02_IN02_SetHigh();
    </code>

*/
#define SW02_IN02_SetHigh()          (_LATC11 = 1)
/**
  @Summary
    Sets the GPIO pin, RC11, low using LATC11.

  @Description
    Sets the GPIO pin, RC11, low using LATC11.

  @Preconditions
    The RC11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC11 low (0)
    SW02_IN02_SetLow();
    </code>

*/
#define SW02_IN02_SetLow()           (_LATC11 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC11, using LATC11.

  @Description
    Toggles the GPIO pin, RC11, using LATC11.

  @Preconditions
    The RC11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC11
    SW02_IN02_Toggle();
    </code>

*/
#define SW02_IN02_Toggle()           (_LATC11 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC11.

  @Description
    Reads the value of the GPIO pin, RC11.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC11
    postValue = SW02_IN02_GetValue();
    </code>

*/
#define SW02_IN02_GetValue()         _RC11
/**
  @Summary
    Configures the GPIO pin, RC11, as an input.

  @Description
    Configures the GPIO pin, RC11, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC11 as an input
    SW02_IN02_SetDigitalInput();
    </code>

*/
#define SW02_IN02_SetDigitalInput()  (_TRISC11 = 1)
/**
  @Summary
    Configures the GPIO pin, RC11, as an output.

  @Description
    Configures the GPIO pin, RC11, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC11 as an output
    SW02_IN02_SetDigitalOutput();
    </code>

*/
#define SW02_IN02_SetDigitalOutput() (_TRISC11 = 0)
/**
  @Summary
    Sets the GPIO pin, RC13, high using LATC13.

  @Description
    Sets the GPIO pin, RC13, high using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC13 high (1)
    SW27_INB13_SetHigh();
    </code>

*/
#define SW27_INB13_SetHigh()          (_LATC13 = 1)
/**
  @Summary
    Sets the GPIO pin, RC13, low using LATC13.

  @Description
    Sets the GPIO pin, RC13, low using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC13 low (0)
    SW27_INB13_SetLow();
    </code>

*/
#define SW27_INB13_SetLow()           (_LATC13 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC13, using LATC13.

  @Description
    Toggles the GPIO pin, RC13, using LATC13.

  @Preconditions
    The RC13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC13
    SW27_INB13_Toggle();
    </code>

*/
#define SW27_INB13_Toggle()           (_LATC13 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC13.

  @Description
    Reads the value of the GPIO pin, RC13.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC13
    postValue = SW27_INB13_GetValue();
    </code>

*/
#define SW27_INB13_GetValue()         _RC13
/**
  @Summary
    Configures the GPIO pin, RC13, as an input.

  @Description
    Configures the GPIO pin, RC13, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC13 as an input
    SW27_INB13_SetDigitalInput();
    </code>

*/
#define SW27_INB13_SetDigitalInput()  (_TRISC13 = 1)
/**
  @Summary
    Configures the GPIO pin, RC13, as an output.

  @Description
    Configures the GPIO pin, RC13, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC13 as an output
    SW27_INB13_SetDigitalOutput();
    </code>

*/
#define SW27_INB13_SetDigitalOutput() (_TRISC13 = 0)
/**
  @Summary
    Sets the GPIO pin, RC2, high using LATC2.

  @Description
    Sets the GPIO pin, RC2, high using LATC2.

  @Preconditions
    The RC2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC2 high (1)
    SW01_IN01_SetHigh();
    </code>

*/
#define SW01_IN01_SetHigh()          (_LATC2 = 1)
/**
  @Summary
    Sets the GPIO pin, RC2, low using LATC2.

  @Description
    Sets the GPIO pin, RC2, low using LATC2.

  @Preconditions
    The RC2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC2 low (0)
    SW01_IN01_SetLow();
    </code>

*/
#define SW01_IN01_SetLow()           (_LATC2 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC2, using LATC2.

  @Description
    Toggles the GPIO pin, RC2, using LATC2.

  @Preconditions
    The RC2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC2
    SW01_IN01_Toggle();
    </code>

*/
#define SW01_IN01_Toggle()           (_LATC2 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC2.

  @Description
    Reads the value of the GPIO pin, RC2.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC2
    postValue = SW01_IN01_GetValue();
    </code>

*/
#define SW01_IN01_GetValue()         _RC2
/**
  @Summary
    Configures the GPIO pin, RC2, as an input.

  @Description
    Configures the GPIO pin, RC2, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC2 as an input
    SW01_IN01_SetDigitalInput();
    </code>

*/
#define SW01_IN01_SetDigitalInput()  (_TRISC2 = 1)
/**
  @Summary
    Configures the GPIO pin, RC2, as an output.

  @Description
    Configures the GPIO pin, RC2, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC2 as an output
    SW01_IN01_SetDigitalOutput();
    </code>

*/
#define SW01_IN01_SetDigitalOutput() (_TRISC2 = 0)
/**
  @Summary
    Sets the GPIO pin, RC3, high using LATC3.

  @Description
    Sets the GPIO pin, RC3, high using LATC3.

  @Preconditions
    The RC3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC3 high (1)
    SW15_INB01_SetHigh();
    </code>

*/
#define SW15_INB01_SetHigh()          (_LATC3 = 1)
/**
  @Summary
    Sets the GPIO pin, RC3, low using LATC3.

  @Description
    Sets the GPIO pin, RC3, low using LATC3.

  @Preconditions
    The RC3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RC3 low (0)
    SW15_INB01_SetLow();
    </code>

*/
#define SW15_INB01_SetLow()           (_LATC3 = 0)
/**
  @Summary
    Toggles the GPIO pin, RC3, using LATC3.

  @Description
    Toggles the GPIO pin, RC3, using LATC3.

  @Preconditions
    The RC3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RC3
    SW15_INB01_Toggle();
    </code>

*/
#define SW15_INB01_Toggle()           (_LATC3 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RC3.

  @Description
    Reads the value of the GPIO pin, RC3.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RC3
    postValue = SW15_INB01_GetValue();
    </code>

*/
#define SW15_INB01_GetValue()         _RC3
/**
  @Summary
    Configures the GPIO pin, RC3, as an input.

  @Description
    Configures the GPIO pin, RC3, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC3 as an input
    SW15_INB01_SetDigitalInput();
    </code>

*/
#define SW15_INB01_SetDigitalInput()  (_TRISC3 = 1)
/**
  @Summary
    Configures the GPIO pin, RC3, as an output.

  @Description
    Configures the GPIO pin, RC3, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RC3 as an output
    SW15_INB01_SetDigitalOutput();
    </code>

*/
#define SW15_INB01_SetDigitalOutput() (_TRISC3 = 0)
/**
  @Summary
    Sets the GPIO pin, RD1, high using LATD1.

  @Description
    Sets the GPIO pin, RD1, high using LATD1.

  @Preconditions
    The RD1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD1 high (1)
    LED08_OUT08_SetHigh();
    </code>

*/
#define LED08_OUT08_SetHigh()          (_LATD1 = 1)
/**
  @Summary
    Sets the GPIO pin, RD1, low using LATD1.

  @Description
    Sets the GPIO pin, RD1, low using LATD1.

  @Preconditions
    The RD1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD1 low (0)
    LED08_OUT08_SetLow();
    </code>

*/
#define LED08_OUT08_SetLow()           (_LATD1 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD1, using LATD1.

  @Description
    Toggles the GPIO pin, RD1, using LATD1.

  @Preconditions
    The RD1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD1
    LED08_OUT08_Toggle();
    </code>

*/
#define LED08_OUT08_Toggle()           (_LATD1 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD1.

  @Description
    Reads the value of the GPIO pin, RD1.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD1
    postValue = LED08_OUT08_GetValue();
    </code>

*/
#define LED08_OUT08_GetValue()         _RD1
/**
  @Summary
    Configures the GPIO pin, RD1, as an input.

  @Description
    Configures the GPIO pin, RD1, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD1 as an input
    LED08_OUT08_SetDigitalInput();
    </code>

*/
#define LED08_OUT08_SetDigitalInput()  (_TRISD1 = 1)
/**
  @Summary
    Configures the GPIO pin, RD1, as an output.

  @Description
    Configures the GPIO pin, RD1, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD1 as an output
    LED08_OUT08_SetDigitalOutput();
    </code>

*/
#define LED08_OUT08_SetDigitalOutput() (_TRISD1 = 0)
/**
  @Summary
    Sets the GPIO pin, RD14, high using LATD14.

  @Description
    Sets the GPIO pin, RD14, high using LATD14.

  @Preconditions
    The RD14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD14 high (1)
    SW10_IN10_SetHigh();
    </code>

*/
#define SW10_IN10_SetHigh()          (_LATD14 = 1)
/**
  @Summary
    Sets the GPIO pin, RD14, low using LATD14.

  @Description
    Sets the GPIO pin, RD14, low using LATD14.

  @Preconditions
    The RD14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD14 low (0)
    SW10_IN10_SetLow();
    </code>

*/
#define SW10_IN10_SetLow()           (_LATD14 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD14, using LATD14.

  @Description
    Toggles the GPIO pin, RD14, using LATD14.

  @Preconditions
    The RD14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD14
    SW10_IN10_Toggle();
    </code>

*/
#define SW10_IN10_Toggle()           (_LATD14 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD14.

  @Description
    Reads the value of the GPIO pin, RD14.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD14
    postValue = SW10_IN10_GetValue();
    </code>

*/
#define SW10_IN10_GetValue()         _RD14
/**
  @Summary
    Configures the GPIO pin, RD14, as an input.

  @Description
    Configures the GPIO pin, RD14, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD14 as an input
    SW10_IN10_SetDigitalInput();
    </code>

*/
#define SW10_IN10_SetDigitalInput()  (_TRISD14 = 1)
/**
  @Summary
    Configures the GPIO pin, RD14, as an output.

  @Description
    Configures the GPIO pin, RD14, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD14 as an output
    SW10_IN10_SetDigitalOutput();
    </code>

*/
#define SW10_IN10_SetDigitalOutput() (_TRISD14 = 0)
/**
  @Summary
    Sets the GPIO pin, RD15, high using LATD15.

  @Description
    Sets the GPIO pin, RD15, high using LATD15.

  @Preconditions
    The RD15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD15 high (1)
    SW11_IN11_SetHigh();
    </code>

*/
#define SW11_IN11_SetHigh()          (_LATD15 = 1)
/**
  @Summary
    Sets the GPIO pin, RD15, low using LATD15.

  @Description
    Sets the GPIO pin, RD15, low using LATD15.

  @Preconditions
    The RD15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD15 low (0)
    SW11_IN11_SetLow();
    </code>

*/
#define SW11_IN11_SetLow()           (_LATD15 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD15, using LATD15.

  @Description
    Toggles the GPIO pin, RD15, using LATD15.

  @Preconditions
    The RD15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD15
    SW11_IN11_Toggle();
    </code>

*/
#define SW11_IN11_Toggle()           (_LATD15 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD15.

  @Description
    Reads the value of the GPIO pin, RD15.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD15
    postValue = SW11_IN11_GetValue();
    </code>

*/
#define SW11_IN11_GetValue()         _RD15
/**
  @Summary
    Configures the GPIO pin, RD15, as an input.

  @Description
    Configures the GPIO pin, RD15, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD15 as an input
    SW11_IN11_SetDigitalInput();
    </code>

*/
#define SW11_IN11_SetDigitalInput()  (_TRISD15 = 1)
/**
  @Summary
    Configures the GPIO pin, RD15, as an output.

  @Description
    Configures the GPIO pin, RD15, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD15 as an output
    SW11_IN11_SetDigitalOutput();
    </code>

*/
#define SW11_IN11_SetDigitalOutput() (_TRISD15 = 0)
/**
  @Summary
    Sets the GPIO pin, RD2, high using LATD2.

  @Description
    Sets the GPIO pin, RD2, high using LATD2.

  @Preconditions
    The RD2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD2 high (1)
    LED09_OUT09_SetHigh();
    </code>

*/
#define LED09_OUT09_SetHigh()          (_LATD2 = 1)
/**
  @Summary
    Sets the GPIO pin, RD2, low using LATD2.

  @Description
    Sets the GPIO pin, RD2, low using LATD2.

  @Preconditions
    The RD2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD2 low (0)
    LED09_OUT09_SetLow();
    </code>

*/
#define LED09_OUT09_SetLow()           (_LATD2 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD2, using LATD2.

  @Description
    Toggles the GPIO pin, RD2, using LATD2.

  @Preconditions
    The RD2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD2
    LED09_OUT09_Toggle();
    </code>

*/
#define LED09_OUT09_Toggle()           (_LATD2 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD2.

  @Description
    Reads the value of the GPIO pin, RD2.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD2
    postValue = LED09_OUT09_GetValue();
    </code>

*/
#define LED09_OUT09_GetValue()         _RD2
/**
  @Summary
    Configures the GPIO pin, RD2, as an input.

  @Description
    Configures the GPIO pin, RD2, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD2 as an input
    LED09_OUT09_SetDigitalInput();
    </code>

*/
#define LED09_OUT09_SetDigitalInput()  (_TRISD2 = 1)
/**
  @Summary
    Configures the GPIO pin, RD2, as an output.

  @Description
    Configures the GPIO pin, RD2, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD2 as an output
    LED09_OUT09_SetDigitalOutput();
    </code>

*/
#define LED09_OUT09_SetDigitalOutput() (_TRISD2 = 0)
/**
  @Summary
    Sets the GPIO pin, RD3, high using LATD3.

  @Description
    Sets the GPIO pin, RD3, high using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD3 high (1)
    LED10_OUT10_SetHigh();
    </code>

*/
#define LED10_OUT10_SetHigh()          (_LATD3 = 1)
/**
  @Summary
    Sets the GPIO pin, RD3, low using LATD3.

  @Description
    Sets the GPIO pin, RD3, low using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD3 low (0)
    LED10_OUT10_SetLow();
    </code>

*/
#define LED10_OUT10_SetLow()           (_LATD3 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD3, using LATD3.

  @Description
    Toggles the GPIO pin, RD3, using LATD3.

  @Preconditions
    The RD3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD3
    LED10_OUT10_Toggle();
    </code>

*/
#define LED10_OUT10_Toggle()           (_LATD3 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD3.

  @Description
    Reads the value of the GPIO pin, RD3.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD3
    postValue = LED10_OUT10_GetValue();
    </code>

*/
#define LED10_OUT10_GetValue()         _RD3
/**
  @Summary
    Configures the GPIO pin, RD3, as an input.

  @Description
    Configures the GPIO pin, RD3, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD3 as an input
    LED10_OUT10_SetDigitalInput();
    </code>

*/
#define LED10_OUT10_SetDigitalInput()  (_TRISD3 = 1)
/**
  @Summary
    Configures the GPIO pin, RD3, as an output.

  @Description
    Configures the GPIO pin, RD3, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD3 as an output
    LED10_OUT10_SetDigitalOutput();
    </code>

*/
#define LED10_OUT10_SetDigitalOutput() (_TRISD3 = 0)
/**
  @Summary
    Sets the GPIO pin, RD4, high using LATD4.

  @Description
    Sets the GPIO pin, RD4, high using LATD4.

  @Preconditions
    The RD4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD4 high (1)
    LED11_OUT11_SetHigh();
    </code>

*/
#define LED11_OUT11_SetHigh()          (_LATD4 = 1)
/**
  @Summary
    Sets the GPIO pin, RD4, low using LATD4.

  @Description
    Sets the GPIO pin, RD4, low using LATD4.

  @Preconditions
    The RD4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD4 low (0)
    LED11_OUT11_SetLow();
    </code>

*/
#define LED11_OUT11_SetLow()           (_LATD4 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD4, using LATD4.

  @Description
    Toggles the GPIO pin, RD4, using LATD4.

  @Preconditions
    The RD4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD4
    LED11_OUT11_Toggle();
    </code>

*/
#define LED11_OUT11_Toggle()           (_LATD4 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD4.

  @Description
    Reads the value of the GPIO pin, RD4.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD4
    postValue = LED11_OUT11_GetValue();
    </code>

*/
#define LED11_OUT11_GetValue()         _RD4
/**
  @Summary
    Configures the GPIO pin, RD4, as an input.

  @Description
    Configures the GPIO pin, RD4, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD4 as an input
    LED11_OUT11_SetDigitalInput();
    </code>

*/
#define LED11_OUT11_SetDigitalInput()  (_TRISD4 = 1)
/**
  @Summary
    Configures the GPIO pin, RD4, as an output.

  @Description
    Configures the GPIO pin, RD4, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD4 as an output
    LED11_OUT11_SetDigitalOutput();
    </code>

*/
#define LED11_OUT11_SetDigitalOutput() (_TRISD4 = 0)
/**
  @Summary
    Sets the GPIO pin, RD8, high using LATD8.

  @Description
    Sets the GPIO pin, RD8, high using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD8 high (1)
    SW22_INB08_SetHigh();
    </code>

*/
#define SW22_INB08_SetHigh()          (_LATD8 = 1)
/**
  @Summary
    Sets the GPIO pin, RD8, low using LATD8.

  @Description
    Sets the GPIO pin, RD8, low using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RD8 low (0)
    SW22_INB08_SetLow();
    </code>

*/
#define SW22_INB08_SetLow()           (_LATD8 = 0)
/**
  @Summary
    Toggles the GPIO pin, RD8, using LATD8.

  @Description
    Toggles the GPIO pin, RD8, using LATD8.

  @Preconditions
    The RD8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RD8
    SW22_INB08_Toggle();
    </code>

*/
#define SW22_INB08_Toggle()           (_LATD8 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RD8.

  @Description
    Reads the value of the GPIO pin, RD8.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RD8
    postValue = SW22_INB08_GetValue();
    </code>

*/
#define SW22_INB08_GetValue()         _RD8
/**
  @Summary
    Configures the GPIO pin, RD8, as an input.

  @Description
    Configures the GPIO pin, RD8, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD8 as an input
    SW22_INB08_SetDigitalInput();
    </code>

*/
#define SW22_INB08_SetDigitalInput()  (_TRISD8 = 1)
/**
  @Summary
    Configures the GPIO pin, RD8, as an output.

  @Description
    Configures the GPIO pin, RD8, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RD8 as an output
    SW22_INB08_SetDigitalOutput();
    </code>

*/
#define SW22_INB08_SetDigitalOutput() (_TRISD8 = 0)
/**
  @Summary
    Sets the GPIO pin, RE0, high using LATE0.

  @Description
    Sets the GPIO pin, RE0, high using LATE0.

  @Preconditions
    The RE0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE0 high (1)
    SILENT_MODE_CAN_SMCI_SetHigh();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_SetHigh()          (_LATE0 = 1)
/**
  @Summary
    Sets the GPIO pin, RE0, low using LATE0.

  @Description
    Sets the GPIO pin, RE0, low using LATE0.

  @Preconditions
    The RE0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE0 low (0)
    SILENT_MODE_CAN_SMCI_SetLow();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_SetLow()           (_LATE0 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE0, using LATE0.

  @Description
    Toggles the GPIO pin, RE0, using LATE0.

  @Preconditions
    The RE0 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE0
    SILENT_MODE_CAN_SMCI_Toggle();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_Toggle()           (_LATE0 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE0.

  @Description
    Reads the value of the GPIO pin, RE0.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE0
    postValue = SILENT_MODE_CAN_SMCI_GetValue();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_GetValue()         _RE0
/**
  @Summary
    Configures the GPIO pin, RE0, as an input.

  @Description
    Configures the GPIO pin, RE0, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE0 as an input
    SILENT_MODE_CAN_SMCI_SetDigitalInput();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_SetDigitalInput()  (_TRISE0 = 1)
/**
  @Summary
    Configures the GPIO pin, RE0, as an output.

  @Description
    Configures the GPIO pin, RE0, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE0 as an output
    SILENT_MODE_CAN_SMCI_SetDigitalOutput();
    </code>

*/
#define SILENT_MODE_CAN_SMCI_SetDigitalOutput() (_TRISE0 = 0)
/**
  @Summary
    Sets the GPIO pin, RE1, high using LATE1.

  @Description
    Sets the GPIO pin, RE1, high using LATE1.

  @Preconditions
    The RE1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE1 high (1)
    SW13_IN13_SetHigh();
    </code>

*/
#define SW13_IN13_SetHigh()          (_LATE1 = 1)
/**
  @Summary
    Sets the GPIO pin, RE1, low using LATE1.

  @Description
    Sets the GPIO pin, RE1, low using LATE1.

  @Preconditions
    The RE1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE1 low (0)
    SW13_IN13_SetLow();
    </code>

*/
#define SW13_IN13_SetLow()           (_LATE1 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE1, using LATE1.

  @Description
    Toggles the GPIO pin, RE1, using LATE1.

  @Preconditions
    The RE1 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE1
    SW13_IN13_Toggle();
    </code>

*/
#define SW13_IN13_Toggle()           (_LATE1 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE1.

  @Description
    Reads the value of the GPIO pin, RE1.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE1
    postValue = SW13_IN13_GetValue();
    </code>

*/
#define SW13_IN13_GetValue()         _RE1
/**
  @Summary
    Configures the GPIO pin, RE1, as an input.

  @Description
    Configures the GPIO pin, RE1, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE1 as an input
    SW13_IN13_SetDigitalInput();
    </code>

*/
#define SW13_IN13_SetDigitalInput()  (_TRISE1 = 1)
/**
  @Summary
    Configures the GPIO pin, RE1, as an output.

  @Description
    Configures the GPIO pin, RE1, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE1 as an output
    SW13_IN13_SetDigitalOutput();
    </code>

*/
#define SW13_IN13_SetDigitalOutput() (_TRISE1 = 0)
/**
  @Summary
    Sets the GPIO pin, RE12, high using LATE12.

  @Description
    Sets the GPIO pin, RE12, high using LATE12.

  @Preconditions
    The RE12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE12 high (1)
    SW06_IN06_SetHigh();
    </code>

*/
#define SW06_IN06_SetHigh()          (_LATE12 = 1)
/**
  @Summary
    Sets the GPIO pin, RE12, low using LATE12.

  @Description
    Sets the GPIO pin, RE12, low using LATE12.

  @Preconditions
    The RE12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE12 low (0)
    SW06_IN06_SetLow();
    </code>

*/
#define SW06_IN06_SetLow()           (_LATE12 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE12, using LATE12.

  @Description
    Toggles the GPIO pin, RE12, using LATE12.

  @Preconditions
    The RE12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE12
    SW06_IN06_Toggle();
    </code>

*/
#define SW06_IN06_Toggle()           (_LATE12 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE12.

  @Description
    Reads the value of the GPIO pin, RE12.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE12
    postValue = SW06_IN06_GetValue();
    </code>

*/
#define SW06_IN06_GetValue()         _RE12
/**
  @Summary
    Configures the GPIO pin, RE12, as an input.

  @Description
    Configures the GPIO pin, RE12, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE12 as an input
    SW06_IN06_SetDigitalInput();
    </code>

*/
#define SW06_IN06_SetDigitalInput()  (_TRISE12 = 1)
/**
  @Summary
    Configures the GPIO pin, RE12, as an output.

  @Description
    Configures the GPIO pin, RE12, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE12 as an output
    SW06_IN06_SetDigitalOutput();
    </code>

*/
#define SW06_IN06_SetDigitalOutput() (_TRISE12 = 0)
/**
  @Summary
    Sets the GPIO pin, RE13, high using LATE13.

  @Description
    Sets the GPIO pin, RE13, high using LATE13.

  @Preconditions
    The RE13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE13 high (1)
    SW07_IN07_SetHigh();
    </code>

*/
#define SW07_IN07_SetHigh()          (_LATE13 = 1)
/**
  @Summary
    Sets the GPIO pin, RE13, low using LATE13.

  @Description
    Sets the GPIO pin, RE13, low using LATE13.

  @Preconditions
    The RE13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE13 low (0)
    SW07_IN07_SetLow();
    </code>

*/
#define SW07_IN07_SetLow()           (_LATE13 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE13, using LATE13.

  @Description
    Toggles the GPIO pin, RE13, using LATE13.

  @Preconditions
    The RE13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE13
    SW07_IN07_Toggle();
    </code>

*/
#define SW07_IN07_Toggle()           (_LATE13 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE13.

  @Description
    Reads the value of the GPIO pin, RE13.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE13
    postValue = SW07_IN07_GetValue();
    </code>

*/
#define SW07_IN07_GetValue()         _RE13
/**
  @Summary
    Configures the GPIO pin, RE13, as an input.

  @Description
    Configures the GPIO pin, RE13, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE13 as an input
    SW07_IN07_SetDigitalInput();
    </code>

*/
#define SW07_IN07_SetDigitalInput()  (_TRISE13 = 1)
/**
  @Summary
    Configures the GPIO pin, RE13, as an output.

  @Description
    Configures the GPIO pin, RE13, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE13 as an output
    SW07_IN07_SetDigitalOutput();
    </code>

*/
#define SW07_IN07_SetDigitalOutput() (_TRISE13 = 0)
/**
  @Summary
    Sets the GPIO pin, RE14, high using LATE14.

  @Description
    Sets the GPIO pin, RE14, high using LATE14.

  @Preconditions
    The RE14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE14 high (1)
    SW08_IN08_SetHigh();
    </code>

*/
#define SW08_IN08_SetHigh()          (_LATE14 = 1)
/**
  @Summary
    Sets the GPIO pin, RE14, low using LATE14.

  @Description
    Sets the GPIO pin, RE14, low using LATE14.

  @Preconditions
    The RE14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE14 low (0)
    SW08_IN08_SetLow();
    </code>

*/
#define SW08_IN08_SetLow()           (_LATE14 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE14, using LATE14.

  @Description
    Toggles the GPIO pin, RE14, using LATE14.

  @Preconditions
    The RE14 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE14
    SW08_IN08_Toggle();
    </code>

*/
#define SW08_IN08_Toggle()           (_LATE14 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE14.

  @Description
    Reads the value of the GPIO pin, RE14.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE14
    postValue = SW08_IN08_GetValue();
    </code>

*/
#define SW08_IN08_GetValue()         _RE14
/**
  @Summary
    Configures the GPIO pin, RE14, as an input.

  @Description
    Configures the GPIO pin, RE14, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE14 as an input
    SW08_IN08_SetDigitalInput();
    </code>

*/
#define SW08_IN08_SetDigitalInput()  (_TRISE14 = 1)
/**
  @Summary
    Configures the GPIO pin, RE14, as an output.

  @Description
    Configures the GPIO pin, RE14, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE14 as an output
    SW08_IN08_SetDigitalOutput();
    </code>

*/
#define SW08_IN08_SetDigitalOutput() (_TRISE14 = 0)
/**
  @Summary
    Sets the GPIO pin, RE15, high using LATE15.

  @Description
    Sets the GPIO pin, RE15, high using LATE15.

  @Preconditions
    The RE15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE15 high (1)
    SW09_IN09_SetHigh();
    </code>

*/
#define SW09_IN09_SetHigh()          (_LATE15 = 1)
/**
  @Summary
    Sets the GPIO pin, RE15, low using LATE15.

  @Description
    Sets the GPIO pin, RE15, low using LATE15.

  @Preconditions
    The RE15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE15 low (0)
    SW09_IN09_SetLow();
    </code>

*/
#define SW09_IN09_SetLow()           (_LATE15 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE15, using LATE15.

  @Description
    Toggles the GPIO pin, RE15, using LATE15.

  @Preconditions
    The RE15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE15
    SW09_IN09_Toggle();
    </code>

*/
#define SW09_IN09_Toggle()           (_LATE15 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE15.

  @Description
    Reads the value of the GPIO pin, RE15.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE15
    postValue = SW09_IN09_GetValue();
    </code>

*/
#define SW09_IN09_GetValue()         _RE15
/**
  @Summary
    Configures the GPIO pin, RE15, as an input.

  @Description
    Configures the GPIO pin, RE15, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE15 as an input
    SW09_IN09_SetDigitalInput();
    </code>

*/
#define SW09_IN09_SetDigitalInput()  (_TRISE15 = 1)
/**
  @Summary
    Configures the GPIO pin, RE15, as an output.

  @Description
    Configures the GPIO pin, RE15, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE15 as an output
    SW09_IN09_SetDigitalOutput();
    </code>

*/
#define SW09_IN09_SetDigitalOutput() (_TRISE15 = 0)
/**
  @Summary
    Sets the GPIO pin, RE8, high using LATE8.

  @Description
    Sets the GPIO pin, RE8, high using LATE8.

  @Preconditions
    The RE8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE8 high (1)
    LED17_OUTB03_SetHigh();
    </code>

*/
#define LED17_OUTB03_SetHigh()          (_LATE8 = 1)
/**
  @Summary
    Sets the GPIO pin, RE8, low using LATE8.

  @Description
    Sets the GPIO pin, RE8, low using LATE8.

  @Preconditions
    The RE8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE8 low (0)
    LED17_OUTB03_SetLow();
    </code>

*/
#define LED17_OUTB03_SetLow()           (_LATE8 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE8, using LATE8.

  @Description
    Toggles the GPIO pin, RE8, using LATE8.

  @Preconditions
    The RE8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE8
    LED17_OUTB03_Toggle();
    </code>

*/
#define LED17_OUTB03_Toggle()           (_LATE8 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE8.

  @Description
    Reads the value of the GPIO pin, RE8.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE8
    postValue = LED17_OUTB03_GetValue();
    </code>

*/
#define LED17_OUTB03_GetValue()         _RE8
/**
  @Summary
    Configures the GPIO pin, RE8, as an input.

  @Description
    Configures the GPIO pin, RE8, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE8 as an input
    LED17_OUTB03_SetDigitalInput();
    </code>

*/
#define LED17_OUTB03_SetDigitalInput()  (_TRISE8 = 1)
/**
  @Summary
    Configures the GPIO pin, RE8, as an output.

  @Description
    Configures the GPIO pin, RE8, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE8 as an output
    LED17_OUTB03_SetDigitalOutput();
    </code>

*/
#define LED17_OUTB03_SetDigitalOutput() (_TRISE8 = 0)
/**
  @Summary
    Sets the GPIO pin, RE9, high using LATE9.

  @Description
    Sets the GPIO pin, RE9, high using LATE9.

  @Preconditions
    The RE9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE9 high (1)
    LED18_OUTB04_SetHigh();
    </code>

*/
#define LED18_OUTB04_SetHigh()          (_LATE9 = 1)
/**
  @Summary
    Sets the GPIO pin, RE9, low using LATE9.

  @Description
    Sets the GPIO pin, RE9, low using LATE9.

  @Preconditions
    The RE9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RE9 low (0)
    LED18_OUTB04_SetLow();
    </code>

*/
#define LED18_OUTB04_SetLow()           (_LATE9 = 0)
/**
  @Summary
    Toggles the GPIO pin, RE9, using LATE9.

  @Description
    Toggles the GPIO pin, RE9, using LATE9.

  @Preconditions
    The RE9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RE9
    LED18_OUTB04_Toggle();
    </code>

*/
#define LED18_OUTB04_Toggle()           (_LATE9 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RE9.

  @Description
    Reads the value of the GPIO pin, RE9.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RE9
    postValue = LED18_OUTB04_GetValue();
    </code>

*/
#define LED18_OUTB04_GetValue()         _RE9
/**
  @Summary
    Configures the GPIO pin, RE9, as an input.

  @Description
    Configures the GPIO pin, RE9, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE9 as an input
    LED18_OUTB04_SetDigitalInput();
    </code>

*/
#define LED18_OUTB04_SetDigitalInput()  (_TRISE9 = 1)
/**
  @Summary
    Configures the GPIO pin, RE9, as an output.

  @Description
    Configures the GPIO pin, RE9, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RE9 as an output
    LED18_OUTB04_SetDigitalOutput();
    </code>

*/
#define LED18_OUTB04_SetDigitalOutput() (_TRISE9 = 0)
/**
  @Summary
    Sets the GPIO pin, RF10, high using LATF10.

  @Description
    Sets the GPIO pin, RF10, high using LATF10.

  @Preconditions
    The RF10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF10 high (1)
    LED26_OUTB12_SetHigh();
    </code>

*/
#define LED26_OUTB12_SetHigh()          (_LATF10 = 1)
/**
  @Summary
    Sets the GPIO pin, RF10, low using LATF10.

  @Description
    Sets the GPIO pin, RF10, low using LATF10.

  @Preconditions
    The RF10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF10 low (0)
    LED26_OUTB12_SetLow();
    </code>

*/
#define LED26_OUTB12_SetLow()           (_LATF10 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF10, using LATF10.

  @Description
    Toggles the GPIO pin, RF10, using LATF10.

  @Preconditions
    The RF10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF10
    LED26_OUTB12_Toggle();
    </code>

*/
#define LED26_OUTB12_Toggle()           (_LATF10 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF10.

  @Description
    Reads the value of the GPIO pin, RF10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF10
    postValue = LED26_OUTB12_GetValue();
    </code>

*/
#define LED26_OUTB12_GetValue()         _RF10
/**
  @Summary
    Configures the GPIO pin, RF10, as an input.

  @Description
    Configures the GPIO pin, RF10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF10 as an input
    LED26_OUTB12_SetDigitalInput();
    </code>

*/
#define LED26_OUTB12_SetDigitalInput()  (_TRISF10 = 1)
/**
  @Summary
    Configures the GPIO pin, RF10, as an output.

  @Description
    Configures the GPIO pin, RF10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF10 as an output
    LED26_OUTB12_SetDigitalOutput();
    </code>

*/
#define LED26_OUTB12_SetDigitalOutput() (_TRISF10 = 0)
/**
  @Summary
    Sets the GPIO pin, RF12, high using LATF12.

  @Description
    Sets the GPIO pin, RF12, high using LATF12.

  @Preconditions
    The RF12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF12 high (1)
    SW05_IN05_SetHigh();
    </code>

*/
#define SW05_IN05_SetHigh()          (_LATF12 = 1)
/**
  @Summary
    Sets the GPIO pin, RF12, low using LATF12.

  @Description
    Sets the GPIO pin, RF12, low using LATF12.

  @Preconditions
    The RF12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF12 low (0)
    SW05_IN05_SetLow();
    </code>

*/
#define SW05_IN05_SetLow()           (_LATF12 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF12, using LATF12.

  @Description
    Toggles the GPIO pin, RF12, using LATF12.

  @Preconditions
    The RF12 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF12
    SW05_IN05_Toggle();
    </code>

*/
#define SW05_IN05_Toggle()           (_LATF12 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF12.

  @Description
    Reads the value of the GPIO pin, RF12.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF12
    postValue = SW05_IN05_GetValue();
    </code>

*/
#define SW05_IN05_GetValue()         _RF12
/**
  @Summary
    Configures the GPIO pin, RF12, as an input.

  @Description
    Configures the GPIO pin, RF12, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF12 as an input
    SW05_IN05_SetDigitalInput();
    </code>

*/
#define SW05_IN05_SetDigitalInput()  (_TRISF12 = 1)
/**
  @Summary
    Configures the GPIO pin, RF12, as an output.

  @Description
    Configures the GPIO pin, RF12, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF12 as an output
    SW05_IN05_SetDigitalOutput();
    </code>

*/
#define SW05_IN05_SetDigitalOutput() (_TRISF12 = 0)
/**
  @Summary
    Sets the GPIO pin, RF13, high using LATF13.

  @Description
    Sets the GPIO pin, RF13, high using LATF13.

  @Preconditions
    The RF13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF13 high (1)
    SW04_IN04_SetHigh();
    </code>

*/
#define SW04_IN04_SetHigh()          (_LATF13 = 1)
/**
  @Summary
    Sets the GPIO pin, RF13, low using LATF13.

  @Description
    Sets the GPIO pin, RF13, low using LATF13.

  @Preconditions
    The RF13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF13 low (0)
    SW04_IN04_SetLow();
    </code>

*/
#define SW04_IN04_SetLow()           (_LATF13 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF13, using LATF13.

  @Description
    Toggles the GPIO pin, RF13, using LATF13.

  @Preconditions
    The RF13 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF13
    SW04_IN04_Toggle();
    </code>

*/
#define SW04_IN04_Toggle()           (_LATF13 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF13.

  @Description
    Reads the value of the GPIO pin, RF13.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF13
    postValue = SW04_IN04_GetValue();
    </code>

*/
#define SW04_IN04_GetValue()         _RF13
/**
  @Summary
    Configures the GPIO pin, RF13, as an input.

  @Description
    Configures the GPIO pin, RF13, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF13 as an input
    SW04_IN04_SetDigitalInput();
    </code>

*/
#define SW04_IN04_SetDigitalInput()  (_TRISF13 = 1)
/**
  @Summary
    Configures the GPIO pin, RF13, as an output.

  @Description
    Configures the GPIO pin, RF13, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF13 as an output
    SW04_IN04_SetDigitalOutput();
    </code>

*/
#define SW04_IN04_SetDigitalOutput() (_TRISF13 = 0)
/**
  @Summary
    Sets the GPIO pin, RF4, high using LATF4.

  @Description
    Sets the GPIO pin, RF4, high using LATF4.

  @Preconditions
    The RF4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF4 high (1)
    SW18_INB04_SetHigh();
    </code>

*/
#define SW18_INB04_SetHigh()          (_LATF4 = 1)
/**
  @Summary
    Sets the GPIO pin, RF4, low using LATF4.

  @Description
    Sets the GPIO pin, RF4, low using LATF4.

  @Preconditions
    The RF4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF4 low (0)
    SW18_INB04_SetLow();
    </code>

*/
#define SW18_INB04_SetLow()           (_LATF4 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF4, using LATF4.

  @Description
    Toggles the GPIO pin, RF4, using LATF4.

  @Preconditions
    The RF4 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF4
    SW18_INB04_Toggle();
    </code>

*/
#define SW18_INB04_Toggle()           (_LATF4 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF4.

  @Description
    Reads the value of the GPIO pin, RF4.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF4
    postValue = SW18_INB04_GetValue();
    </code>

*/
#define SW18_INB04_GetValue()         _RF4
/**
  @Summary
    Configures the GPIO pin, RF4, as an input.

  @Description
    Configures the GPIO pin, RF4, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF4 as an input
    SW18_INB04_SetDigitalInput();
    </code>

*/
#define SW18_INB04_SetDigitalInput()  (_TRISF4 = 1)
/**
  @Summary
    Configures the GPIO pin, RF4, as an output.

  @Description
    Configures the GPIO pin, RF4, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF4 as an output
    SW18_INB04_SetDigitalOutput();
    </code>

*/
#define SW18_INB04_SetDigitalOutput() (_TRISF4 = 0)
/**
  @Summary
    Sets the GPIO pin, RF5, high using LATF5.

  @Description
    Sets the GPIO pin, RF5, high using LATF5.

  @Preconditions
    The RF5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF5 high (1)
    SW19_INB05_SetHigh();
    </code>

*/
#define SW19_INB05_SetHigh()          (_LATF5 = 1)
/**
  @Summary
    Sets the GPIO pin, RF5, low using LATF5.

  @Description
    Sets the GPIO pin, RF5, low using LATF5.

  @Preconditions
    The RF5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF5 low (0)
    SW19_INB05_SetLow();
    </code>

*/
#define SW19_INB05_SetLow()           (_LATF5 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF5, using LATF5.

  @Description
    Toggles the GPIO pin, RF5, using LATF5.

  @Preconditions
    The RF5 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF5
    SW19_INB05_Toggle();
    </code>

*/
#define SW19_INB05_Toggle()           (_LATF5 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF5.

  @Description
    Reads the value of the GPIO pin, RF5.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF5
    postValue = SW19_INB05_GetValue();
    </code>

*/
#define SW19_INB05_GetValue()         _RF5
/**
  @Summary
    Configures the GPIO pin, RF5, as an input.

  @Description
    Configures the GPIO pin, RF5, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF5 as an input
    SW19_INB05_SetDigitalInput();
    </code>

*/
#define SW19_INB05_SetDigitalInput()  (_TRISF5 = 1)
/**
  @Summary
    Configures the GPIO pin, RF5, as an output.

  @Description
    Configures the GPIO pin, RF5, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF5 as an output
    SW19_INB05_SetDigitalOutput();
    </code>

*/
#define SW19_INB05_SetDigitalOutput() (_TRISF5 = 0)
/**
  @Summary
    Sets the GPIO pin, RF7, high using LATF7.

  @Description
    Sets the GPIO pin, RF7, high using LATF7.

  @Preconditions
    The RF7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF7 high (1)
    RED_LED_SERVICE_SetHigh();
    </code>

*/
#define RED_LED_SERVICE_SetHigh()          (_LATF7 = 1)
/**
  @Summary
    Sets the GPIO pin, RF7, low using LATF7.

  @Description
    Sets the GPIO pin, RF7, low using LATF7.

  @Preconditions
    The RF7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF7 low (0)
    RED_LED_SERVICE_SetLow();
    </code>

*/
#define RED_LED_SERVICE_SetLow()           (_LATF7 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF7, using LATF7.

  @Description
    Toggles the GPIO pin, RF7, using LATF7.

  @Preconditions
    The RF7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF7
    RED_LED_SERVICE_Toggle();
    </code>

*/
#define RED_LED_SERVICE_Toggle()           (_LATF7 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF7.

  @Description
    Reads the value of the GPIO pin, RF7.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF7
    postValue = RED_LED_SERVICE_GetValue();
    </code>

*/
#define RED_LED_SERVICE_GetValue()         _RF7
/**
  @Summary
    Configures the GPIO pin, RF7, as an input.

  @Description
    Configures the GPIO pin, RF7, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF7 as an input
    RED_LED_SERVICE_SetDigitalInput();
    </code>

*/
#define RED_LED_SERVICE_SetDigitalInput()  (_TRISF7 = 1)
/**
  @Summary
    Configures the GPIO pin, RF7, as an output.

  @Description
    Configures the GPIO pin, RF7, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF7 as an output
    RED_LED_SERVICE_SetDigitalOutput();
    </code>

*/
#define RED_LED_SERVICE_SetDigitalOutput() (_TRISF7 = 0)
/**
  @Summary
    Sets the GPIO pin, RF9, high using LATF9.

  @Description
    Sets the GPIO pin, RF9, high using LATF9.

  @Preconditions
    The RF9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF9 high (1)
    LED25_OUTB11_SetHigh();
    </code>

*/
#define LED25_OUTB11_SetHigh()          (_LATF9 = 1)
/**
  @Summary
    Sets the GPIO pin, RF9, low using LATF9.

  @Description
    Sets the GPIO pin, RF9, low using LATF9.

  @Preconditions
    The RF9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RF9 low (0)
    LED25_OUTB11_SetLow();
    </code>

*/
#define LED25_OUTB11_SetLow()           (_LATF9 = 0)
/**
  @Summary
    Toggles the GPIO pin, RF9, using LATF9.

  @Description
    Toggles the GPIO pin, RF9, using LATF9.

  @Preconditions
    The RF9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RF9
    LED25_OUTB11_Toggle();
    </code>

*/
#define LED25_OUTB11_Toggle()           (_LATF9 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RF9.

  @Description
    Reads the value of the GPIO pin, RF9.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RF9
    postValue = LED25_OUTB11_GetValue();
    </code>

*/
#define LED25_OUTB11_GetValue()         _RF9
/**
  @Summary
    Configures the GPIO pin, RF9, as an input.

  @Description
    Configures the GPIO pin, RF9, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF9 as an input
    LED25_OUTB11_SetDigitalInput();
    </code>

*/
#define LED25_OUTB11_SetDigitalInput()  (_TRISF9 = 1)
/**
  @Summary
    Configures the GPIO pin, RF9, as an output.

  @Description
    Configures the GPIO pin, RF9, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RF9 as an output
    LED25_OUTB11_SetDigitalOutput();
    </code>

*/
#define LED25_OUTB11_SetDigitalOutput() (_TRISF9 = 0)
/**
  @Summary
    Sets the GPIO pin, RG10, high using LATG10.

  @Description
    Sets the GPIO pin, RG10, high using LATG10.

  @Preconditions
    The RG10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG10 high (1)
    LED16_OUTB02_SetHigh();
    </code>

*/
#define LED16_OUTB02_SetHigh()          (_LATG10 = 1)
/**
  @Summary
    Sets the GPIO pin, RG10, low using LATG10.

  @Description
    Sets the GPIO pin, RG10, low using LATG10.

  @Preconditions
    The RG10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG10 low (0)
    LED16_OUTB02_SetLow();
    </code>

*/
#define LED16_OUTB02_SetLow()           (_LATG10 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG10, using LATG10.

  @Description
    Toggles the GPIO pin, RG10, using LATG10.

  @Preconditions
    The RG10 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG10
    LED16_OUTB02_Toggle();
    </code>

*/
#define LED16_OUTB02_Toggle()           (_LATG10 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG10.

  @Description
    Reads the value of the GPIO pin, RG10.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG10
    postValue = LED16_OUTB02_GetValue();
    </code>

*/
#define LED16_OUTB02_GetValue()         _RG10
/**
  @Summary
    Configures the GPIO pin, RG10, as an input.

  @Description
    Configures the GPIO pin, RG10, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG10 as an input
    LED16_OUTB02_SetDigitalInput();
    </code>

*/
#define LED16_OUTB02_SetDigitalInput()  (_TRISG10 = 1)
/**
  @Summary
    Configures the GPIO pin, RG10, as an output.

  @Description
    Configures the GPIO pin, RG10, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG10 as an output
    LED16_OUTB02_SetDigitalOutput();
    </code>

*/
#define LED16_OUTB02_SetDigitalOutput() (_TRISG10 = 0)
/**
  @Summary
    Sets the GPIO pin, RG11, high using LATG11.

  @Description
    Sets the GPIO pin, RG11, high using LATG11.

  @Preconditions
    The RG11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG11 high (1)
    SW03_IN03_SetHigh();
    </code>

*/
#define SW03_IN03_SetHigh()          (_LATG11 = 1)
/**
  @Summary
    Sets the GPIO pin, RG11, low using LATG11.

  @Description
    Sets the GPIO pin, RG11, low using LATG11.

  @Preconditions
    The RG11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG11 low (0)
    SW03_IN03_SetLow();
    </code>

*/
#define SW03_IN03_SetLow()           (_LATG11 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG11, using LATG11.

  @Description
    Toggles the GPIO pin, RG11, using LATG11.

  @Preconditions
    The RG11 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG11
    SW03_IN03_Toggle();
    </code>

*/
#define SW03_IN03_Toggle()           (_LATG11 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG11.

  @Description
    Reads the value of the GPIO pin, RG11.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG11
    postValue = SW03_IN03_GetValue();
    </code>

*/
#define SW03_IN03_GetValue()         _RG11
/**
  @Summary
    Configures the GPIO pin, RG11, as an input.

  @Description
    Configures the GPIO pin, RG11, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG11 as an input
    SW03_IN03_SetDigitalInput();
    </code>

*/
#define SW03_IN03_SetDigitalInput()  (_TRISG11 = 1)
/**
  @Summary
    Configures the GPIO pin, RG11, as an output.

  @Description
    Configures the GPIO pin, RG11, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG11 as an output
    SW03_IN03_SetDigitalOutput();
    </code>

*/
#define SW03_IN03_SetDigitalOutput() (_TRISG11 = 0)
/**
  @Summary
    Sets the GPIO pin, RG15, high using LATG15.

  @Description
    Sets the GPIO pin, RG15, high using LATG15.

  @Preconditions
    The RG15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG15 high (1)
    LED04_OUT04_SetHigh();
    </code>

*/
#define LED04_OUT04_SetHigh()          (_LATG15 = 1)
/**
  @Summary
    Sets the GPIO pin, RG15, low using LATG15.

  @Description
    Sets the GPIO pin, RG15, low using LATG15.

  @Preconditions
    The RG15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG15 low (0)
    LED04_OUT04_SetLow();
    </code>

*/
#define LED04_OUT04_SetLow()           (_LATG15 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG15, using LATG15.

  @Description
    Toggles the GPIO pin, RG15, using LATG15.

  @Preconditions
    The RG15 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG15
    LED04_OUT04_Toggle();
    </code>

*/
#define LED04_OUT04_Toggle()           (_LATG15 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG15.

  @Description
    Reads the value of the GPIO pin, RG15.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG15
    postValue = LED04_OUT04_GetValue();
    </code>

*/
#define LED04_OUT04_GetValue()         _RG15
/**
  @Summary
    Configures the GPIO pin, RG15, as an input.

  @Description
    Configures the GPIO pin, RG15, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG15 as an input
    LED04_OUT04_SetDigitalInput();
    </code>

*/
#define LED04_OUT04_SetDigitalInput()  (_TRISG15 = 1)
/**
  @Summary
    Configures the GPIO pin, RG15, as an output.

  @Description
    Configures the GPIO pin, RG15, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG15 as an output
    LED04_OUT04_SetDigitalOutput();
    </code>

*/
#define LED04_OUT04_SetDigitalOutput() (_TRISG15 = 0)
/**
  @Summary
    Sets the GPIO pin, RG2, high using LATG2.

  @Description
    Sets the GPIO pin, RG2, high using LATG2.

  @Preconditions
    The RG2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG2 high (1)
    SW16_INB02_SetHigh();
    </code>

*/
#define SW16_INB02_SetHigh()          (_LATG2 = 1)
/**
  @Summary
    Sets the GPIO pin, RG2, low using LATG2.

  @Description
    Sets the GPIO pin, RG2, low using LATG2.

  @Preconditions
    The RG2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG2 low (0)
    SW16_INB02_SetLow();
    </code>

*/
#define SW16_INB02_SetLow()           (_LATG2 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG2, using LATG2.

  @Description
    Toggles the GPIO pin, RG2, using LATG2.

  @Preconditions
    The RG2 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG2
    SW16_INB02_Toggle();
    </code>

*/
#define SW16_INB02_Toggle()           (_LATG2 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG2.

  @Description
    Reads the value of the GPIO pin, RG2.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG2
    postValue = SW16_INB02_GetValue();
    </code>

*/
#define SW16_INB02_GetValue()         _RG2
/**
  @Summary
    Configures the GPIO pin, RG2, as an input.

  @Description
    Configures the GPIO pin, RG2, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG2 as an input
    SW16_INB02_SetDigitalInput();
    </code>

*/
#define SW16_INB02_SetDigitalInput()  (_TRISG2 = 1)
/**
  @Summary
    Configures the GPIO pin, RG2, as an output.

  @Description
    Configures the GPIO pin, RG2, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG2 as an output
    SW16_INB02_SetDigitalOutput();
    </code>

*/
#define SW16_INB02_SetDigitalOutput() (_TRISG2 = 0)
/**
  @Summary
    Sets the GPIO pin, RG3, high using LATG3.

  @Description
    Sets the GPIO pin, RG3, high using LATG3.

  @Preconditions
    The RG3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG3 high (1)
    SW17_INB03_SetHigh();
    </code>

*/
#define SW17_INB03_SetHigh()          (_LATG3 = 1)
/**
  @Summary
    Sets the GPIO pin, RG3, low using LATG3.

  @Description
    Sets the GPIO pin, RG3, low using LATG3.

  @Preconditions
    The RG3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG3 low (0)
    SW17_INB03_SetLow();
    </code>

*/
#define SW17_INB03_SetLow()           (_LATG3 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG3, using LATG3.

  @Description
    Toggles the GPIO pin, RG3, using LATG3.

  @Preconditions
    The RG3 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG3
    SW17_INB03_Toggle();
    </code>

*/
#define SW17_INB03_Toggle()           (_LATG3 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG3.

  @Description
    Reads the value of the GPIO pin, RG3.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG3
    postValue = SW17_INB03_GetValue();
    </code>

*/
#define SW17_INB03_GetValue()         _RG3
/**
  @Summary
    Configures the GPIO pin, RG3, as an input.

  @Description
    Configures the GPIO pin, RG3, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG3 as an input
    SW17_INB03_SetDigitalInput();
    </code>

*/
#define SW17_INB03_SetDigitalInput()  (_TRISG3 = 1)
/**
  @Summary
    Configures the GPIO pin, RG3, as an output.

  @Description
    Configures the GPIO pin, RG3, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG3 as an output
    SW17_INB03_SetDigitalOutput();
    </code>

*/
#define SW17_INB03_SetDigitalOutput() (_TRISG3 = 0)
/**
  @Summary
    Sets the GPIO pin, RG6, high using LATG6.

  @Description
    Sets the GPIO pin, RG6, high using LATG6.

  @Preconditions
    The RG6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG6 high (1)
    LED12_OUT12_SetHigh();
    </code>

*/
#define LED12_OUT12_SetHigh()          (_LATG6 = 1)
/**
  @Summary
    Sets the GPIO pin, RG6, low using LATG6.

  @Description
    Sets the GPIO pin, RG6, low using LATG6.

  @Preconditions
    The RG6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG6 low (0)
    LED12_OUT12_SetLow();
    </code>

*/
#define LED12_OUT12_SetLow()           (_LATG6 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG6, using LATG6.

  @Description
    Toggles the GPIO pin, RG6, using LATG6.

  @Preconditions
    The RG6 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG6
    LED12_OUT12_Toggle();
    </code>

*/
#define LED12_OUT12_Toggle()           (_LATG6 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG6.

  @Description
    Reads the value of the GPIO pin, RG6.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG6
    postValue = LED12_OUT12_GetValue();
    </code>

*/
#define LED12_OUT12_GetValue()         _RG6
/**
  @Summary
    Configures the GPIO pin, RG6, as an input.

  @Description
    Configures the GPIO pin, RG6, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG6 as an input
    LED12_OUT12_SetDigitalInput();
    </code>

*/
#define LED12_OUT12_SetDigitalInput()  (_TRISG6 = 1)
/**
  @Summary
    Configures the GPIO pin, RG6, as an output.

  @Description
    Configures the GPIO pin, RG6, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG6 as an output
    LED12_OUT12_SetDigitalOutput();
    </code>

*/
#define LED12_OUT12_SetDigitalOutput() (_TRISG6 = 0)
/**
  @Summary
    Sets the GPIO pin, RG7, high using LATG7.

  @Description
    Sets the GPIO pin, RG7, high using LATG7.

  @Preconditions
    The RG7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG7 high (1)
    LED13_OUT13_SetHigh();
    </code>

*/
#define LED13_OUT13_SetHigh()          (_LATG7 = 1)
/**
  @Summary
    Sets the GPIO pin, RG7, low using LATG7.

  @Description
    Sets the GPIO pin, RG7, low using LATG7.

  @Preconditions
    The RG7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG7 low (0)
    LED13_OUT13_SetLow();
    </code>

*/
#define LED13_OUT13_SetLow()           (_LATG7 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG7, using LATG7.

  @Description
    Toggles the GPIO pin, RG7, using LATG7.

  @Preconditions
    The RG7 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG7
    LED13_OUT13_Toggle();
    </code>

*/
#define LED13_OUT13_Toggle()           (_LATG7 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG7.

  @Description
    Reads the value of the GPIO pin, RG7.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG7
    postValue = LED13_OUT13_GetValue();
    </code>

*/
#define LED13_OUT13_GetValue()         _RG7
/**
  @Summary
    Configures the GPIO pin, RG7, as an input.

  @Description
    Configures the GPIO pin, RG7, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG7 as an input
    LED13_OUT13_SetDigitalInput();
    </code>

*/
#define LED13_OUT13_SetDigitalInput()  (_TRISG7 = 1)
/**
  @Summary
    Configures the GPIO pin, RG7, as an output.

  @Description
    Configures the GPIO pin, RG7, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG7 as an output
    LED13_OUT13_SetDigitalOutput();
    </code>

*/
#define LED13_OUT13_SetDigitalOutput() (_TRISG7 = 0)
/**
  @Summary
    Sets the GPIO pin, RG8, high using LATG8.

  @Description
    Sets the GPIO pin, RG8, high using LATG8.

  @Preconditions
    The RG8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG8 high (1)
    LED14_OUT14_SetHigh();
    </code>

*/
#define LED14_OUT14_SetHigh()          (_LATG8 = 1)
/**
  @Summary
    Sets the GPIO pin, RG8, low using LATG8.

  @Description
    Sets the GPIO pin, RG8, low using LATG8.

  @Preconditions
    The RG8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG8 low (0)
    LED14_OUT14_SetLow();
    </code>

*/
#define LED14_OUT14_SetLow()           (_LATG8 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG8, using LATG8.

  @Description
    Toggles the GPIO pin, RG8, using LATG8.

  @Preconditions
    The RG8 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG8
    LED14_OUT14_Toggle();
    </code>

*/
#define LED14_OUT14_Toggle()           (_LATG8 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG8.

  @Description
    Reads the value of the GPIO pin, RG8.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG8
    postValue = LED14_OUT14_GetValue();
    </code>

*/
#define LED14_OUT14_GetValue()         _RG8
/**
  @Summary
    Configures the GPIO pin, RG8, as an input.

  @Description
    Configures the GPIO pin, RG8, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG8 as an input
    LED14_OUT14_SetDigitalInput();
    </code>

*/
#define LED14_OUT14_SetDigitalInput()  (_TRISG8 = 1)
/**
  @Summary
    Configures the GPIO pin, RG8, as an output.

  @Description
    Configures the GPIO pin, RG8, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG8 as an output
    LED14_OUT14_SetDigitalOutput();
    </code>

*/
#define LED14_OUT14_SetDigitalOutput() (_TRISG8 = 0)
/**
  @Summary
    Sets the GPIO pin, RG9, high using LATG9.

  @Description
    Sets the GPIO pin, RG9, high using LATG9.

  @Preconditions
    The RG9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG9 high (1)
    LED15_OUTB01_SetHigh();
    </code>

*/
#define LED15_OUTB01_SetHigh()          (_LATG9 = 1)
/**
  @Summary
    Sets the GPIO pin, RG9, low using LATG9.

  @Description
    Sets the GPIO pin, RG9, low using LATG9.

  @Preconditions
    The RG9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Set RG9 low (0)
    LED15_OUTB01_SetLow();
    </code>

*/
#define LED15_OUTB01_SetLow()           (_LATG9 = 0)
/**
  @Summary
    Toggles the GPIO pin, RG9, using LATG9.

  @Description
    Toggles the GPIO pin, RG9, using LATG9.

  @Preconditions
    The RG9 must be set to an output.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Toggle RG9
    LED15_OUTB01_Toggle();
    </code>

*/
#define LED15_OUTB01_Toggle()           (_LATG9 ^= 1)
/**
  @Summary
    Reads the value of the GPIO pin, RG9.

  @Description
    Reads the value of the GPIO pin, RG9.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    uint16_t portValue;

    // Read RG9
    postValue = LED15_OUTB01_GetValue();
    </code>

*/
#define LED15_OUTB01_GetValue()         _RG9
/**
  @Summary
    Configures the GPIO pin, RG9, as an input.

  @Description
    Configures the GPIO pin, RG9, as an input.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG9 as an input
    LED15_OUTB01_SetDigitalInput();
    </code>

*/
#define LED15_OUTB01_SetDigitalInput()  (_TRISG9 = 1)
/**
  @Summary
    Configures the GPIO pin, RG9, as an output.

  @Description
    Configures the GPIO pin, RG9, as an output.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    // Sets the RG9 as an output
    LED15_OUTB01_SetDigitalOutput();
    </code>

*/
#define LED15_OUTB01_SetDigitalOutput() (_TRISG9 = 0)

/**
    Section: Function Prototypes
*/
/**
  @Summary
    Configures the pin settings of the dsPIC33EP256GM710
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Description
    This is the generated manager file for the PIC24 / dsPIC33 / PIC32MM MCUs device.  This manager
    configures the pins direction, initial state, analog setting.
    The peripheral pin select, PPS, configuration is also handled by this manager.

  @Preconditions
    None.

  @Returns
    None.

  @Param
    None.

  @Example
    <code>
    void SYSTEM_Initialize(void)
    {
        // Other initializers are called from this function
        PIN_MANAGER_Initialize();
    }
    </code>

*/
void PIN_MANAGER_Initialize (void);



#endif
